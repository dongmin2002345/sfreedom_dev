// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_CECROPS_ANIM_H__
#define M_CECROPS_ANIM_H__

namespace M_Cecrops_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000066,
        ATTK_01_BTLIDLE         = 1000065,
        ATTK_02_ATTACK          = 1000067,
        ATTK_02_BTLIDLE         = 1000068,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        DIE_01                  = 1000020,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_01_BLENDING         = 1000015,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_01_START          = 1000204,
        SKILL_04_ATTACK         = 1000207,
        SKILL_04_BTLIDLE        = 1000210,
        SKILL_05_ATTACK         = 1000208,
        SKILL_06_ATTACK         = 1000209,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef M_CECROPS_ANIM_H__
