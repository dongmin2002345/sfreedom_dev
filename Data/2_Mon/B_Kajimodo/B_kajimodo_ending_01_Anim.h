// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_KAJIMODO_ENDING_01_ANIM_H__
#define B_KAJIMODO_ENDING_01_ANIM_H__

namespace B_kajimodo_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000082
    };
}

#endif  // #ifndef B_KAJIMODO_ENDING_01_ANIM_H__
