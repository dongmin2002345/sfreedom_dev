// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_PIEDAMA_ANIM_H__
#define M_PIEDAMA_ANIM_H__

namespace M_Piedama_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_BLOWUP_DOWN         = 1000044,
        DIE_BLOWUP_LOOP         = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING        = 1000042,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN                     = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef M_PIEDAMA_ANIM_H__
