// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_BADCOMPANYBOSS_OPENING_ANIM_H__
#define N_BADCOMPANYBOSS_OPENING_ANIM_H__

namespace N_BadCompanyBoss_opening_Anim
{
    enum
    {
        OPENING_01              = 1000082
    };
}

#endif  // #ifndef N_BADCOMPANYBOSS_OPENING_ANIM_H__
