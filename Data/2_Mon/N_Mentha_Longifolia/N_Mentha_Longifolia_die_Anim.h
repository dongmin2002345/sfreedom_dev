// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MENTHA_LONGIFOLIA_DIE_ANIM_H__
#define N_MENTHA_LONGIFOLIA_DIE_ANIM_H__

namespace N_Mentha_Longifolia_die_Anim
{
    enum
    {
        DIE_01_FINISH           = 1000020,
        DIE_01_LOOP             = 1000021
    };
}

#endif  // #ifndef N_MENTHA_LONGIFOLIA_DIE_ANIM_H__
