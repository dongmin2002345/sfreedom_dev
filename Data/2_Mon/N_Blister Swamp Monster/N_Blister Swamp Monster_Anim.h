// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_BLISTER_SWAMP_MONSTER_ANIM_H__
#define N_BLISTER_SWAMP_MONSTER_ANIM_H__

namespace N_Blister_Swamp_Monster_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SHOT_01                 = 1000065,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef N_BLISTER_SWAMP_MONSTER_ANIM_H__
