// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MIMIR_STINGSOLDIER_ANIM_H__
#define N_MIMIR_STINGSOLDIER_ANIM_H__

namespace N_Mimir_stingsoldier_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000001,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000071,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef N_MIMIR_STINGSOLDIER_ANIM_H__
