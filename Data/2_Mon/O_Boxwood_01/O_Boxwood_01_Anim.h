// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef O_BOXWOOD_01_ANIM_H__
#define O_BOXWOOD_01_ANIM_H__

namespace O_Boxwood_01_Anim
{
    enum
    {
        DIE_01                  = 1000022,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef O_BOXWOOD_01_ANIM_H__
