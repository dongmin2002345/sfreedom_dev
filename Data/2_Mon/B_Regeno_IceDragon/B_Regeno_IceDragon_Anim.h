// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_REGENO_ICEDRAGON_ANIM_H__
#define B_REGENO_ICEDRAGON_ANIM_H__

namespace B_Regeno_IceDragon_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_BLOWUP              = 1000014,
        FURY_01                 = 1000900,
        RUN_01                  = 1000055,
        SKILL_01_01             = 1000201,
        SKILL_01_02             = 1000202,
        SKILL_02_01             = 1000203,
        SKILL_02_02             = 1000204,
        SKILL_02_03             = 1000205,
        SKILL_03_01             = 1000206,
        SKILL_03_02             = 1000207,
        SKILL_04_01             = 1000208,
        SKILL_04_02             = 1000209,
        SKILL_04_03             = 1000210,
        SKILL_05_01             = 1000211,
        SKILL_05_02             = 1000212,
        SKILL_05_03             = 1000213,
        SKILL_06_01             = 1000214,
        SKILL_06_02             = 1000215,
        SKILL_06_03             = 1000216,
        SKILL_07_01             = 1000217,
        SKILL_07_02             = 1000218,
        SKILL_07_03             = 1000219,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef B_REGENO_ICEDRAGON_ANIM_H__
