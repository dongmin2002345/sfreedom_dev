// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ARAM_ARA_ANIM_H__
#define N_ARAM_ARA_ANIM_H__

namespace N_aram_ara_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01_ATTCK           = 1000063,
        SHOT_01_BTLIDLE         = 1000062,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef N_ARAM_ARA_ANIM_H__
