// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ART_DRUM1001_01_ANIM_H__
#define ART_DRUM1001_01_ANIM_H__

namespace Art_Drum1001_01_Anim
{
    enum
    {
        BREAK_01                = 1000022,
        IDLE_01                 = 1000000,
        SHAKE_01                = 1000011
    };
}

#endif  // #ifndef ART_DRUM1001_01_ANIM_H__
