// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GRIFFION_ANIM_H__
#define GRIFFION_ANIM_H__

namespace griffion_Anim
{
    enum
    {
        ATTK_01                 = 0,
        DMG_01                  = 1,
        DOZE                    = 2,
        FLY_ATTK_01             = 3,
        FLY_IDLE                = 4,
        FLY_RUN_01              = 5,
        IDLE_01                 = 6,
        IDLE_02                 = 7,
        RUN_01                  = 8,
        WALK_01                 = 9,
        WALK_02                 = 10
    };
}

#endif  // #ifndef GRIFFION_ANIM_H__
