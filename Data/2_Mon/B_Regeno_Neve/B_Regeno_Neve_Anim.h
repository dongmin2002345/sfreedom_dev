// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_REGENO_NEVE_ANIM_H__
#define B_REGENO_NEVE_ANIM_H__

namespace B_Regeno_Neve_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTACK         = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTACK         = 1000206,
        SKILL_03_BTLIDLE        = 1000205,
        SKILL_04                = 1000207,
        SKILL_05_ATTACK         = 1000209,
        SKILL_05_BTLIDLE        = 1000208
    };
}

#endif  // #ifndef B_REGENO_NEVE_ANIM_H__
