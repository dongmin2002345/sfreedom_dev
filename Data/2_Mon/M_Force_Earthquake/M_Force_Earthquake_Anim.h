// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_FORCE_EARTHQUAKE_ANIM_H__
#define M_FORCE_EARTHQUAKE_ANIM_H__

namespace M_Force_Earthquake_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        DIE_01                  = 1000020,
        IDLE_01                 = 1000000,
        SKILL_01_ATTCK          = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTCK          = 1000205,
        SKILL_02_FINISH         = 1000206,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTCK          = 1000209,
        SKILL_03_ATTCK_IDLE     = 1000210,
        SKILL_03_BTLIDLE        = 1000208,
        SKILL_03_FINISH         = 1000211,
        SKILL_03_START          = 1000207,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef M_FORCE_EARTHQUAKE_ANIM_H__
