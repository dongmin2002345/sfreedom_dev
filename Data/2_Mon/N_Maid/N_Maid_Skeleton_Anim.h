// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MAID_SKELETON_ANIM_H__
#define N_MAID_SKELETON_ANIM_H__

namespace N_Maid_Skeleton_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BTLIDLE_01              = 1000005,
        IDLE_01                 = 1000000,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef N_MAID_SKELETON_ANIM_H__
