// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_MASTER_MACHINE_ANIM_H__
#define B_MASTER_MACHINE_ANIM_H__

namespace B_Master_machine_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01_01               = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTACK         = 1000209,
        SKILL_03_BTLIDLE        = 1000208,
        SKILL_03_START          = 1000207,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef B_MASTER_MACHINE_ANIM_H__
