// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_MASTER_MACHINE_DIE_ANIM_H__
#define B_MASTER_MACHINE_DIE_ANIM_H__

namespace B_Master_machine_die_Anim
{
    enum
    {
        DIE_01_02               = 1000022
    };
}

#endif  // #ifndef B_MASTER_MACHINE_DIE_ANIM_H__
