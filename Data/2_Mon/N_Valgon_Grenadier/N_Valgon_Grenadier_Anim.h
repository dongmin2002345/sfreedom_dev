// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_VALGON_GRENADIER_ANIM_H__
#define N_VALGON_GRENADIER_ANIM_H__

namespace N_Valgon_Grenadier_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01_01              = 1000063,
        SHOT_01_02              = 1000064,
        SHOT_01_BTLIDLE         = 1000065,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_VALGON_GRENADIER_ANIM_H__
