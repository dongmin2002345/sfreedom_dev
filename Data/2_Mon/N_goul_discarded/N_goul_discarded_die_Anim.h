// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_GOUL_DISCARDED_DIE_ANIM_H__
#define N_GOUL_DISCARDED_DIE_ANIM_H__

namespace N_goul_discarded_die_Anim
{
    enum
    {
        DIE                     = 1000021
    };
}

#endif  // #ifndef N_GOUL_DISCARDED_DIE_ANIM_H__
