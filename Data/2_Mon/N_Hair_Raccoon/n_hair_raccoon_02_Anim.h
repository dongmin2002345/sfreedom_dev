// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_HAIR_RACCOON_02_ANIM_H__
#define N_HAIR_RACCOON_02_ANIM_H__

namespace n_hair_raccoon_02_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef N_HAIR_RACCOON_02_ANIM_H__
