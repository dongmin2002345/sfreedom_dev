// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SHARK_KING_ABDULA_DIE_ANIM_H__
#define N_SHARK_KING_ABDULA_DIE_ANIM_H__

namespace N_Shark_King_Abdula_die_Anim
{
    enum
    {
        DIE_AFTER_01            = 1000021
    };
}

#endif  // #ifndef N_SHARK_KING_ABDULA_DIE_ANIM_H__
