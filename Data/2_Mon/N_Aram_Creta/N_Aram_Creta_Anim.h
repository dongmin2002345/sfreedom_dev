// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ARAM_CRETA_ANIM_H__
#define N_ARAM_CRETA_ANIM_H__

namespace N_Aram_Creta_Anim
{
    enum
    {
        ATTK_01_ATTCK           = 1000062,
        ATTK_01_BTLIDLE         = 1000061,
        ATTK_02_ATTCK           = 1000064,
        ATTK_02_BTLIDLE         = 1000063,
        ATTK_02_FINISH          = 1000065,
        ATTK_03_ATTCK           = 1000067,
        ATTK_03_ATTCK_IDLE      = 1000068,
        ATTK_03_BTLIDLE         = 1000066,
        ATTK_03_FINISH          = 1000069,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_ARAM_CRETA_ANIM_H__
