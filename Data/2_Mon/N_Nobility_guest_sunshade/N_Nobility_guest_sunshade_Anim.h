// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_NOBILITY_GUEST_SUNSHADE_ANIM_H__
#define N_NOBILITY_GUEST_SUNSHADE_ANIM_H__

namespace N_Nobility_guest_sunshade_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_ATTACK         = 1000061,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_03_ATTACK         = 1000204,
        SKILL_03_BTLIDLE        = 1000203,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef N_NOBILITY_GUEST_SUNSHADE_ANIM_H__
