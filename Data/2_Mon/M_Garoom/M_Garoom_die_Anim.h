// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_GAROOM_DIE_ANIM_H__
#define M_GAROOM_DIE_ANIM_H__

namespace M_Garoom_die_Anim
{
    enum
    {
        DIE_01                  = 1000027
    };
}

#endif  // #ifndef M_GAROOM_DIE_ANIM_H__
