// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_FOSSILEARTH_CAM_ANIM_H__
#define B_FOSSILEARTH_CAM_ANIM_H__

namespace B_FossilEarth_cam_Anim
{
    enum
    {
        DIE_01                  = 1000002,
        FRIEBREACE_01           = 1000001,
        OPENING_01              = 1000000
    };
}

#endif  // #ifndef B_FOSSILEARTH_CAM_ANIM_H__
