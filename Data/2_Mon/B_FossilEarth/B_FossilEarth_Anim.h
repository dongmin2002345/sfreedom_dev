// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_FOSSILEARTH_ANIM_H__
#define B_FOSSILEARTH_ANIM_H__

namespace B_FossilEarth_Anim
{
    enum
    {
        ATTK_LOOP_L             = 1000062,
        ATTK_LOOP_R             = 1000072,
        ATTK_SHOT_L             = 1000063,
        ATTK_SHOT_R             = 1000073,
        ATTK_START_L            = 1000061,
        ATTK_START_R            = 1000071,
        CALL_01                 = 1000202,
        DIE_01                  = 1000020,
        DIE_02                  = 1000024,
        DMG_01                  = 1000011,
        FIREBREACE_01           = 1000074,
        FIRETURN_LEFT           = 1000076,
        FIRETURN_RIGHT          = 1000077,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        KNOCKDOWN_01            = 1000021,
        KNOCKDOWN_DMG_01        = 1000012,
        KNOCKDOWN_IDLE_01       = 1000022,
        KNOCKDOWN_IDLE_02       = 1000023,
        OPENING_01              = 1000081,
        POWERBREAK_01_01        = 1000075,
        POWERBREAK_01_02        = 1000078,
        POWERBREAK_01_03        = 1000079,
        SHOUT_01                = 1000201,
        STANDUP_01              = 1000025
    };
}

#endif  // #ifndef B_FOSSILEARTH_ANIM_H__
