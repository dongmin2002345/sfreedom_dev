// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_STONE_SCORPION_DIE_ANIM_H__
#define N_STONE_SCORPION_DIE_ANIM_H__

namespace N_Stone_Scorpion_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef N_STONE_SCORPION_DIE_ANIM_H__
