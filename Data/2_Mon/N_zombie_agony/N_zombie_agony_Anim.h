// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ZOMBIE_AGONY_ANIM_H__
#define N_ZOMBIE_AGONY_ANIM_H__

namespace N_zombie_agony_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000036,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTACK         = 1000204,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_ZOMBIE_AGONY_ANIM_H__
