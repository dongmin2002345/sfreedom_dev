// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_SAPIERON_ANIM_H__
#define M_SAPIERON_ANIM_H__

namespace M_Sapieron_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000201,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_02_ATTACK         = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTACK         = 1000206,
        SKILL_03_BTLIDLE        = 1000205,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef M_SAPIERON_ANIM_H__
