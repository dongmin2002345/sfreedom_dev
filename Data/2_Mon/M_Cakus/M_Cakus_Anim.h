// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_CAKUS_ANIM_H__
#define M_CAKUS_ANIM_H__

namespace M_Cakus_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01_01              = 1000063,
        SHOT_01_02              = 1000064,
        SHOT_01_BTLIDLE         = 1000065,
        SKILL_01_ATTACK         = 1000201,
        SKILL_01_BTLIDLE        = 1000204,
        SKILL_02_ATTACK         = 1000203,
        SKILL_02_BTLIDLE        = 1000202,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef M_CAKUS_ANIM_H__
