// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_DISRUPTER_ANIM_H__
#define M_DISRUPTER_ANIM_H__

namespace M_Disrupter_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING            = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING                 = 1000081,
        RUN_01                  = 1000051,
        SKILL_02_ATTCK          = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_05_ATTCK          = 1000210,
        SKILL_05_BTLIDLE        = 1000209,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALLK_01                = 1000055
    };
}

#endif  // #ifndef M_DISRUPTER_ANIM_H__
