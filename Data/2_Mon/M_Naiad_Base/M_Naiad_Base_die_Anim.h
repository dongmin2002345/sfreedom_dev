// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_NAIAD_BASE_DIE_ANIM_H__
#define M_NAIAD_BASE_DIE_ANIM_H__

namespace M_Naiad_Base_die_Anim
{
    enum
    {
        DIE_02                  = 1000021
    };
}

#endif  // #ifndef M_NAIAD_BASE_DIE_ANIM_H__
