// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_TAMER_MARKER_ANIM_H__
#define M_TAMER_MARKER_ANIM_H__

namespace M_Tamer_Marker_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000063,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202
    };
}

#endif  // #ifndef M_TAMER_MARKER_ANIM_H__
