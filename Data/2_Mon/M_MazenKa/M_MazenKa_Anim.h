// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_MAZENKA_ANIM_H__
#define M_MAZENKA_ANIM_H__

namespace M_MazenKa_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        ATTK_03                 = 1000063,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        SHOT_01_ATTACK          = 1000071,
        SHOT_01_BTLIDLE         = 1000072,
        SHOT_01_FINISH          = 1000073,
        SKILL_01_ATTACK         = 1000201,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_02_ATTACK         = 1000203,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef M_MAZENKA_ANIM_H__
