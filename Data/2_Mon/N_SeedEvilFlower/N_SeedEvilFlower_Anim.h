// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SEEDEVILFLOWER_ANIM_H__
#define N_SEEDEVILFLOWER_ANIM_H__

namespace N_SeedEvilFlower_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        OPENING_01              = 1000081,
        OPENING_02              = 1000082,
        RUN_01                  = 1000051,
        RUN_02                  = 1000052,
        SHOT_01                 = 1000071,
        SHOT_02                 = 1000101,
        SKILL_01                = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_SEEDEVILFLOWER_ANIM_H__
