// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_WOODY_BIG_MALLOW_DIE_01_ANIM_H__
#define B_WOODY_BIG_MALLOW_DIE_01_ANIM_H__

namespace B_Woody_Big_mallow_die_01_Anim
{
    enum
    {
        DIE_01                  = 1000025
    };
}

#endif  // #ifndef B_WOODY_BIG_MALLOW_DIE_01_ANIM_H__
