// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_WOODY_BIG_MALLOW_ANIM_H__
#define B_WOODY_BIG_MALLOW_ANIM_H__

namespace B_Woody_Big_mallow_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_FINISH         = 1000203,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_START          = 1000204,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef B_WOODY_BIG_MALLOW_ANIM_H__
