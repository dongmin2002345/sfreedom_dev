// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_WOODY_BIG_MALLOW_DIE_04_ANIM_H__
#define B_WOODY_BIG_MALLOW_DIE_04_ANIM_H__

namespace B_Woody_Big_mallow_die_04_Anim
{
    enum
    {
        DIE_04                  = 1000028
    };
}

#endif  // #ifndef B_WOODY_BIG_MALLOW_DIE_04_ANIM_H__
