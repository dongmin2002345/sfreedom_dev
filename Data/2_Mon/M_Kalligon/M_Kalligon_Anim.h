// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_KALLIGON_ANIM_H__
#define M_KALLIGON_ANIM_H__

namespace M_Kalligon_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_01_SKILL_SHOT   = 1000402,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000404,
        BTLIDLE_01_SKILL_SHOT_START = 1000401,
        DASH_AFTER_ATTK_01_02   = 1000744,
        DASH_AFTER_ATTK_BTLIDLE_01_IDLE = 1000742,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000741,
        DASH_AFTER_ATTK_RUSH    = 1000743,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000512,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000511,
        DASH_PIERCING_ATTK_FINISH = 1000514,
        DASH_PIERCING_ATTK_RUSH = 1000513,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE = 1000612,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE_START = 1000611,
        MASSIVE_PROJECTILE_JUMP_FINISH = 1000614,
        MASSIVE_PROJECTILE_JUMP_FIRE = 1000613,
        OPENING                 = 1000082,
        RUN_01                  = 1000055,
        SET_TRAP_ATTK_01        = 1001113,
        SET_TRAP_BTLIDLE_01     = 1001112,
        SET_TRAP_BTLIDLE_01_START = 1001111,
        SHOT_01                 = 1000252,
        SHOT_RETURN_01          = 1000405,
        SKILL_SHOT_01           = 1000403,
        STANDUP                 = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef M_KALLIGON_ANIM_H__
