// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_VEGAS_DIE_ANIM_H__
#define N_VEGAS_DIE_ANIM_H__

namespace N_Vegas_die_Anim
{
    enum
    {
        DIE_03                  = 1000026
    };
}

#endif  // #ifndef N_VEGAS_DIE_ANIM_H__
