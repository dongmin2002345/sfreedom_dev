// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_BOMBMENTHA_ANIM_H__
#define N_BOMBMENTHA_ANIM_H__

namespace N_BombMentha_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        IDLE_01                 = 1000000,
        LEVEL_01_DROP           = 1000201,
        LEVEL_01_IDLE           = 1000202,
        LEVEL_02_DROP           = 1000203,
        LEVEL_02_IDLE           = 1000204,
        LEVEL_03_DROP           = 1000205,
        LEVEL_03_IDLE           = 1000206,
        LEVEL_04_DROP           = 1000207,
        LEVEL_04_IDLE           = 1000208
    };
}

#endif  // #ifndef N_BOMBMENTHA_ANIM_H__
