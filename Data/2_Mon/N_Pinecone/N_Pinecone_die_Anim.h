// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_PINECONE_DIE_ANIM_H__
#define N_PINECONE_DIE_ANIM_H__

namespace N_Pinecone_die_Anim
{
    enum
    {
        DIE_02                  = 1000021
    };
}

#endif  // #ifndef N_PINECONE_DIE_ANIM_H__
