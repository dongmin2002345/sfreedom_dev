// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MONSTERGATE_ANIM_H__
#define N_MONSTERGATE_ANIM_H__

namespace N_MonsterGate_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        DIE_01                  = 1000021,
        IDLE_01                 = 1000000,
        SHOT_01                 = 1000062
    };
}

#endif  // #ifndef N_MONSTERGATE_ANIM_H__
