// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PUMKIN_ANIM_H__
#define PUMKIN_ANIM_H__

namespace Pumkin_Anim
{
    enum
    {
        PUMPKIN_DIE_01          = 1000020
    };
}

#endif  // #ifndef PUMKIN_ANIM_H__
