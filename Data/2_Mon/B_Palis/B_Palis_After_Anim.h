// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_PALIS_AFTER_ANIM_H__
#define B_PALIS_AFTER_ANIM_H__

namespace B_Palis_After_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DIE_IDLE_01             = 1000022,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTACK         = 1000205,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTACK         = 1000207,
        SKILL_03_BTLIDLE        = 1000206,
        SKILL_04_ATTACK         = 1000209,
        SKILL_04_BTLIDLE        = 1000208,
        SKILL_05_ATTACK         = 1000212,
        SKILL_05_BTLIDLE        = 1000211,
        SKILL_05_START          = 1000210,
        SKILL_06_ATTACK         = 1000213
    };
}

#endif  // #ifndef B_PALIS_AFTER_ANIM_H__
