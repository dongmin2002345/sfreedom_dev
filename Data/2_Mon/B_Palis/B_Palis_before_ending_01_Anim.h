// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_PALIS_BEFORE_ENDING_01_ANIM_H__
#define B_PALIS_BEFORE_ENDING_01_ANIM_H__

namespace B_Palis_before_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000000
    };
}

#endif  // #ifndef B_PALIS_BEFORE_ENDING_01_ANIM_H__
