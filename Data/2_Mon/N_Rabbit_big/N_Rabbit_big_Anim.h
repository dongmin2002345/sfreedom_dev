// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_RABBIT_BIG_ANIM_H__
#define N_RABBIT_BIG_ANIM_H__

namespace N_Rabbit_big_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        STANDUP_01              = 1000024,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_RABBIT_BIG_ANIM_H__
