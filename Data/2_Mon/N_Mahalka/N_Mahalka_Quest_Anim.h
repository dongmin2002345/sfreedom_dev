// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MAHALKA_QUEST_ANIM_H__
#define N_MAHALKA_QUEST_ANIM_H__

namespace N_Mahalka_Quest_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000061,
        ATTK_01_BTLIDLE         = 1000005,
        DASH_01                 = 1000211,
        IDLE_01                 = 1000000,
        JUMP_01                 = 1000251,
        JUMP_02                 = 1000252,
        JUMP_03                 = 1000254,
        JUMP_DASH_01            = 1000255,
        JUMP_IDLE_01            = 1000253,
        LADDER_01               = 1100101,
        LADDER_DOWN_01          = 1100102,
        LADDER_IDLE_01          = 1100103,
        WALK_01                 = 1000204
    };
}

#endif  // #ifndef N_MAHALKA_QUEST_ANIM_H__
