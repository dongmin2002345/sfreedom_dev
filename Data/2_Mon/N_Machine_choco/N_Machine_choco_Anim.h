// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MACHINE_CHOCO_ANIM_H__
#define N_MACHINE_CHOCO_ANIM_H__

namespace N_Machine_choco_Anim
{
    enum
    {
        BTLIDLE                 = 1000005,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201
    };
}

#endif  // #ifndef N_MACHINE_CHOCO_ANIM_H__
