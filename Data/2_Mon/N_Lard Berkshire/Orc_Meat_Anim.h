// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ORC_MEAT_ANIM_H__
#define ORC_MEAT_ANIM_H__

namespace Orc_Meat_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef ORC_MEAT_ANIM_H__
