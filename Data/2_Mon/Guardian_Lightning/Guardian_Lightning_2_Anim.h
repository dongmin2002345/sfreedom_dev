// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GUARDIAN_LIGHTNING_2_ANIM_H__
#define GUARDIAN_LIGHTNING_2_ANIM_H__

namespace Guardian_Lightning_2_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        OPENING_02              = 1000080
    };
}

#endif  // #ifndef GUARDIAN_LIGHTNING_2_ANIM_H__
