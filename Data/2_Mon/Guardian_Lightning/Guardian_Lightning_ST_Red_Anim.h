// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GUARDIAN_LIGHTNING_ST_RED_ANIM_H__
#define GUARDIAN_LIGHTNING_ST_RED_ANIM_H__

namespace Guardian_Lightning_ST_Red_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        IDLE_01                 = 1000000,
        OPENING_02              = 1000081,
        TRANS_BLUE              = 1000002
    };
}

#endif  // #ifndef GUARDIAN_LIGHTNING_ST_RED_ANIM_H__
