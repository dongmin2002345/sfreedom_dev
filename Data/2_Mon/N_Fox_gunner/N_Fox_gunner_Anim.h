// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_FOX_GUNNER_ANIM_H__
#define N_FOX_GUNNER_ANIM_H__

namespace N_Fox_gunner_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000061,
        SHOT_02                 = 1000062,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef N_FOX_GUNNER_ANIM_H__
