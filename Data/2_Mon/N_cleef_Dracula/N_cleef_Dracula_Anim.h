// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_CLEEF_DRACULA_ANIM_H__
#define N_CLEEF_DRACULA_ANIM_H__

namespace N_cleef_Dracula_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000026,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_01_IDLE             = 1000021,
        DIE_02                  = 1000022,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        RUN_01                  = 1000055,
        SKILL_01_ATTCK          = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTCK          = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTCK          = 1000206,
        SKILL_03_BTLIDLE        = 1000205,
        SKILL_04_ATTCK          = 1000207,
        SKILL_05_ATTCK          = 1000210,
        SKILL_05_BTLIDLE        = 1000209,
        SKILL_05_START          = 1000208,
        SKILL_06_ATTCK          = 1000212,
        SKILL_06_BTLIDLE        = 1000211,
        SKILL_07_ATTCK          = 1000214,
        SKILL_07_ATTCK_IDLE     = 1000215,
        SKILL_07_FINISH         = 1000216,
        SKLII_07_BTLIDLE        = 1000213,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_CLEEF_DRACULA_ANIM_H__
