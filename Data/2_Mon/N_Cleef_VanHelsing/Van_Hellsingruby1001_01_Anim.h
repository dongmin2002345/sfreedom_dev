// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_HELLSINGRUBY1001_01_ANIM_H__
#define VAN_HELLSINGRUBY1001_01_ANIM_H__

namespace Van_Hellsingruby1001_01_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef VAN_HELLSINGRUBY1001_01_ANIM_H__
