// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_OLM_LAVA_ANIM_H__
#define N_OLM_LAVA_ANIM_H__

namespace N_Olm_lava_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BTLIDLE_01              = 1000005,
        CLOSE_01                = 1000085,
        CLOSE_IDLE_01           = 1000086,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000071,
        SKILL_01_01             = 1000201,
        SKILL_01_02             = 1000202,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_OLM_LAVA_ANIM_H__
