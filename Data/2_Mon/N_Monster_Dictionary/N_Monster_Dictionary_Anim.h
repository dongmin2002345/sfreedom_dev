// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MONSTER_DICTIONARY_ANIM_H__
#define N_MONSTER_DICTIONARY_ANIM_H__

namespace N_Monster_Dictionary_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE                     = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01                = 1000201,
        SKILL_02_01             = 1000202,
        SKILL_02_02             = 1000203,
        SKILL_03_01             = 1000204,
        SKILL_03_02             = 1000205,
        SKILL_03_03             = 1000206,
        STANDUP_01              = 1000026
    };
}

#endif  // #ifndef N_MONSTER_DICTIONARY_ANIM_H__
