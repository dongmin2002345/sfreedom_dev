// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_HEATH_ANIM_H__
#define M_HEATH_ANIM_H__

namespace M_Heath_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_02              = 1000203,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_START        = 1000201,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000330,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000329,
        DASH_PIERCING_ATTK_FINISH = 1000332,
        DASH_PIERCING_ATTK_RUSH = 1000331,
        DASHATTK_01_BTLIDLE_01  = 1000444,
        DASHATTK_01_BTLIDLE_01_START = 1000443,
        DASHATTK_01_FINISH      = 1000447,
        DASHATTK_01_RUSH        = 1000446,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        OPENING_02              = 1000082,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000380,
        WHIRLWIND_01_FINISH     = 1000381,
        WHIRLWIND_BTLIDLE_01    = 1000379,
        WHIRLWIND_BTLIDLE_01_START = 1000378
    };
}

#endif  // #ifndef M_HEATH_ANIM_H__
