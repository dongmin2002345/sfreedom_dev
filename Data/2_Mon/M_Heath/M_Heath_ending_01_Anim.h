// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_HEATH_ENDING_01_ANIM_H__
#define M_HEATH_ENDING_01_ANIM_H__

namespace M_Heath_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000000
    };
}

#endif  // #ifndef M_HEATH_ENDING_01_ANIM_H__
