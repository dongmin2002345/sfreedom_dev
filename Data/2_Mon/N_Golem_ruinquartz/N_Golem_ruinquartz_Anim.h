// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_GOLEM_RUINQUARTZ_ANIM_H__
#define N_GOLEM_RUINQUARTZ_ANIM_H__

namespace N_Golem_ruinquartz_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_02              = 1000006,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000071,
        SHOT_02                 = 1000072,
        SKILL_01                = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef N_GOLEM_RUINQUARTZ_ANIM_H__
