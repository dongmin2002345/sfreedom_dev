// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_FIREWOOD_BUNYAN_PAUL_ANIM_H__
#define N_FIREWOOD_BUNYAN_PAUL_ANIM_H__

namespace N_Firewood_Bunyan_Paul_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000068,
        ATTK_01_BTLIDLE         = 1000067,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_01                  = 1000020,
        DIE_01_IDLE             = 1000021,
        DIE_02                  = 1000024,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        RUN_01                  = 1000051,
        SHOT_01_ATTACK          = 1000062,
        SHOT_01_ATTACK_FINISH   = 1000064,
        SHOT_01_ATTACK_IDLE     = 1000063,
        SHOT_01_BTLIDLE         = 1000061,
        SHOT_02_ATTACK          = 1000066,
        SHOT_02_BTLIDLE         = 1000065,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_01_FINISH         = 1000204,
        SKILL_01_START          = 1000202,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_FIREWOOD_BUNYAN_PAUL_ANIM_H__
