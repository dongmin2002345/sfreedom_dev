// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MUSHROOM_POISONOUS_BLUE_ANIM_H__
#define N_MUSHROOM_POISONOUS_BLUE_ANIM_H__

namespace N_Mushroom_Poisonous_blue_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000061,
        SKILL_01_01             = 1000201,
        SKILL_01_02             = 1000202
    };
}

#endif  // #ifndef N_MUSHROOM_POISONOUS_BLUE_ANIM_H__
