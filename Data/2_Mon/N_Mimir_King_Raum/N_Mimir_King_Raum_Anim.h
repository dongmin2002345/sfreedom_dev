// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MIMIR_KING_RAUM_ANIM_H__
#define N_MIMIR_KING_RAUM_ANIM_H__

namespace N_Mimir_King_Raum_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_01                  = 1000020,
        DIE_IDLE_02             = 1000025,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_02_ATTACK         = 1000209,
        SKILL_02_BTLIDLE        = 1000208,
        SKILL_02_FINISH         = 1000210,
        SKILL_02_START          = 1000207,
        SKILL_03_ATTACK         = 1000212,
        SKILL_03_BTLIDLE        = 1000211,
        SKILL_03_FINISH_01      = 1000213,
        SKILL_03_FINISH_02      = 1000214,
        SKILL_04_ATTACK         = 1000216,
        SKILL_04_BTLIDLE        = 1000215,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000024,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_MIMIR_KING_RAUM_ANIM_H__
