// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SEUKELRETON_MAGIC_ICE_ANIM_H__
#define N_SEUKELRETON_MAGIC_ICE_ANIM_H__

namespace N_Seukelreton_Magic_Ice_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000027,
        BLOWUP_LOOP             = 1000026,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SHOT_01                 = 1000071,
        STANDUP_01              = 1000021,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_SEUKELRETON_MAGIC_ICE_ANIM_H__
