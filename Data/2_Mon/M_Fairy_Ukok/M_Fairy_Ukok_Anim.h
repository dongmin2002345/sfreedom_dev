// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_FAIRY_UKOK_ANIM_H__
#define M_FAIRY_UKOK_ANIM_H__

namespace M_Fairy_Ukok_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTACK         = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTACK         = 1000206,
        SKILL_03_BTLIDLE        = 1000205
    };
}

#endif  // #ifndef M_FAIRY_UKOK_ANIM_H__
