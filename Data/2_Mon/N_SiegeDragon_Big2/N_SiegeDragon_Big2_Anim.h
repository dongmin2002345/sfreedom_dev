// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SIEGEDRAGON_BIG2_ANIM_H__
#define N_SIEGEDRAGON_BIG2_ANIM_H__

namespace N_SiegeDragon_Big2_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SKILL_01_FIRE           = 1000203,
        SKILL_01_READY          = 1000202,
        SKILL_01_SET            = 1000201,
        SKILL_02_01             = 1000204,
        SKILL_02_02             = 1000205,
        SKILL_02_03             = 1000206,
        SKILL_03_FIRE           = 1000209,
        SKILL_03_READY          = 1000208,
        SKILL_03_SET            = 1000207,
        SKILL_04_FIRE           = 1000212,
        SKILL_04_READY          = 1000211,
        SKILL_04_SET            = 1000210
    };
}

#endif  // #ifndef N_SIEGEDRAGON_BIG2_ANIM_H__
