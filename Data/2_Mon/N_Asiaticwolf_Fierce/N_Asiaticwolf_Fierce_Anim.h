// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ASIATICWOLF_FIERCE_ANIM_H__
#define N_ASIATICWOLF_FIERCE_ANIM_H__

namespace N_Asiaticwolf_Fierce_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        ATTK_03                 = 1000063,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_DOWN_01          = 1000032,
        BLOWUP_LOOP             = 1000022,
        BLOWUP_LOOP_01          = 1000031,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_02                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_BLENDING_01         = 1000030,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SKILL_VAGABOND_01       = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_ASIATICWOLF_FIERCE_ANIM_H__
