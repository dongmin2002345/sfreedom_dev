// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_PAREL_OPENING_CAM_ANIM_H__
#define M_PAREL_OPENING_CAM_ANIM_H__

namespace M_Parel_opening_cam_Anim
{
    enum
    {
        M_PAREL_OPENING_CAM     = 0
    };
}

#endif  // #ifndef M_PAREL_OPENING_CAM_ANIM_H__
