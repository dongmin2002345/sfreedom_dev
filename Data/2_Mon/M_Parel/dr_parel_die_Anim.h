// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DR_PAREL_DIE_ANIM_H__
#define DR_PAREL_DIE_ANIM_H__

namespace dr_parel_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef DR_PAREL_DIE_ANIM_H__
