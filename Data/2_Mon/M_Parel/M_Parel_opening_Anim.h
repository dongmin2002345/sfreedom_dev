// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_PAREL_OPENING_ANIM_H__
#define M_PAREL_OPENING_ANIM_H__

namespace M_Parel_opening_Anim
{
    enum
    {
        OPENING_01              = 1000000
    };
}

#endif  // #ifndef M_PAREL_OPENING_ANIM_H__
