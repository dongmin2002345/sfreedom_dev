// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_PAREL_CHAR_ROOT_ANIM_H__
#define M_PAREL_CHAR_ROOT_ANIM_H__

namespace M_Parel_char_root_Anim
{
    enum
    {
        ATTK_01                 = 1000000,
        ATTK_02                 = 1000001,
        ATTK_03                 = 1000009,
        BLOWUP_DOWN             = 1000027,
        BLOWUP_LOOP             = 1000026,
        DMG_01                  = 1000025,
        ENDING_01               = 1000022,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000002,
        IDLE_02                 = 1000003,
        IDLE_03                 = 1000004,
        INTRO_01                = 1000011,
        INTRO_02                = 1000012,
        INTRO_03                = 1000024,
        INTRO_04                = 1000023,
        KNOCKDOWN_01            = 1000005,
        KNOCKDOWN_IDLE_01       = 1000006,
        KNOCKDOWN_UP_01         = 1000007,
        RUN_01                  = 1000008,
        SKILL_02_01             = 1000201,
        SKILL_02_02             = 1000202,
        SKILL_02_03             = 1000203,
        SKILL_04_01             = 1000014,
        SKILL_04_02             = 1000015,
        SKILL_04_03             = 1000016,
        SKILL_05_01             = 1000019,
        SKILL_05_02             = 1000020,
        SKILL_05_03             = 1000021,
        STUN_01                 = 1000018
    };
}

#endif  // #ifndef M_PAREL_CHAR_ROOT_ANIM_H__
