// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WOODENDOLL1002_01_ANIM_H__
#define WOODENDOLL1002_01_ANIM_H__

namespace WoodenDoll1002_01_Anim
{
    enum
    {
        DMG_01                  = 0,
        DMG_02                  = 1,
        DMG_03                  = 2
    };
}

#endif  // #ifndef WOODENDOLL1002_01_ANIM_H__
