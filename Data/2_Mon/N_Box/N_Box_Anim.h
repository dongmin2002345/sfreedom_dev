// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_BOX_ANIM_H__
#define N_BOX_ANIM_H__

namespace N_Box_Anim
{
    enum
    {
        N_BOX_BREAK             = 1000000,
        N_BOX_IDLE              = 1000001,
        N_BOX_SHAKE             = 1000002
    };
}

#endif  // #ifndef N_BOX_ANIM_H__
