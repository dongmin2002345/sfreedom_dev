// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_CENTAURUS_MISSION04_ANIM_H__
#define N_CENTAURUS_MISSION04_ANIM_H__

namespace N_Centaurus_Mission04_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_02                  = 1000021,
        DIE_IDLE_01             = 1000027,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        RUN_01                  = 1000051,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_02_ATTACK         = 1000208,
        SKILL_02_START          = 1000207,
        SKILL_03                = 1000209,
        SKILL_04_ATTACK         = 1000211,
        SKILL_04_BTLIDLE        = 1000210,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_CENTAURUS_MISSION04_ANIM_H__
