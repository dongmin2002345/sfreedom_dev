// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SAMBHAVA_ANIM_H__
#define N_SAMBHAVA_ANIM_H__

namespace N_Sambhava_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000068,
        ATTK_01_BTLIDLE         = 1000067,
        ATTK_02_ATTACK          = 1000070,
        ATTK_02_BTLIDLE         = 1000069,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_01                  = 1000020,
        DIE_02                  = 1000024,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DMG_01                  = 1000011,
        DMG_03                  = 1000013,
        DMG_04                  = 1000014,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_02_ATTACK         = 1000208,
        SKILL_02_BTLIDLE        = 1000207,
        SKILL_03_BTLIDLE        = 1000210,
        SKILL_03_FINISH         = 1000211,
        SKILL_03_START          = 1000209,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        TALK_01                 = 1000005,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_SAMBHAVA_ANIM_H__
