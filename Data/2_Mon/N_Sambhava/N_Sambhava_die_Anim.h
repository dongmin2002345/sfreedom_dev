// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SAMBHAVA_DIE_ANIM_H__
#define N_SAMBHAVA_DIE_ANIM_H__

namespace N_Sambhava_die_Anim
{
    enum
    {
        DIEAFTER_IDLE_01        = 1000027,
        DIEAFTER_OPENING_01     = 1000026
    };
}

#endif  // #ifndef N_SAMBHAVA_DIE_ANIM_H__
