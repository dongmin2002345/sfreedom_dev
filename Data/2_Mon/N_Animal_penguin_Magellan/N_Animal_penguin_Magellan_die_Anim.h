// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ANIMAL_PENGUIN_MAGELLAN_DIE_ANIM_H__
#define N_ANIMAL_PENGUIN_MAGELLAN_DIE_ANIM_H__

namespace N_Animal_penguin_Magellan_die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef N_ANIMAL_PENGUIN_MAGELLAN_DIE_ANIM_H__
