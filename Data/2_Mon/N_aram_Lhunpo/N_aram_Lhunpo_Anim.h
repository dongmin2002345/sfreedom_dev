// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ARAM_LHUNPO_ANIM_H__
#define N_ARAM_LHUNPO_ANIM_H__

namespace N_aram_Lhunpo_Anim
{
    enum
    {
        ATTK_01_ATTCK           = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BLOWUP_STANDUP          = 1000024,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000012,
        DMG_02                  = 1000013,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SKILL_01_ATTCK          = 1000201
    };
}

#endif  // #ifndef N_ARAM_LHUNPO_ANIM_H__
