// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAGICCANNON1001_01_ANIM_H__
#define MAGICCANNON1001_01_ANIM_H__

namespace MagicCannon1001_01_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        CAST_01                 = 1000201,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        SHOT_01                 = 1000202
    };
}

#endif  // #ifndef MAGICCANNON1001_01_ANIM_H__
