// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_ARAM_ANIM_H__
#define B_ARAM_ANIM_H__

namespace B_Aram_Anim
{
    enum
    {
        ATTK_01_01              = 1000061,
        ATTK_01_02              = 1000062,
        ATTK_01_03              = 1000063,
        ATTK_01_04              = 1000064,
        ATTK_02_01              = 1000065,
        ATTK_02_02              = 1000066,
        ATTK_02_03              = 1000067,
        ATTK_02_04              = 1000068,
        DIE_01                  = 1000020,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        SKILL_01_01             = 1000205,
        SKILL_01_02             = 1000206,
        SKILL_01_03             = 1000207,
        SKILL_01_04             = 1000208,
        SKILL_02_01             = 1000211,
        SKILL_02_02             = 1000212,
        SKILL_02_03             = 1000213,
        SKILL_02_04             = 1000214,
        SKILL_03_01             = 1000209,
        SKILL_03_02             = 1000210,
        SKILL_04_01             = 1000201,
        SKILL_04_02             = 1000202
    };
}

#endif  // #ifndef B_ARAM_ANIM_H__
