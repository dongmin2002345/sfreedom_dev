// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_WOODENDOLL2001_01_ANIM_H__
#define N_WOODENDOLL2001_01_ANIM_H__

namespace N_WoodenDoll2001_01_Anim
{
    enum
    {
        DMG_01                  = 1,
        DMG_02                  = 2,
        DMG_03                  = 0
    };
}

#endif  // #ifndef N_WOODENDOLL2001_01_ANIM_H__
