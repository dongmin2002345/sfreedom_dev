// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_ANUKUS_ENDING_ANIM_H__
#define B_ANUKUS_ENDING_ANIM_H__

namespace B_Anukus_ending_Anim
{
    enum
    {
        ENDING_01               = 1000000
    };
}

#endif  // #ifndef B_ANUKUS_ENDING_ANIM_H__
