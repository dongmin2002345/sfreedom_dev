// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_DARK_CHEF_OPENING_01_ANIM_H__
#define M_DARK_CHEF_OPENING_01_ANIM_H__

namespace M_Dark_Chef_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000082
    };
}

#endif  // #ifndef M_DARK_CHEF_OPENING_01_ANIM_H__
