// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_BAJEAT_ANIM_H__
#define M_BAJEAT_ANIM_H__

namespace M_Bajeat_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000062,
        ATTK_01_BTLIDLE         = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        RUN_01                  = 1000055,
        SHOT_01_ATTACK          = 1000064,
        SHOT_01_BTLIDLE         = 1000063,
        SKILL_01_ATTACK         = 1000066,
        SKILL_01_BTLIDLE        = 1000065,
        SKILL_02_ATTACK         = 1000069,
        SKILL_02_BTLIDLE        = 1000068,
        SKILL_02_START          = 1000067,
        SKILL_03_ATTACK         = 1000072,
        SKILL_03_BTLIDLE        = 1000071,
        SKILL_03_START          = 1000070,
        SKILL_04_ATTACK         = 1000075,
        SKILL_04_BTLIDLE        = 1000074,
        SKILL_04_START          = 1000073,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef M_BAJEAT_ANIM_H__
