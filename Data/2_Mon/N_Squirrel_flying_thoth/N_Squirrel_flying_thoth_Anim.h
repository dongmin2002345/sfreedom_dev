// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SQUIRREL_FLYING_THOTH_ANIM_H__
#define N_SQUIRREL_FLYING_THOTH_ANIM_H__

namespace N_Squirrel_flying_thoth_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTK_01        = 1000202,
        SKILL_01_BTLIDLE_01     = 1000201,
        SKILL_01_FINISH_01      = 1000203,
        SKILL_02_ATTK_01        = 1000206,
        SKILL_02_BTLIDLE_01     = 1000205,
        SKILL_02_FINISH_01      = 1000207,
        SKILL_02_START_01       = 1000204,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef N_SQUIRREL_FLYING_THOTH_ANIM_H__
