// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SHADOW_MAN_ANIM_H__
#define N_SHADOW_MAN_ANIM_H__

namespace N_Shadow_Man_Anim
{
    enum
    {
        ATTK_01                 = 1000062,
        ATTK_02                 = 1000064,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        CLOSE_IDLE_01           = 1000086,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SHOT_01                 = 1000071,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_SHADOW_MAN_ANIM_H__
