// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_GREED_DOYEN_ATTLS_ANIM_H__
#define N_GREED_DOYEN_ATTLS_ANIM_H__

namespace N_Greed_Doyen_Attls_Anim
{
    enum
    {
        ATTK_01_01              = 1000061,
        ATTK_01_02              = 1000062,
        ATTK_02_01              = 1000063,
        ATTK_02_02              = 1000064,
        ATTK_03_01              = 1000065,
        ATTK_03_02              = 1000066,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_02                  = 1000021,
        DIE_IDLE_01             = 1000026,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        OPENING_02              = 1000082,
        RUN_01                  = 1000051,
        SHOT_03_01              = 1000073,
        SHOT_03_02              = 1000074,
        SHOT_04_01              = 1000075,
        SHOT_04_02              = 1000076,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_GREED_DOYEN_ATTLS_ANIM_H__
