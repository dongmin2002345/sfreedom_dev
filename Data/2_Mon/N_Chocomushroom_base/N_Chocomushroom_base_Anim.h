// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_CHOCOMUSHROOM_BASE_ANIM_H__
#define N_CHOCOMUSHROOM_BASE_ANIM_H__

namespace N_Chocomushroom_base_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef N_CHOCOMUSHROOM_BASE_ANIM_H__
