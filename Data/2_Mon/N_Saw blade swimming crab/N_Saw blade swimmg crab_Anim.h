// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SAW_BLADE_SWIMMG_CRAB_ANIM_H__
#define N_SAW_BLADE_SWIMMG_CRAB_ANIM_H__

namespace N_Saw_blade_swimmg_crab_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BLOWUP_START            = 1000021,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef N_SAW_BLADE_SWIMMG_CRAB_ANIM_H__
