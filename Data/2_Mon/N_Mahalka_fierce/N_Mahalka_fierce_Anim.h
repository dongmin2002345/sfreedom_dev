// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MAHALKA_FIERCE_ANIM_H__
#define N_MAHALKA_FIERCE_ANIM_H__

namespace N_Mahalka_fierce_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000068,
        ATTK_01_BTLIDLE         = 1000067,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_01                  = 1000020,
        DIE_02                  = 1000024,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_03                  = 1000013,
        DMG_04                  = 1000014,
        DMG_BLENDING_01         = 1000015,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000205,
        SKILL_01_ATTACK_BTLIDLE = 1000211,
        SKILL_01_BTLIDLE        = 1000204,
        SKILL_01_FINISH         = 1000206,
        SKILL_02_ATTACK         = 1000208,
        SKILL_02_BTLIDLE        = 1000207,
        SKILL_03_ATTACK         = 1000210,
        SKILL_03_BTLIDLE        = 1000209,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef N_MAHALKA_FIERCE_ANIM_H__
