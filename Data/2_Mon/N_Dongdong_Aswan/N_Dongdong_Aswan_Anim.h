// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_DONGDONG_ASWAN_ANIM_H__
#define N_DONGDONG_ASWAN_ANIM_H__

namespace N_Dongdong_Aswan_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SHOT_01                 = 1000071,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_DONGDONG_ASWAN_ANIM_H__
