// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SANDBAG_ANIM_H__
#define N_SANDBAG_ANIM_H__

namespace N_SandBag_Anim
{
    enum
    {
        DMG_01                  = 0
    };
}

#endif  // #ifndef N_SANDBAG_ANIM_H__
