// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_CLEEF_GATEKEEPER_DIE_ANIM_H__
#define N_CLEEF_GATEKEEPER_DIE_ANIM_H__

namespace N_Cleef_gatekeeper_die_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        DIE_AFTER               = 1000022,
        DIE_IDLE_01             = 1000023
    };
}

#endif  // #ifndef N_CLEEF_GATEKEEPER_DIE_ANIM_H__
