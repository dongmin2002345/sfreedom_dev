// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_DEATH_MASTER_GHOST_ANIM_H__
#define M_DEATH_MASTER_GHOST_ANIM_H__

namespace M_Death_master_ghost_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000066,
        ATTK_01_BTLIDLE         = 1000065,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        DIE_BLENDING_01         = 1000021,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_ATTACK_BTLIDLE = 1000216,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_01_FINISH         = 1000207,
        SKILL_02_ATTACK         = 1000210,
        SKILL_02_BTLIDLE        = 1000209,
        SKILL_02_START          = 1000208,
        SKILL_03_ATTACK         = 1000211,
        SKILL_03_FINISH         = 1000212,
        SKILL_03_LOOP           = 1000215,
        SKILL_04_ATTACK         = 1000214,
        SKILL_04_BTLIDLE        = 1000213,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef M_DEATH_MASTER_GHOST_ANIM_H__
