// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SITA_ANIM_H__
#define N_SITA_ANIM_H__

namespace N_Sita_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000066,
        ATTK_01_BTLIDLE         = 1000065,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        RUN_01                  = 1000055,
        SHOT_01_ATTACK          = 1000068,
        SHOT_01_BTLIDLE         = 1000067,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_02_ATTACK         = 1000208,
        SKILL_02_BTLIDLE        = 1000207,
        SKILL_03_ATTACK         = 1000209,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_SITA_ANIM_H__
