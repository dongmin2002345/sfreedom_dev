// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_FIREWOOD_BUNYAN_PAUL_MISSION04_DIE_ANIM_H__
#define N_FIREWOOD_BUNYAN_PAUL_MISSION04_DIE_ANIM_H__

namespace N_Firewood_Bunyan_Paul_Mission04_die_Anim
{
    enum
    {
        DIE_02                  = 1000027,
        DIE_03                  = 1000028,
        DIE_IDLE_01             = 1000029
    };
}

#endif  // #ifndef N_FIREWOOD_BUNYAN_PAUL_MISSION04_DIE_ANIM_H__
