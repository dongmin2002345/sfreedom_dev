// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_FIREWOOD_BUNYAN_PAUL_WOOD_ANIM_H__
#define N_FIREWOOD_BUNYAN_PAUL_WOOD_ANIM_H__

namespace N_Firewood_Bunyan_Paul_wood_Anim
{
    enum
    {
        ATTK_01                 = 1000061
    };
}

#endif  // #ifndef N_FIREWOOD_BUNYAN_PAUL_WOOD_ANIM_H__
