// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SAND_ALMOND_ANIM_H__
#define N_SAND_ALMOND_ANIM_H__

namespace N_Sand_almond_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        RUN_01                  = 1000051,
        SHOT_02                 = 1000071,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_SAND_ALMOND_ANIM_H__
