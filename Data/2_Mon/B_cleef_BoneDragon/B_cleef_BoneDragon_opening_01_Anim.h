// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_CLEEF_BONEDRAGON_OPENING_01_ANIM_H__
#define B_CLEEF_BONEDRAGON_OPENING_01_ANIM_H__

namespace B_cleef_BoneDragon_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000000
    };
}

#endif  // #ifndef B_CLEEF_BONEDRAGON_OPENING_01_ANIM_H__
