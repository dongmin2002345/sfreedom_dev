// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_CLEEF_BONEDRAGON_ANIM_H__
#define B_CLEEF_BONEDRAGON_ANIM_H__

namespace B_cleef_BoneDragon_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        ATTK_03                 = 1000063,
        BTLIDLE                 = 1000005,
        DIE_01                  = 1000021,
        DIE_IDLE_01             = 1000020,
        SKILL_01_ATTCK          = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTCK          = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_FINISH         = 1000226,
        SKILL_02_LOOP           = 1000225,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTCK          = 1000211,
        SKILL_03_ATTCK_02       = 1000224,
        SKILL_03_BTLIDLE        = 1000210,
        SKILL_03_FINISH         = 1000213,
        SKILL_03_IDLE           = 1000208,
        SKILL_03_RUN            = 1000209,
        SKILL_03_START          = 1000207,
        SKILL_04_ATTCK          = 1000216,
        SKILL_04_BTLIDLE        = 1000215,
        SKILL_04_START          = 1000214,
        SKILL_05_ATTCK          = 1000219,
        SKILL_05_BTLIDLE        = 1000218,
        SKILL_05_FINISH         = 1000220,
        SKILL_05_START          = 1000217,
        SKILL_06_ATTCK          = 1000223,
        SKILL_06_BTLIDLE        = 1000222,
        SKILL_06_FINISH         = 1000227,
        SKILL_06_START          = 1000221
    };
}

#endif  // #ifndef B_CLEEF_BONEDRAGON_ANIM_H__
