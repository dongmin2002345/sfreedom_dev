// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_HOVERHYENA_DIGGER_DIE_ANIM_H__
#define N_HOVERHYENA_DIGGER_DIE_ANIM_H__

namespace N_Hoverhyena_Digger_die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef N_HOVERHYENA_DIGGER_DIE_ANIM_H__
