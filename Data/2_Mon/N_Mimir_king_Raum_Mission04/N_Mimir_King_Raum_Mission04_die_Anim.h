// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MIMIR_KING_RAUM_MISSION04_DIE_ANIM_H__
#define N_MIMIR_KING_RAUM_MISSION04_DIE_ANIM_H__

namespace N_Mimir_King_Raum_Mission04_die_Anim
{
    enum
    {
        DIE_IDLE_01             = 1000021
    };
}

#endif  // #ifndef N_MIMIR_KING_RAUM_MISSION04_DIE_ANIM_H__
