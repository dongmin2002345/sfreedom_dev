// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GUARDIAN_GAIA_1_ANIM_H__
#define GUARDIAN_GAIA_1_ANIM_H__

namespace Guardian_Gaia_1_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        EARTHQUAKE_01           = 1000387,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef GUARDIAN_GAIA_1_ANIM_H__
