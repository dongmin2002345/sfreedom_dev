// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_DRAGONSAPPHIRE_SAPPHIRE_05_ANIM_H__
#define B_DRAGONSAPPHIRE_SAPPHIRE_05_ANIM_H__

namespace b_dragonsapphire_sapphire_05_Anim
{
    enum
    {
        ATTK_01_01              = 0,
        ATTK_01_02              = 1,
        ATTK_01_03              = 17,
        ATTK_02_01              = 2,
        ATTK_02_02              = 3,
        ATTK_02_03              = 18,
        ATTK_03_01              = 4,
        ATTK_03_02              = 5,
        ATTK_03_03              = 21,
        ATTK_04_01              = 6,
        ATTK_04_02              = 7,
        ATTK_04_03              = 8,
        ATTK_05_01              = 9,
        ATTK_05_02              = 10,
        ATTK_05_03              = 11,
        ATTK_06                 = 12,
        ATTK_07                 = 22,
        DIE_01                  = 13,
        DMG_01                  = 14,
        IDLE_01                 = 15,
        OPENING_01              = 16,
        TURN_L                  = 19,
        TURN_R                  = 20
    };
}

#endif  // #ifndef B_DRAGONSAPPHIRE_SAPPHIRE_05_ANIM_H__
