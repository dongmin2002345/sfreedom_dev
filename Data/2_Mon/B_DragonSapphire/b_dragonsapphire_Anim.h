// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_DRAGONSAPPHIRE_ANIM_H__
#define B_DRAGONSAPPHIRE_ANIM_H__

namespace b_dragonsapphire_Anim
{
    enum
    {
        ATTK_01_01              = 1000000,
        ATTK_01_02              = 1000001,
        ATTK_01_03              = 1000017,
        ATTK_02_01              = 1000002,
        ATTK_02_02              = 1000003,
        ATTK_02_03              = 1000018,
        ATTK_03_01              = 1000004,
        ATTK_03_02              = 1000005,
        ATTK_03_03              = 1000021,
        ATTK_04_01              = 1000006,
        ATTK_04_02              = 1000007,
        ATTK_04_03              = 1000008,
        ATTK_05_01              = 1000009,
        ATTK_05_02              = 1000010,
        ATTK_05_03              = 1000011,
        ATTK_06                 = 1000012,
        ATTK_07                 = 1000022,
        DIE_01                  = 1000013,
        DMG_01                  = 1000014,
        IDLE_01                 = 1000015,
        OPENING_01              = 1000016,
        TURN_L                  = 1000019,
        TURN_R                  = 1000020
    };
}

#endif  // #ifndef B_DRAGONSAPPHIRE_ANIM_H__
