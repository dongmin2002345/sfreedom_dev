// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_STONE_SCORPION_ANTARES_ANIM_H__
#define N_STONE_SCORPION_ANTARES_ANIM_H__

namespace N_Stone_Scorpion_Antares_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        DIE_01                  = 1000020,
        DIE_01_LOOP             = 1000021,
        DIE_FINISH              = 1000019,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING            = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_01_FINISH         = 1000207,
        SKILL_02_ATTACK         = 1000209,
        SKILL_02_BTLIDLE        = 1000208,
        SKILL_03_ATTACK         = 1000211,
        SKILL_03_BTLIDLE        = 1000210,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef N_STONE_SCORPION_ANTARES_ANIM_H__
