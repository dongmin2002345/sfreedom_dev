// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SITA_MAN_JACO_ANIM_H__
#define N_SITA_MAN_JACO_ANIM_H__

namespace N_Sita_Man_jaco_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000063,
        SKILL_01                = 1000201,
        SKILL_02_01             = 1000202,
        SKILL_02_02             = 1000203,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef N_SITA_MAN_JACO_ANIM_H__
