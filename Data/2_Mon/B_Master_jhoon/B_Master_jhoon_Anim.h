// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_MASTER_JHOON_ANIM_H__
#define B_MASTER_JHOON_ANIM_H__

namespace B_Master_jhoon_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLILDE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTACK         = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTACK         = 1000207,
        SKILL_03_BTLIDLE        = 1000206,
        SKILL_03_START          = 1000205,
        SKILL_04_ATTACK         = 1000209,
        SKILL_04_FINISH         = 1000210,
        SKILL_04_START          = 1000208,
        SKILL_05_ATTACK         = 1000212,
        SKILL_05_BTLIDLE        = 1000211,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef B_MASTER_JHOON_ANIM_H__
