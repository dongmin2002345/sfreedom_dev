// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_SIEGEDRAGON_BIG_ANIM_H__
#define N_SIEGEDRAGON_BIG_ANIM_H__

namespace N_SiegeDragon_Big_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        RUN_01                  = 1000051,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTACK         = 1000209,
        SKILL_03_BTLIDLE        = 1000208,
        SKILL_03_START          = 1000207,
        SKILL_04_ATTACK         = 1000210,
        SKILL_04_FINISH         = 1000212,
        SKILL_05_ATTACK         = 1000211
    };
}

#endif  // #ifndef N_SIEGEDRAGON_BIG_ANIM_H__
