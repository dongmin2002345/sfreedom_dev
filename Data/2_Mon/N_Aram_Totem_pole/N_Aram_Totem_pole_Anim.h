// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_ARAM_TOTEM_POLE_ANIM_H__
#define N_ARAM_TOTEM_POLE_ANIM_H__

namespace N_Aram_Totem_pole_Anim
{
    enum
    {
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01_ATTK            = 1000202,
        SHOT_01_BTLIDLE         = 1000201,
        SHOT_02_ATTK            = 1000204,
        SHOT_02_BTLIDLE         = 1000203,
        SHOT_03_ATTK            = 1000206,
        SHOT_03_BTLIDLE         = 1000205,
        SHOT_04_ATTK            = 1000208,
        SHOT_04_BTLIDLE         = 1000207,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef N_ARAM_TOTEM_POLE_ANIM_H__
