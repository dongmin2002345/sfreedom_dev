// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_BAULK_ANIM_H__
#define N_BAULK_ANIM_H__

namespace N_Baulk_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        ATTK_02                 = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01_01              = 1000312,
        SHOT_01_02              = 1000314,
        SHOT_01_BTLIDLE         = 1000313,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef N_BAULK_ANIM_H__
