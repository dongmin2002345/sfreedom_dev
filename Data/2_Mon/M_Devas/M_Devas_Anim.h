// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef M_DEVAS_ANIM_H__
#define M_DEVAS_ANIM_H__

namespace M_Devas_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef M_DEVAS_ANIM_H__
