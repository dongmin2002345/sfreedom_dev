// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_01_ANIM_H__
#define ELGA_01_ANIM_H__

namespace Elga_01_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT_START   = 1000451,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000452,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000330,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000329,
        DASH_PIERCING_ATTK_FINISH = 1000332,
        DASH_PIERCING_ATTK_RUSH = 1000331,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FURY_01                 = 1000900,
        JUMPDOWNCRASH_DIAG_DOWN = 1000412,
        JUMPDOWNCRASH_DIAG_GETUP = 1000113,
        JUMPDOWNCRASH_DIAG_UP   = 1000411,
        RUN_01                  = 1000055,
        SHOT_AROUND_01          = 1000419,
        SHOT_AROUND_01_FIRE     = 1000420,
        SHOT_AROUND_01_START    = 1000418,
        SHOT_RETURN_01          = 1000454,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        SKILL_SHOT_01           = 1000453,
        STUN_01                 = 1000025,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355,
        TELEPORT_SKILL_01       = 1000450,
        TELEPORT_SKILL_01_BTLIDLE = 1000449,
        TELEPORT_SKILL_01_START = 1000448
    };
}

#endif  // #ifndef ELGA_01_ANIM_H__
