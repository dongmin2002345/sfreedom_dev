// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_EGG_ANIM_H__
#define ELGA_EGG_ANIM_H__

namespace Elga_Egg_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        OPEING_01               = 1000081
    };
}

#endif  // #ifndef ELGA_EGG_ANIM_H__
