// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef THAILDOM_ANIM_H__
#define THAILDOM_ANIM_H__

namespace Thaildom_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef THAILDOM_ANIM_H__
