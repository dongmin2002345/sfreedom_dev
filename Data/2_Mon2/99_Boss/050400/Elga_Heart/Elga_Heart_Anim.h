// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_HEART_ANIM_H__
#define ELGA_HEART_ANIM_H__

namespace Elga_Heart_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        SKILL_01                = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02                = 1000204,
        SKILL_02_BTLIDLE        = 1000203
    };
}

#endif  // #ifndef ELGA_HEART_ANIM_H__
