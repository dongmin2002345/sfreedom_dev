// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_STONE_ANIM_H__
#define ELGA_STONE_ANIM_H__

namespace Elga_Stone_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202
    };
}

#endif  // #ifndef ELGA_STONE_ANIM_H__
