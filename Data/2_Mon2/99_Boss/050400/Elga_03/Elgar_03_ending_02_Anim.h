// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGAR_03_ENDING_02_ANIM_H__
#define ELGAR_03_ENDING_02_ANIM_H__

namespace Elgar_03_ending_02_Anim
{
    enum
    {
        ENDING_05               = 1000005,
        ENDING_06               = 1000006,
        ENDING_07               = 1000007
    };
}

#endif  // #ifndef ELGAR_03_ENDING_02_ANIM_H__
