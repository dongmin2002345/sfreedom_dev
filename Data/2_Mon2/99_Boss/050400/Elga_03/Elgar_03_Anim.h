// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGAR_03_ANIM_H__
#define ELGAR_03_ANIM_H__

namespace Elgar_03_Anim
{
    enum
    {
        ATTACK_LOOP_01          = 1000333,
        ATTK_ATTER_LOOP_01      = 1000334,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTIDLE_LOOP_01_START    = 1000330,
        BTLIDLE_01_SHOT_START   = 1000451,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000452,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_LOOP_01         = 1000332,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        ENERGY_EXPLOSION_02     = 1000377,
        ENERGY_EXPLOSION_02_BTIDLE = 1000379,
        ENERGY_EXPLOSION_03     = 1000378,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FINISH_LOOP_01          = 1000335,
        FURY_01                 = 1000900,
        GROGGY_FINISH_01        = 1000403,
        GROGGY_IDLE_01          = 1000402,
        IDLE_01                 = 1000001,
        MASSIVE_PROJECTILE_BTLIDLE = 1000342,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000341,
        MASSIVE_PROJECTILE_FINISH = 1000344,
        MASSIVE_PROJECTILE_FIRE = 1000343,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE = 1000338,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE_START = 1000337,
        MASSIVE_PROJECTILE_JUMP_FINISH = 1000340,
        MASSIVE_PROJECTILE_JUMP_FIRE = 1000339,
        SHOT_AROUND_01          = 1000419,
        SHOT_AROUND_01_FIRE     = 1000420,
        SHOT_AROUND_01_START    = 1000418,
        SHOT_RETURN_01          = 1000454,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_SHOT_01           = 1000453,
        SUMMON_DARK_EYE         = 1000358,
        SUMMON_DARK_EYE_FINISH  = 1000359,
        SUMMON_DARK_EYE_START   = 1000357,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355
    };
}

#endif  // #ifndef ELGAR_03_ANIM_H__
