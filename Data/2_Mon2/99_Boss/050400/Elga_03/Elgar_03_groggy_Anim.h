// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGAR_03_GROGGY_ANIM_H__
#define ELGAR_03_GROGGY_ANIM_H__

namespace Elgar_03_groggy_Anim
{
    enum
    {
        GROGGY_SCENE_01         = 1000400,
        GROGGY_SCENE_02         = 1000401
    };
}

#endif  // #ifndef ELGAR_03_GROGGY_ANIM_H__
