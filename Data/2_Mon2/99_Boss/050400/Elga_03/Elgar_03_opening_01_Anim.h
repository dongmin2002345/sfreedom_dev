// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGAR_03_OPENING_01_ANIM_H__
#define ELGAR_03_OPENING_01_ANIM_H__

namespace Elgar_03_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000082,
        OPENING_02              = 1000083
    };
}

#endif  // #ifndef ELGAR_03_OPENING_01_ANIM_H__
