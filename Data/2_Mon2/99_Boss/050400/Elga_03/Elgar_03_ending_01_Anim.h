// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGAR_03_ENDING_01_ANIM_H__
#define ELGAR_03_ENDING_01_ANIM_H__

namespace Elgar_03_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000001,
        ENDING_02               = 1000002,
        ENDING_03               = 1000003,
        ENDING_04               = 1000004
    };
}

#endif  // #ifndef ELGAR_03_ENDING_01_ANIM_H__
