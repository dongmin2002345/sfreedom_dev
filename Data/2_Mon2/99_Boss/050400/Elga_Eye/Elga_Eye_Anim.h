// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_EYE_ANIM_H__
#define ELGA_EYE_ANIM_H__

namespace Elga_Eye_Anim
{
    enum
    {
        DIE                     = 1000020,
        DIE_02                  = 1000021,
        DMG_01                  = 1000011,
        IDLE                    = 1000001,
        OPENING                 = 1000081,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202,
        SKILL_03                = 1000203
    };
}

#endif  // #ifndef ELGA_EYE_ANIM_H__
