// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_02_ANIM_H__
#define ELGA_02_ANIM_H__

namespace Elga_02_Anim
{
    enum
    {
        BREATH_RETURN_01        = 1000454,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_BREATH_START = 1000451,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_BREATH = 1000452,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        CROSSATTACK_01_FIRE     = 1000397,
        CROSSATTACK_01_IDLE     = 1000396,
        CROSSATTACK_01_START    = 1000395,
        CROSSATTACK_02_FIRE     = 1000402,
        CROSSATTACK_02_IDLE     = 1000401,
        CROSSATTACK_02_START    = 1000400,
        DASH_AFTER_ATTK_01_02   = 1000417,
        DASH_AFTER_ATTK_BTLIDLE_01_IDLE = 1000415,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_RUSH    = 1000416,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FURY_01                 = 1000900,
        MASSIVE_PROJECTILE_BTLIDLE = 1000339,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000338,
        MASSIVE_PROJECTILE_DOWN_START = 1000344,
        MASSIVE_PROJECTILE_JUMP = 1000340,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE = 1000341,
        MASSIVE_PROJECTILE_JUMP_FINISH = 1000343,
        MASSIVE_PROJECTILE_JUMP_FIRE = 1000342,
        MASSIVE_PROJECTILE_STANDUP = 1000345,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        SKILL_BREATH_01         = 1000453,
        SKILL_SHOT_01           = 1000312,
        STUN_01                 = 1000025,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355
    };
}

#endif  // #ifndef ELGA_02_ANIM_H__
