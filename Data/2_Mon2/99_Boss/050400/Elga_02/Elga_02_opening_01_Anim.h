// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_02_OPENING_01_ANIM_H__
#define ELGA_02_OPENING_01_ANIM_H__

namespace Elga_02_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000001,
        OPENING_02              = 1000002
    };
}

#endif  // #ifndef ELGA_02_OPENING_01_ANIM_H__
