// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_BOX_ANIM_H__
#define ELGA_BOX_ANIM_H__

namespace Elga_Box_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DIE_02                  = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef ELGA_BOX_ANIM_H__
