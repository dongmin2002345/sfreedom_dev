// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTSOUL_AVIS_ANIM_H__
#define FROSTSOUL_AVIS_ANIM_H__

namespace Frostsoul_Avis_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_01_START        = 1000201,
        ATTK_01_02              = 1000203,
        ATTK_01_BTLIDLE         = 1000005,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK   = 1000305,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_ATTK_START = 1000304,
        BTLIDLE_02              = 1000006,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        CROSSATTACK_01_FIRE     = 1000397,
        CROSSATTACK_01_IDLE     = 1000396,
        CROSSATTACK_01_START    = 1000395,
        DIE_BLENDING            = 1000021,
        DMG_BLOWUP              = 1000014,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        SHOT_AROUND_01          = 1000419,
        SHOT_AROUND_01_FIRE     = 1000420,
        SHOT_AROUND_01_START    = 1000418,
        SKILL_ATTK_01           = 1000306,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_03           = 1000309,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef FROSTSOUL_AVIS_ANIM_H__
