// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef REGENO_ICEDRAGON_OPENING_01_ANIM_H__
#define REGENO_ICEDRAGON_OPENING_01_ANIM_H__

namespace Regeno_IceDragon_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000082
    };
}

#endif  // #ifndef REGENO_ICEDRAGON_OPENING_01_ANIM_H__
