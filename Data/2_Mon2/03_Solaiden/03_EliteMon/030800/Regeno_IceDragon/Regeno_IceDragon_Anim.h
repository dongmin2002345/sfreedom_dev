// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef REGENO_ICEDRAGON_ANIM_H__
#define REGENO_ICEDRAGON_ANIM_H__

namespace Regeno_IceDragon_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        ATTK_05_01              = 1000214,
        ATTK_05_01_START        = 1000213,
        ATTK_05_02              = 1000215,
        BREATH_01               = 1000347,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DIE_01                  = 1000020,
        FURY_01                 = 1000900,
        OPENING_01              = 1000082,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_SHOT_01           = 1000312,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355,
        SUMMONBONE_BATTLEIDLE   = 1000358,
        SUMMONBONE_BATTLEIDLE_START = 1000357,
        SUMMONBONE_FIRE         = 1000359
    };
}

#endif  // #ifndef REGENO_ICEDRAGON_ANIM_H__
