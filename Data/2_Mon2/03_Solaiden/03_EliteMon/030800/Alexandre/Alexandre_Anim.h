// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ALEXANDRE_ANIM_H__
#define ALEXANDRE_ANIM_H__

namespace Alexandre_Anim
{
    enum
    {
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DASH_AFTER_ATTK_01_02   = 1000416,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_RUSH    = 1000415,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000330,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000329,
        DASH_PIERCING_ATTK_FINISH = 1000332,
        DASH_PIERCING_ATTK_RUSH = 1000331,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000082,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMONBONE_BATTLEIDLE   = 1000358,
        SUMMONBONE_BATTLEIDLE_START = 1000357,
        SUMMONBONE_FIRE         = 1000359,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef ALEXANDRE_ANIM_H__
