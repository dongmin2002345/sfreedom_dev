// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WORM_CHILLED_ANIM_H__
#define WORM_CHILLED_ANIM_H__

namespace Worm_Chilled_Anim
{
    enum
    {
        ATTK_01                 = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_03_03              = 1000210,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef WORM_CHILLED_ANIM_H__
