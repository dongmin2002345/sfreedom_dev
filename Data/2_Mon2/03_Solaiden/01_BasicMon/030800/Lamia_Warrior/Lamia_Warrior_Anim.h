// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef LAMIA_WARRIOR_ANIM_H__
#define LAMIA_WARRIOR_ANIM_H__

namespace Lamia_Warrior_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_UP        = 1000361,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALLK_01                = 1000050,
        WHIRLWIND_01            = 1000384,
        WHIRLWIND_01_FINISH     = 1000385,
        WHIRLWIND_BTLIDLE_01    = 1000383,
        WHIRLWIND_BTLIDLE_01_START = 1000382
    };
}

#endif  // #ifndef LAMIA_WARRIOR_ANIM_H__
