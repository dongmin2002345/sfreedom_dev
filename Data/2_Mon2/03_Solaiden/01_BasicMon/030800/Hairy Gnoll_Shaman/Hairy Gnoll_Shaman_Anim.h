// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HAIRY_GNOLL_SHAMAN_ANIM_H__
#define HAIRY_GNOLL_SHAMAN_ANIM_H__

namespace Hairy_Gnoll_Shaman_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        RUN_01                  = 1000055,
        SHOT_AROUND_02          = 1000422,
        SHOT_AROUND_02_FIRE     = 1000423,
        SHOT_AROUND_02_START    = 1000421,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_SHOT_01           = 1000312,
        STAND_UP                = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef HAIRY_GNOLL_SHAMAN_ANIM_H__
