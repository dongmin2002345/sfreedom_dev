// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef POLAR_BEAR_ANIM_H__
#define POLAR_BEAR_ANIM_H__

namespace Polar_Bear_Anim
{
    enum
    {
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK   = 1000205,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_ATTK_START = 1000204,
        BTLIDLE_02_SKILL_ATTK   = 1000208,
        BTLIDLE_02_SKILL_ATTK_START = 1000207,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000354,
        EARTHQUAKE_BATTLEIDLE_START = 1000353,
        EARTHQUAKE_FIRE         = 1000355,
        IDLE_01                 = 1000001,
        MASSIVE_PROJECTILE_BTLIDLE = 1000387,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000386,
        MASSIVE_PROJECTILE_FIRE = 1000388,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000206,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000309,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef POLAR_BEAR_ANIM_H__
