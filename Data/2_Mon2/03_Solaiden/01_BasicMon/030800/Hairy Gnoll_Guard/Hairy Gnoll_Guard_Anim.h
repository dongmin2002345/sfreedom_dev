// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HAIRY_GNOLL_GUARD_ANIM_H__
#define HAIRY_GNOLL_GUARD_ANIM_H__

namespace Hairy_Gnoll_Guard_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_SHOT_01           = 1000312,
        STAND_UP                = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef HAIRY_GNOLL_GUARD_ANIM_H__
