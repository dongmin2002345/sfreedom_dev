// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TIGER_FROST_ANIM_H__
#define TIGER_FROST_ANIM_H__

namespace Tiger_Frost_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000206,
        ATTK_01_BTLIDLE         = 1000205,
        ATTK_02_ATTACK          = 1000209,
        ATTK_02_BTLIDLE         = 1000208,
        ATTK_03_BTLIDLE         = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BREATH_01               = 1000347,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DIE_02                  = 1000020,
        DMG_02                  = 1000011,
        DMG_03                  = 1000013,
        IDLE_01                 = 1000012,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_02_ATTACK         = 1000303,
        SKILL_02_BTLIDLE        = 1000302,
        SKILL_ATTK_02           = 1000306,
        SKILL_SHOT_01           = 1000312,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef TIGER_FROST_ANIM_H__
