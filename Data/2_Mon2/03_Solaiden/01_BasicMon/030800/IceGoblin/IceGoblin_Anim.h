// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ICEGOBLIN_ANIM_H__
#define ICEGOBLIN_ANIM_H__

namespace IceGoblin_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000301,
        BTLIDLE_01_SKILL_ATTK_START = 1000302,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000330,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000329,
        DASH_PIERCING_ATTK_FINISH = 1000332,
        DASH_PIERCING_ATTK_RUSH = 1000331,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef ICEGOBLIN_ANIM_H__
