// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DONGDONG_COOL_ANIM_H__
#define DONGDONG_COOL_ANIM_H__

namespace DongDong_Cool_Anim
{
    enum
    {
        _DOWN                   = 1000412,
        _GETUP                  = 1000413,
        _UP                     = 1000411,
        ATTK_01                 = 1000206,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_SHOT   = 1000316,
        BTLIDLE_02_SKILL_SHOT_MIDDLE = 1000318,
        BTLIDLE_02_SKILL_SHOT_START = 1000315,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SHOT_RETURN_02          = 1000319,
        SKILL_ATTK_01           = 1000303,
        SKILL_SHOT_01           = 1000312,
        SKILL_SHOT_02           = 1000317,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef DONGDONG_COOL_ANIM_H__
