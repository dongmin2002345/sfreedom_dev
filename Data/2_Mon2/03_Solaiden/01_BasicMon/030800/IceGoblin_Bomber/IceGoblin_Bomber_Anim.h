// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ICEGOBLIN_BOMBER_ANIM_H__
#define ICEGOBLIN_BOMBER_ANIM_H__

namespace IceGoblin_Bomber_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        POINT_BURST_FINISH      = 1000447,
        POINT_BURST_FIRE        = 1000445,
        POINT_BURST_IDLE        = 1000444,
        POINT_BURST_MIDDLE      = 1000446,
        POINT_BURST_START       = 1000443,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000350,
        SUMMON_THORN_BTLIDLE_START = 1000349,
        SUMMON_THORN_FIRE       = 1000351,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef ICEGOBLIN_BOMBER_ANIM_H__
