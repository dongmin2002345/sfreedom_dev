// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ICEGOBLIN_SHAMAN_ANIM_H__
#define ICEGOBLIN_SHAMAN_ANIM_H__

namespace IceGoblin_Shaman_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_SHOT   = 1000316,
        BTLIDLE_02_SKILL_SHOT_START = 1000315,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_SHOT_01           = 1000312,
        SKILL_SHOT_02           = 1000317,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef ICEGOBLIN_SHAMAN_ANIM_H__
