// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GOLEM_FROST_ANIM_H__
#define GOLEM_FROST_ANIM_H__

namespace Golem_Frost_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_01_START        = 1000201,
        ATTK_01_02              = 1000203,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        DROP_PROJECTILE_BTLIDLE = 1000371,
        DROP_PROJECTILE_BTLIDLE_START = 1000370,
        DROP_PROJECTILE_FIRE    = 1000372,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_SHOT_01           = 1000312,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef GOLEM_FROST_ANIM_H__
