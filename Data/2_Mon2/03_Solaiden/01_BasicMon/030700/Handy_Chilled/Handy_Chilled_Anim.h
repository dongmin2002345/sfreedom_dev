// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HANDY_CHILLED_ANIM_H__
#define HANDY_CHILLED_ANIM_H__

namespace Handy_Chilled_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_02_SHOT         = 1000218,
        BTLIDLE_02_SHOT_START   = 1000217,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_02                 = 1000219,
        STANDUP                 = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000350,
        SUMMON_THORN_BTLIDLE_START = 1000349,
        SUMMON_THORN_FIRE       = 1000351,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef HANDY_CHILLED_ANIM_H__
