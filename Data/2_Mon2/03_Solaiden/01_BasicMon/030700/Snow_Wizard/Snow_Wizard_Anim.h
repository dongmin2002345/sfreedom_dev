// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SNOW_WIZARD_ANIM_H__
#define SNOW_WIZARD_ANIM_H__

namespace Snow_Wizard_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        MASSIVE_PROJECTILE_BTLIDLE = 1000334,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000333,
        MASSIVE_PROJECTILE_FINISH = 1000336,
        MASSIVE_PROJECTILE_FIRE = 1000335,
        OPENING_01              = 1000081,
        POINTBURST_FIRE         = 1000445,
        POINTBURST_IDLE         = 1000444,
        POINTBURST_START        = 1000443,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_AROUND_01          = 1000419,
        SHOT_AROUND_01_FIRE     = 1000420,
        SHOT_AROUND_01_START    = 1000418,
        SHOT_AROUND_02          = 1000422,
        SHOT_AROUND_02_FIRE     = 1000423,
        SHOT_AROUND_02_START    = 1000421,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_FIRE             = 1000440,
        SUMMON_IDLE             = 1000439,
        SUMMON_START            = 1000438,
        SUMMON_THORN_BTLIDLE    = 1000350,
        SUMMON_THORN_BTLIDLE_START = 1000349,
        SUMMON_THORN_FIRE       = 1000351
    };
}

#endif  // #ifndef SNOW_WIZARD_ANIM_H__
