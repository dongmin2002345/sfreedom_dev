// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SELIC_FREEZING_ANIM_H__
#define SELIC_FREEZING_ANIM_H__

namespace Selic_Freezing_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        MASSIVE_PROJECTILE_BTLIDLE = 1000334,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000333,
        MASSIVE_PROJECTILE_FINISH = 1000336,
        MASSIVE_PROJECTILE_FIRE = 1000335,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SELF-DESTRUCTION_01     = 1000406,
        SHOT_RETURN_01          = 1000314,
        SKILL_SHOT_01           = 1000312,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef SELIC_FREEZING_ANIM_H__
