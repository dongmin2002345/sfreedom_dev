// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WORM_POIKY_ANIM_H__
#define WORM_POIKY_ANIM_H__

namespace Worm_Poiky_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        ATTK_FIN_01             = 1000434,
        ATTK_FIN_01_FIN_01      = 1000436,
        ATTK_FIN_01_FIRE        = 1000435,
        ATTK_FIN_01_START       = 1000433,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DIE_01                  = 1000020,
        DIESKILL_ATTK_01        = 1000407,
        DIESKILL_BTLIDLE_01     = 1000406,
        DIESKILL_BTLIDLE_01_START = 1000405,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef WORM_POIKY_ANIM_H__
