// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SELIC_ANIM_H__
#define SELIC_ANIM_H__

namespace Selic_Anim
{
    enum
    {
        ATTK                    = 1000410,
        ATTK_01_02              = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_ATTK            = 1000409,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SELF-DESTRUCTION_01     = 1000406,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        START                   = 1000408,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef SELIC_ANIM_H__
