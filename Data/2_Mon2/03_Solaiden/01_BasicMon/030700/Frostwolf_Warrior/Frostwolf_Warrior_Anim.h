// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTWOLF_WARRIOR_ANIM_H__
#define FROSTWOLF_WARRIOR_ANIM_H__

namespace Frostwolf_Warrior_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        ATTK_02                 = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_02_ATTACK         = 1000347,
        SKILL_02_BTLIDLE        = 1000346,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef FROSTWOLF_WARRIOR_ANIM_H__
