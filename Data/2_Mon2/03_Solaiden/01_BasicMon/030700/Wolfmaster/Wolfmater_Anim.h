// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WOLFMATER_ANIM_H__
#define WOLFMATER_ANIM_H__

namespace Wolfmater_Anim
{
    enum
    {
        ATTK_01_01              = 0,
        ATTK_01_02              = 1,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 2,
        BTLIDLE_01_START        = 3,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef WOLFMATER_ANIM_H__
