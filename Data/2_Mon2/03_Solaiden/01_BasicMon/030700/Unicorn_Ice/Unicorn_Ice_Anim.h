// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef UNICORN_ICE_ANIM_H__
#define UNICORN_ICE_ANIM_H__

namespace Unicorn_Ice_Anim
{
    enum
    {
        _FINISH                 = 1000437,
        _FIRE                   = 1000435,
        _IDLE                   = 1000434,
        _MIDDLE                 = 1000436,
        _START                  = 1000433,
        ATTK_01_01              = 1000202,
        ATTK_01_02              = 1000203,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_START        = 1000201,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef UNICORN_ICE_ANIM_H__
