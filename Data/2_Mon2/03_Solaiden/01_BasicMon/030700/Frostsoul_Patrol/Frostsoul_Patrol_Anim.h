// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTSOUL_PATROL_ANIM_H__
#define FROSTSOUL_PATROL_ANIM_H__

namespace Frostsoul_Patrol_Anim
{
    enum
    {
        ATTK_01_02              = 1000203,
        ATTK_01_BTLIDLE         = 1000005,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000202,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_START        = 1000201,
        DMG_02                  = 1000011,
        DMG_03                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_RETURN_01          = 1000216,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000350,
        SUMMON_THORN_BTLIDLE_START = 1000349,
        SUMMON_THORN_FIRE       = 1000351,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef FROSTSOUL_PATROL_ANIM_H__
