// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTSOUL_PATROL_DIE_ANIM_H__
#define FROSTSOUL_PATROL_DIE_ANIM_H__

namespace Frostsoul_Patrol_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef FROSTSOUL_PATROL_DIE_ANIM_H__
