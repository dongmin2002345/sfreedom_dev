// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CETVROS_OPENING_ANIM_H__
#define CETVROS_OPENING_ANIM_H__

namespace Cetvros_Opening_Anim
{
    enum
    {
        SEQUENCE_01             = 1000001
    };
}

#endif  // #ifndef CETVROS_OPENING_ANIM_H__
