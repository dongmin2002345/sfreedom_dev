// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CETVROS_BOMB_ANIM_H__
#define CETVROS_BOMB_ANIM_H__

namespace Cetvros_Bomb_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        IDLE_01                 = 1000001,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef CETVROS_BOMB_ANIM_H__
