// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CETVROS_ENDING_ANIM_H__
#define CETVROS_ENDING_ANIM_H__

namespace Cetvros_ending_Anim
{
    enum
    {
        ENDING_01               = 1000001
    };
}

#endif  // #ifndef CETVROS_ENDING_ANIM_H__
