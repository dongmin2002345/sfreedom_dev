// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ERDAR_CRYSTAL_ANIM_H__
#define ERDAR_CRYSTAL_ANIM_H__

namespace Erdar_crystal_Anim
{
    enum
    {
        ATTK_01_02              = 1000203,
        BTLIDLE_01              = 1000202,
        BTLIDLE_01_START        = 1000201,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef ERDAR_CRYSTAL_ANIM_H__
