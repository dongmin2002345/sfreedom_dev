// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SOUL_ACHONAI_ANIM_H__
#define SOUL_ACHONAI_ANIM_H__

namespace Soul_Achonai_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        FURY                    = 1000900,
        MASSIVE_PROJECTILE_BTLIDLE = 1000334,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000333,
        MASSIVE_PROJECTILE_FINISH = 1000336,
        MASSIVE_PROJECTILE_FIRE = 1000335,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WHIRLWIND_01            = 1000380,
        WHIRLWIND_01_FINISH     = 1000381,
        WHIRLWIND_BTLIDLE_01    = 1000379,
        WHIRLWIND_BTLIDLE_01_START = 1000378
    };
}

#endif  // #ifndef SOUL_ACHONAI_ANIM_H__
