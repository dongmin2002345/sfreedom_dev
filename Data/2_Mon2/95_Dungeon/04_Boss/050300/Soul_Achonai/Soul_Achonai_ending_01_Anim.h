// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SOUL_ACHONAI_ENDING_01_ANIM_H__
#define SOUL_ACHONAI_ENDING_01_ANIM_H__

namespace Soul_Achonai_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000001
    };
}

#endif  // #ifndef SOUL_ACHONAI_ENDING_01_ANIM_H__
