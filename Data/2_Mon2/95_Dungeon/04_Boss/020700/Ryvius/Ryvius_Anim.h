// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RYVIUS_ANIM_H__
#define RYVIUS_ANIM_H__

namespace Ryvius_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DASH_AFTER_ATTK_01_02   = 1000417,
        DASH_AFTER_ATTK_BTLIDLE_01 = 1000415,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_RUSH    = 1000416,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000001,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        SKILL_SHOT_01           = 1000312,
        STUN                    = 1000025
    };
}

#endif  // #ifndef RYVIUS_ANIM_H__
