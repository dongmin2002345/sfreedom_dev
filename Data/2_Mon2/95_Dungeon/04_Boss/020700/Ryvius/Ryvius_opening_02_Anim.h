// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RYVIUS_OPENING_02_ANIM_H__
#define RYVIUS_OPENING_02_ANIM_H__

namespace Ryvius_opening_02_Anim
{
    enum
    {
        OPENING_02              = 1000000
    };
}

#endif  // #ifndef RYVIUS_OPENING_02_ANIM_H__
