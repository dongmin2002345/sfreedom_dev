// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TYPHOON_ANIM_H__
#define TYPHOON_ANIM_H__

namespace Typhoon_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_02_SHOT         = 1000254,
        BTLIDLE_02_SHOT_START   = 1000253,
        BTLIDLE_03_SHOT         = 1000602,
        BTLIDLE_03_SHOT_START   = 1000601,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        MASSIVE_PROJECTILE_BTLIDLE = 1000622,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000621,
        MASSIVE_PROJECTILE_FIRE = 1000623,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_02                 = 1000255,
        SHOT_03                 = 1000603,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef TYPHOON_ANIM_H__
