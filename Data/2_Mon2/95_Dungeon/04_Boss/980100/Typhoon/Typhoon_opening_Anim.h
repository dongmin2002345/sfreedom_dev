// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TYPHOON_OPENING_ANIM_H__
#define TYPHOON_OPENING_ANIM_H__

namespace Typhoon_opening_Anim
{
    enum
    {
        OPENING_01              = 1000001
    };
}

#endif  // #ifndef TYPHOON_OPENING_ANIM_H__
