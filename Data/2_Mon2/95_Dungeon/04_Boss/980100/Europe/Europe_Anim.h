// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EUROPE_ANIM_H__
#define EUROPE_ANIM_H__

namespace Europe_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        SKILL_01_BTLIDLE        = 1000251,
        SKILL_01_FINISH         = 1000252,
        SKILL_01_START          = 1000250,
        SKILL_02_BTLIDLE        = 1000254,
        SKILL_02_FINISH         = 1000255,
        SKILL_02_START          = 1000253,
        SKILL_03_BTLIDLE        = 1000257,
        SKILL_03_FINISH         = 1000258,
        SKILL_03_START          = 1000256
    };
}

#endif  // #ifndef EUROPE_ANIM_H__
