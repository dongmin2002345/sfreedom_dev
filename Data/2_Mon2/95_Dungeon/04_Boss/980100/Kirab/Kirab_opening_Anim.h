// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KIRAB_OPENING_ANIM_H__
#define KIRAB_OPENING_ANIM_H__

namespace Kirab_opening_Anim
{
    enum
    {
        SCENE_01                = 1000001
    };
}

#endif  // #ifndef KIRAB_OPENING_ANIM_H__
