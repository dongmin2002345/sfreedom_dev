// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KIRAB_ANIM_H__
#define KIRAB_ANIM_H__

namespace Kirab_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        MASSIVE_PROJECTILE01_BTLIDLE = 1000602,
        MASSIVE_PROJECTILE01_BTLIDLE_START = 1000601,
        MASSIVE_PROJECTILE01_FINISH = 1000604,
        MASSIVE_PROJECTILE01_FIRE = 1000603,
        MASSIVE_PROJECTILE02_BTLIDLE = 1000338,
        MASSIVE_PROJECTILE02_BTLIDLE_START = 1000337,
        MASSIVE_PROJECTILE02_FINISH = 1000340,
        MASSIVE_PROJECTILE02_FIRE = 1000339,
        SHOT_01                 = 1000252,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001
    };
}

#endif  // #ifndef KIRAB_ANIM_H__
