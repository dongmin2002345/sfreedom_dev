// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KINGSPION_ANIM_H__
#define KINGSPION_ANIM_H__

namespace Kingspion_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000352,
        ATTK_04_01_START        = 1000351,
        ATTK_04_02              = 1000353,
        ATTK_04_02_FINISH       = 1000354,
        ATTK_05_01              = 1000257,
        ATTK_05_01_START        = 1000256,
        ATTK_05_02              = 1000258,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_01_SKILLL_ATTK_START = 1000301,
        BTLIDLE_01_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_02_SKILL_ATTK   = 1000402,
        BTLIDLE_02_SKILL_ATTK_START = 1000401,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        RUN_01                  = 1000055,
        SKILL_ATTK_01_01        = 1000303,
        SKILL_ATTK_01_02        = 1000305,
        SKILL_ATTK_01_02_FINISH = 1000306,
        SKILL_ATTK_02           = 1000403,
        SKILL_ATTK_02_FINISH    = 1000404,
        STANDUP                 = 1000024,
        STUN                    = 100025
    };
}

#endif  // #ifndef KINGSPION_ANIM_H__
