// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HADES_ANIM_H__
#define HADES_ANIM_H__

namespace Hades_Anim
{
    enum
    {
        ATTK_04_01              = 1000260,
        ATTK_04_01_START        = 1000259,
        ATTK_04_02              = 1000261,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_02_SHOT         = 1000302,
        BTLIDLE_02_SHOT_START   = 1000301,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1001122,
        ENERGY_EXPLOSION_BTLIDLE_START = 1001121,
        ENERGY_EXPLOSION_FIRE   = 1001123,
        FURY_01                 = 1000044,
        MASSIVE_PROJECTILE_BTLIDLE = 1000612,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000611,
        MASSIVE_PROJECTILE_FINISH = 1000614,
        MASSIVE_PROJECTILE_FIRE = 1000613,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_02                 = 1000303,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef HADES_ANIM_H__
