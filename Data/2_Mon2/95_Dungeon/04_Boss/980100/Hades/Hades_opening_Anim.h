// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HADES_OPENING_ANIM_H__
#define HADES_OPENING_ANIM_H__

namespace Hades_opening_Anim
{
    enum
    {
        SCENE_01                = 1000001
    };
}

#endif  // #ifndef HADES_OPENING_ANIM_H__
