// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef POLLOCKS_ANIM_H__
#define POLLOCKS_ANIM_H__

namespace Pollocks_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_FUSION       = 1000302,
        BTLIDLE_01_FUSION_FINISH = 1000303,
        BTLIDLE_01_FUSION_START = 1000301,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        BTLIDLE_01_SKILL_START  = 1001151,
        BTLIDLE_02_SHOT         = 1000254,
        BTLIDLE_02_SHOT_START   = 1000253,
        DIE_01                  = 1000020,
        DIE_02                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        MASSIVE_PROJECTILE_JUMP = 1000614,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE = 1000612,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE_START = 1000611,
        MASSIVE_PROJECTILE_JUMP_FINISH = 1000615,
        MASSIVE_PROJECTILE_JUMP_FIRE = 1000613,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_02                 = 1000255,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001,
        SHOT_RETURN_01          = 1001154,
        SKILL_SHOT_01           = 1001153,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_CAST  = 1001144,
        TELEPORT_SKILL_01_START = 1001141
    };
}

#endif  // #ifndef POLLOCKS_ANIM_H__
