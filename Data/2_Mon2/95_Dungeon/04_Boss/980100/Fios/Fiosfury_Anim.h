// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FIOSFURY_ANIM_H__
#define FIOSFURY_ANIM_H__

namespace Fiosfury_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_HIT          = 1000314,
        BTLIDLE_01_HIT_START    = 1000313,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1000402,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000401,
        ENERGY_EXPLOSION_FIRE   = 1000403,
        HIT_01                  = 1000315,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_START = 1001141
    };
}

#endif  // #ifndef FIOSFURY_ANIM_H__
