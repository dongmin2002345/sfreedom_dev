// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FIOS_FINAL_ANIM_H__
#define FIOS_FINAL_ANIM_H__

namespace Fios_final_Anim
{
    enum
    {
        FINAL_01                = 1110001,
        FINAL_02                = 1110002,
        FINAL_03                = 1110003
    };
}

#endif  // #ifndef FIOS_FINAL_ANIM_H__
