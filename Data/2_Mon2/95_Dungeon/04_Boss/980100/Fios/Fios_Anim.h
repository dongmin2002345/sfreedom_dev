// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FIOS_ANIM_H__
#define FIOS_ANIM_H__

namespace Fios_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        BTLIDLE_01_SKILL_START  = 1001151,
        DIE_02                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1000402,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000401,
        ENERGY_EXPLOSION_FIRE   = 1000403,
        FURY_01                 = 1000044,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001,
        SKILL_01_SHOT_MIDDLE    = 1001154,
        SKILL_ATTK_01           = 1000303,
        SKILL_SHOT_01           = 1001153,
        SKILL_SHOT_01_FINISH    = 1001155,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORNS_BTLIDLE   = 1000642,
        SUMMON_THORNS_BTLIDLE_START = 1000641,
        SUMMON_THORNS_FINISH    = 1000644,
        SUMMON_THORNS_FIRE      = 1000643,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_START = 1001141
    };
}

#endif  // #ifndef FIOS_ANIM_H__
