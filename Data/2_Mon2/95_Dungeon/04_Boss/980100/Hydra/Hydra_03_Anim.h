// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HYDRA_03_ANIM_H__
#define HYDRA_03_ANIM_H__

namespace Hydra_03_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000205,
        BTLIDLE_01_SHOT_START   = 1000204,
        BTLIDLE_01_SKILL_ATTK   = 1000452,
        BTLIDLE_01_SKILL_ATTK_START = 1000451,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        BTLIDLE_01_SKILL_START  = 1001151,
        BTLIDLE_02_SHOT         = 1000208,
        BTLIDLE_02_SHOT_START   = 1000207,
        BTLIDLE_03_SHOT         = 1000612,
        BTLIDLE_03_SHOT_START   = 1000611,
        DIE_01                  = 1000020,
        DIE_IDLE                = 1000021,
        EARTHQUAKE_BATTLEIDLE   = 1001182,
        EARTHQUAKE_BATTLEIDLE_START = 1001181,
        EARTHQUAKE_FIRE         = 1001183,
        FURY_01                 = 1000044,
        SHOT_01                 = 1000206,
        SHOT_02                 = 1000209,
        SHOT_03                 = 1000613,
        SHOT_RETURN_01          = 1001154,
        SKILL_ATTK_01           = 1000453,
        SKILL_SHOT_01           = 1001153,
        STANDUP_01              = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef HYDRA_03_ANIM_H__
