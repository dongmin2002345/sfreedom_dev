// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HYDRA_01_ANIM_H__
#define HYDRA_01_ANIM_H__

namespace Hydra_01_Anim
{
    enum
    {
        ATTK_01_01              = 1000251,
        ATTK_01_01_START        = 1000250,
        ATTK_01_02              = 1000252,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000205,
        BTLIDLE_01_SHOT_START   = 1000204,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        BTLIDLE_01_SKILL_START  = 1001151,
        BTLIDLE_02_SHOT         = 1000208,
        BTLIDLE_02_SHOT_START   = 1000207,
        DASH_AFTER_ATTK_01_02   = 1000417,
        DASH_AFTER_ATTK_BTLIDLE_01_IDLE = 1000415,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_RUSH    = 1000416,
        DIE_01                  = 1000020,
        FURY_01                 = 1000044,
        S_CURVE_FINISH          = 1000434,
        S_CURVE_FIRE            = 1000433,
        S_CURVE_IDLE            = 1000432,
        S_CURVE_START           = 1000431,
        SHOT_01                 = 1000206,
        SHOT_02                 = 1000209,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001,
        SHOT_RETURN_01          = 1001154,
        SKILL_SHOT_01           = 1001153,
        STUN                    = 1000025
    };
}

#endif  // #ifndef HYDRA_01_ANIM_H__
