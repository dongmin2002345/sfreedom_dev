// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HYDRA_OPENING_ANIM_H__
#define HYDRA_OPENING_ANIM_H__

namespace Hydra_opening_Anim
{
    enum
    {
        OPENING_01              = 1000001,
        OPENING_02              = 1000002
    };
}

#endif  // #ifndef HYDRA_OPENING_ANIM_H__
