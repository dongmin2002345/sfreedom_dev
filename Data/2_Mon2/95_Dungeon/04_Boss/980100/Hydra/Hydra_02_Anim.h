// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HYDRA_02_ANIM_H__
#define HYDRA_02_ANIM_H__

namespace Hydra_02_Anim
{
    enum
    {
        BREATH_01               = 1001123,
        BREATH_01_FINISH        = 1001124,
        BREATH_BTLIDLE_01       = 1001122,
        BREATH_BTLIDLE_01_START = 1001121,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000205,
        BTLIDLE_01_SHOT_START   = 1000204,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        BTLIDLE_01_SKILL_START  = 1001151,
        BTLIDLE_02_SHOT         = 1000208,
        BTLIDLE_02_SHOT_START   = 1000207,
        DASHATTK_BTLIDLE_01     = 1000502,
        DASHATTK_BTLIDLE_01_START = 1000501,
        DASHATTK_FINISH         = 1000504,
        DASHATTK_RUSH           = 1000503,
        DIE_01                  = 1000020,
        DIE_IDLE                = 1000021,
        FURY_01                 = 1000044,
        SET_TRAP_ATTK_01        = 1001113,
        SET_TRAP_BTLIDLE_01     = 1001112,
        SET_TRAP_BTLIDLE_01_START = 1001111,
        SHOT_01                 = 1000206,
        SHOT_02                 = 1000209,
        SHOT_RETURN_01          = 1001154,
        SKILL_SHOT_01           = 1001153,
        STANDUP_01              = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef HYDRA_02_ANIM_H__
