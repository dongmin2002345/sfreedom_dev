// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MINOTAUROS_ANIM_H__
#define MINOTAUROS_ANIM_H__

namespace Minotauros_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_05_01              = 1000251,
        ATTK_05_01_START        = 1000250,
        ATTK_05_02              = 1000252,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DASHATTK_BTLIDLE_01     = 1000502,
        DASHATTK_BTLIDLE_01_START = 1000501,
        DASHATTK_FINISH         = 1000504,
        DASHATTK_RUSH           = 1000503,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        JUMPDOWNCRASH_DOWN      = 1000702,
        JUMPDOWNCRASH_GETUP     = 1000703,
        JUMPDOWNCRASH_UP        = 1000701,
        RUN_01                  = 1000055,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WHIRLWIND_01            = 1000803,
        WHIRLWIND_01_FINISH     = 1000804,
        WHIRLWIND_BTLIDLE_01    = 1000802,
        WHIRLWIND_BTLIDLE_01_START = 1000801
    };
}

#endif  // #ifndef MINOTAUROS_ANIM_H__
