// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MINOTAUROS_ENDING_ANIM_H__
#define MINOTAUROS_ENDING_ANIM_H__

namespace Minotauros_ending_Anim
{
    enum
    {
        DIE_02                  = 1000018,
        ENDING_01               = 1000019
    };
}

#endif  // #ifndef MINOTAUROS_ENDING_ANIM_H__
