// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SERPENS_ANIM_H__
#define SERPENS_ANIM_H__

namespace Serpens_Anim
{
    enum
    {
        BREATH_01               = 1001123,
        BREATH_01_FINISH        = 1001124,
        BREATH_BTLIDLE_01       = 1001122,
        BREATH_BTLIDLE_01_START = 1001121,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DIE_01                  = 1000020,
        FURY_01                 = 1000044,
        MASSIVE_PROJECTILE_BTLIDLE = 1000602,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000601,
        MASSIVE_PROJECTILE_FINISH = 1000604,
        MASSIVE_PROJECTILE_FIRE = 1000603,
        SET_TRAP_ATTK_01        = 1001113,
        SET_TRAP_BTLIDLE_01     = 1001112,
        SET_TRAP_BTLIDLE_01_START = 1001111,
        SHOT_AROUND_01          = 1001002,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001001,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        SUMMON_THRONS_BTLIDLE   = 1000642,
        SUMMON_THRONS_BTLIDLE_START = 1000641,
        SUMMON_THRONS_FINISH    = 1000644,
        SUMMON_THRONS_FIRE      = 1000643
    };
}

#endif  // #ifndef SERPENS_ANIM_H__
