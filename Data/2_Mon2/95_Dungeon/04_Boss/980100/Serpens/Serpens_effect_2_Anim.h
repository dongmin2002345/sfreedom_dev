// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SERPENS_EFFECT_2_ANIM_H__
#define SERPENS_EFFECT_2_ANIM_H__

namespace Serpens_effect_2_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef SERPENS_EFFECT_2_ANIM_H__
