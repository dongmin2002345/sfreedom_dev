// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SERPENS_ENDING_ANIM_H__
#define SERPENS_ENDING_ANIM_H__

namespace Serpens_ending_Anim
{
    enum
    {
        ENDING_01               = 1000018
    };
}

#endif  // #ifndef SERPENS_ENDING_ANIM_H__
