// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SERPENS_OPENING_ANIM_H__
#define SERPENS_OPENING_ANIM_H__

namespace Serpens_opening_Anim
{
    enum
    {
        OPENING_01              = 1000001,
        OPENING_02              = 1000002,
        OPENING_03              = 1000003
    };
}

#endif  // #ifndef SERPENS_OPENING_ANIM_H__
