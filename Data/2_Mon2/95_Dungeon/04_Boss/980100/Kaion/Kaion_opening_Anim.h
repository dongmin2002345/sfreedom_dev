// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAION_OPENING_ANIM_H__
#define KAION_OPENING_ANIM_H__

namespace Kaion_opening_Anim
{
    enum
    {
        KAION_OPENING_01        = 1000001,
        KAION_OPENING_02        = 1000002,
        KAION_OPENING_03        = 1000003
    };
}

#endif  // #ifndef KAION_OPENING_ANIM_H__
