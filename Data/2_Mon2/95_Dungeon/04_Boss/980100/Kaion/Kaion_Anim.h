// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAION_ANIM_H__
#define KAION_ANIM_H__

namespace Kaion_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_AFTER_LOOP_01      = 1000334,
        ATTK_LOOP_01            = 1000333,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_LOOP_01         = 1000332,
        BTLIDLE_LOOP_01_START   = 1000331,
        DASHATTK_BTLIDLE_01     = 1000415,
        DASHATTK_BTLIDLE_01_START = 1000414,
        DASHATTK_FINISH         = 1000417,
        DASHATTK_RUSH           = 1000416,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FINISH_LOOP_01          = 1000335,
        FURY_01                 = 1000044,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_UP        = 1000361,
        RUN_01                  = 1000055,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef KAION_ANIM_H__
