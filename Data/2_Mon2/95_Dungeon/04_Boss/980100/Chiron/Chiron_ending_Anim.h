// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHIRON_ENDING_ANIM_H__
#define CHIRON_ENDING_ANIM_H__

namespace Chiron_ending_Anim
{
    enum
    {
        DIE_02                  = 1000018,
        SCENE_01                = 1000001
    };
}

#endif  // #ifndef CHIRON_ENDING_ANIM_H__
