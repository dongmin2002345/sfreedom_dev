// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHIRON_ANIM_H__
#define CHIRON_ANIM_H__

namespace Chiron_Anim
{
    enum
    {
        ATTK_03_01              = 1000260,
        ATTK_03_01_START        = 1000259,
        ATTK_03_02              = 1000261,
        ATTK_04_01              = 1000263,
        ATTK_04_01_START        = 1000262,
        ATTK_04_02              = 1000264,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_HIT          = 1000214,
        BTLIDLE_01_HIT_START    = 1000213,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_03_SHOT         = 1000257,
        BTLIDLE_03_SHOT_START   = 1000256,
        BTLIDLE_04_SHOT_START   = 1001151,
        BTLIDLE_04_SKILL_SHOT   = 1001152,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY                    = 1000044,
        HIT_01                  = 1000215,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_03                 = 1000258,
        SHOT_RETURN_01          = 1001154,
        SKILL_SHOT_01           = 1001153,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef CHIRON_ANIM_H__
