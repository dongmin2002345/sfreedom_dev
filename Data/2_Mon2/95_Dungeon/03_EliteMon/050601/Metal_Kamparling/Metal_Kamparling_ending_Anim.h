// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METAL_KAMPARLING_ENDING_ANIM_H__
#define METAL_KAMPARLING_ENDING_ANIM_H__

namespace Metal_Kamparling_ending_Anim
{
    enum
    {
        ENDING_01               = 1000000
    };
}

#endif  // #ifndef METAL_KAMPARLING_ENDING_ANIM_H__
