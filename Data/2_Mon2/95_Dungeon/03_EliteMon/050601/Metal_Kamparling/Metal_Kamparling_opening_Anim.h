// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METAL_KAMPARLING_OPENING_ANIM_H__
#define METAL_KAMPARLING_OPENING_ANIM_H__

namespace Metal_Kamparling_opening_Anim
{
    enum
    {
        OPENING_01              = 1000082
    };
}

#endif  // #ifndef METAL_KAMPARLING_OPENING_ANIM_H__
