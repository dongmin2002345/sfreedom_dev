// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METAL_KAMPARLING_ANIM_H__
#define METAL_KAMPARLING_ANIM_H__

namespace Metal_Kamparling_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000022,
        BLOWUP_LOOP             = 1000023,
        BTLIDLE_01              = 1000005,
        BTLIDLE_02              = 1000006,
        CROSSATTACK_01_FINISH   = 1000904,
        CROSSATTACK_01_FIRE     = 1000903,
        CROSSATTACK_01_IDLE     = 1000902,
        CROSSATTACK_01_START    = 1000901,
        DASH_AFTER_ATTK_01_02   = 1000744,
        DASH_AFTER_ATTK_BTLIDLE_01_IDLE = 1000742,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000741,
        DASH_AFTER_ATTK_RUSH    = 1000743,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000512,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000511,
        DASH_PIERCING_ATTK_FINISH = 1000514,
        DASH_PIERCING_ATTK_RUSH = 1000513,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE = 1000612,
        MASSIVE_PROJECTILE_JUMP_BTLIDLE_START = 1000611,
        MASSIVE_PROJECTILE_JUMP_FINISH = 1000614,
        MASSIVE_PROJECTILE_JUMP_FIRE = 1000613,
        RUN_01                  = 1000055,
        S_CURVE_FINISH          = 1000434,
        S_CURVE_FIRE            = 1000433,
        S_CURVE_IDLE            = 1000432,
        S_CURVE_START           = 1000431,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef METAL_KAMPARLING_ANIM_H__
