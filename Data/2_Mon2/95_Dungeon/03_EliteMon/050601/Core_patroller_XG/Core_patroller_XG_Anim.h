// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CORE_PATROLLER_XG_ANIM_H__
#define CORE_PATROLLER_XG_ANIM_H__

namespace Core_patroller_XG_Anim
{
    enum
    {
        BTLIDLE_01_SHOT_START   = 1001151,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        SHOT_RETURN_01          = 1001154,
        SHOT_RETURN_02          = 1001156,
        SKILL_SHOT_01           = 1001153,
        SKILL_SHOT_02           = 1001155
    };
}

#endif  // #ifndef CORE_PATROLLER_XG_ANIM_H__
