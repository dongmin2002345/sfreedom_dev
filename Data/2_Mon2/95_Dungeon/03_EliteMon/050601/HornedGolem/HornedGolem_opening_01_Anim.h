// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HORNEDGOLEM_OPENING_01_ANIM_H__
#define HORNEDGOLEM_OPENING_01_ANIM_H__

namespace HornedGolem_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000082
    };
}

#endif  // #ifndef HORNEDGOLEM_OPENING_01_ANIM_H__
