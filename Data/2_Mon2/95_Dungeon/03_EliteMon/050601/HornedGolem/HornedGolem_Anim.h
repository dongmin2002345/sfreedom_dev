// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HORNEDGOLEM_ANIM_H__
#define HORNEDGOLEM_ANIM_H__

namespace HornedGolem_Anim
{
    enum
    {
        ATTK_01_02              = 1000203,
        ATTK_01_BTLIDLE         = 1000005,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000202,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_START        = 1000201,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DMG_02                  = 1000011,
        DMG_03                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000001,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_UP        = 1000361,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef HORNEDGOLEM_ANIM_H__
