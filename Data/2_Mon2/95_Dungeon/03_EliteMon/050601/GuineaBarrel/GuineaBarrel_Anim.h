// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GUINEABARREL_ANIM_H__
#define GUINEABARREL_ANIM_H__

namespace GuineaBarrel_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        ATTK_05_01              = 1000214,
        ATTK_05_01_START        = 1000213,
        ATTK_05_02              = 1000215,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTIDLE_01               = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000402,
        BTLIDLE_01_SKILL_SHOT_START = 1000401,
        BTLIDLE_02_SHOT_START   = 1001151,
        BTLIDLE_02_SKILL_SHOT   = 1001152,
        BTLIDLE_03_SKILL_SHOT   = 1000412,
        BTLIDLE_03_SKILL_SHOT_START = 1000411,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000512,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000511,
        DASH_PIERCING_ATTK_FINISH = 1000514,
        DASH_PIERCING_ATTK_RUSH = 1000513,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        MASSIVE_PROJECTILE_BTLIDLE = 1000622,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000621,
        MASSIVE_PROJECTILE_FINISH = 1000624,
        MASSIVE_PROJECTILE_FIRE = 1000623,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000404,
        SHOT_RETURN_02          = 1001154,
        SKILL_SHOT_01           = 1000403,
        SKILL_SHOT_02           = 1001153,
        SKILL_SHOT_03           = 1000413,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef GUINEABARREL_ANIM_H__
