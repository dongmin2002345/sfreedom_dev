// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAMPA_MUCHINE_DIE_ANIM_H__
#define KAMPA_MUCHINE_DIE_ANIM_H__

namespace Kampa_muchine_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef KAMPA_MUCHINE_DIE_ANIM_H__
