// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAMPA_MUCHINE_ANIM_H__
#define KAMPA_MUCHINE_ANIM_H__

namespace Kampa_muchine_Anim
{
    enum
    {
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        SUMMON_THORN_BTLIDLE_START = 1000631,
        SUMMON_THORN_FINISH     = 1000635,
        SUMMON_THORN_FIRE       = 1000633,
        SUMMON_THORN_MIDDLE     = 1000634,
        SUMMON_THRON_BTLDIE     = 1000632
    };
}

#endif  // #ifndef KAMPA_MUCHINE_ANIM_H__
