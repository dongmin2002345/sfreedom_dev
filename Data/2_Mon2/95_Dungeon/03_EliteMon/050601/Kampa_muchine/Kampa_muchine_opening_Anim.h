// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAMPA_MUCHINE_OPENING_ANIM_H__
#define KAMPA_MUCHINE_OPENING_ANIM_H__

namespace Kampa_muchine_opening_Anim
{
    enum
    {
        OPENING_01              = 1000082,
        OPENING_02              = 1000083
    };
}

#endif  // #ifndef KAMPA_MUCHINE_OPENING_ANIM_H__
