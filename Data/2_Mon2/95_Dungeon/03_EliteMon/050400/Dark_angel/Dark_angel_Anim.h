// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DARK_ANGEL_ANIM_H__
#define DARK_ANGEL_ANIM_H__

namespace Dark_angel_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        ATTK_05_01              = 1000214,
        ATTK_05_01_START        = 1000213,
        ATTK_05_02              = 1000215,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FURY                    = 1000900,
        LIGHTNING_STROKE_01_FINISH = 1000436,
        LIGHTNING_STROKE_01_FIRE = 1000435,
        LIGHTNING_STROKE_01_IDLE = 1000434,
        LIGHTNING_STROKE_01_START = 1000433,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        SUMMON_THORNS_BTLIDLE   = 1000354,
        SUMMON_THORNS_BTLIDLE_START = 1000353,
        SUMMON_THORNS_FINISH    = 1000356,
        SUMMON_THORNS_FIRE      = 1000355,
        TELEPORT_SKILL_01       = 1000450,
        TELEPORT_SKILL_01_BTLIDLE = 1000449,
        TELEPORT_SKILL_01_START = 1000448
    };
}

#endif  // #ifndef DARK_ANGEL_ANIM_H__
