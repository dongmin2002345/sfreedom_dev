// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TREE_POISON_ANIM_H__
#define TREE_POISON_ANIM_H__

namespace Tree_Poison_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BLTIDLE_01              = 1000005,
        BLTIDLE_01_SKILL_ATTK   = 1000302,
        BLTIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        CROSSATTACK_01_BTLIDLE  = 1000396,
        CROSSATTACK_01_FIRE     = 1000397,
        CROSSATTACK_01_START    = 1000395,
        DIE_01                  = 1000020,
        DIESKILL_ATTK_01        = 1000407,
        DIESKILL_BTLIDLE_01     = 1000406,
        DIESKILL_BTLIDLE_01_START = 1000405,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DROP_PROJECTILE_BTLIDLE = 1000371,
        DROP_PROJECTILE_BTLIDLE_START = 1000370,
        DROP_PROJECTILE_FIRE    = 1000372,
        FURY_01                 = 1000900,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        STANDUP_01              = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef TREE_POISON_ANIM_H__
