// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef AGNIS_EM_ANIM_H__
#define AGNIS_EM_ANIM_H__

namespace Agnis_Em_Anim
{
    enum
    {
        ATTK_01_02              = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_START        = 1000201,
        BTLIDLE_02              = 1000202,
        DASH_AFTER_ATTK_01_02   = 1000417,
        DASH_AFTER_ATTK_BTLIDLE_01 = 1000415,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_RUSH    = 1000416,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_DOWN_02   = 1000368,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_GETUP_02  = 1000369,
        JUMPDOWNCRASH_UP        = 1000361,
        JUMPDOWNCRASH_UP_02     = 1000367,
        OPENING                 = 1000082,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_01_FINISH    = 1000304,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WHIRLWIND_01            = 1000380,
        WHIRLWIND_BTLIDLE_01    = 1000379,
        WHIRLWIND_BTLIDLE_01_START = 1000378
    };
}

#endif  // #ifndef AGNIS_EM_ANIM_H__
