// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTSOUL_POLLUTION_DIE_ANIM_H__
#define FROSTSOUL_POLLUTION_DIE_ANIM_H__

namespace Frostsoul_Pollution_die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef FROSTSOUL_POLLUTION_DIE_ANIM_H__
