// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROSTSOUL_POLLUTION_ANIM_H__
#define FROSTSOUL_POLLUTION_ANIM_H__

namespace Frostsoul_Pollution_Anim
{
    enum
    {
        ATTK_01_02              = 1000203,
        ATTK_01_BTLIDLE         = 1000005,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BREATH_01               = 1000347,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000202,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_START        = 1000201,
        DMG_02                  = 1000011,
        DMG_03                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        RUN_01                  = 1000055,
        SKILL_02_ATTACK         = 1000210,
        SKILL_02_BTLIDLE        = 1000209,
        SKILL_02_START          = 1000208,
        SKILL_ATTK_01           = 1000303,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000350,
        SUMMON_THORN_BTLIDLE_START = 1000349,
        SUMMON_THORN_FIRE       = 1000351,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef FROSTSOUL_POLLUTION_ANIM_H__
