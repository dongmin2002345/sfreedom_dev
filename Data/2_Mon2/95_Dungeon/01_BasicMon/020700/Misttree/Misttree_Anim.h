// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISTTREE_ANIM_H__
#define MISTTREE_ANIM_H__

namespace Misttree_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        SKILL_01                = 1000201
    };
}

#endif  // #ifndef MISTTREE_ANIM_H__
