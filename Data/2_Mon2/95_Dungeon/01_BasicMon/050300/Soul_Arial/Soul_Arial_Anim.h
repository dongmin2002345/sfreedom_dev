// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SOUL_ARIAL_ANIM_H__
#define SOUL_ARIAL_ANIM_H__

namespace Soul_Arial_Anim
{
    enum
    {
        _IDLE                   = 1000415,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_02_SHOT         = 1000255,
        BTLIDLE_02_SHOT_MIDDLE  = 1000257,
        BTLIDLE_02_SHOT_START   = 1000254,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FIRE                    = 1000416,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RETURN                  = 1000417,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SHOT_02                 = 1000256,
        SHOT_RETURN_02          = 1000258,
        STANDUP                 = 1000024,
        START                   = 1000414,
        STUN                    = 1000025,
        WALLK_01                = 1000050
    };
}

#endif  // #ifndef SOUL_ARIAL_ANIM_H__
