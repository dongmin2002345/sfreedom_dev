// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISPENG_WORKER_ANIM_H__
#define MISPENG_WORKER_ANIM_H__

namespace Mispeng_Worker_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 23,
        BLOWUP_LOOP_01          = 24,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        SKILL_SHOT_01           = 1000312,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef MISPENG_WORKER_ANIM_H__
