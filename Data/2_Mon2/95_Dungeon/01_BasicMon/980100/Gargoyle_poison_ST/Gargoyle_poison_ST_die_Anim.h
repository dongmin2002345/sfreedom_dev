// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GARGOYLE_POISON_ST_DIE_ANIM_H__
#define GARGOYLE_POISON_ST_DIE_ANIM_H__

namespace Gargoyle_poison_ST_die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef GARGOYLE_POISON_ST_DIE_ANIM_H__
