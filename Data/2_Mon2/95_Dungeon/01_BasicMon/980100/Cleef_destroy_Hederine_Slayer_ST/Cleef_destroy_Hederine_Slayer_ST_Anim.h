// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CLEEF_DESTROY_HEDERINE_SLAYER_ST_ANIM_H__
#define CLEEF_DESTROY_HEDERINE_SLAYER_ST_ANIM_H__

namespace Cleef_destroy_Hederine_Slayer_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING            = 1000015,
        IDLE_01                 = 1000000,
        OPENING                 = 1000081,
        RUN_01                  = 1000051,
        SKILL_03_ATTCK          = 1000206,
        SKILL_03_BTLIDLE        = 1000205,
        SKILL_06_ATTCK          = 1000212,
        SKILL_06_BTLIDLE        = 1000211,
        STANDUP_01              = 1000026,
        STUN_01                 = 1000025,
        WALLK_01                = 1000055
    };
}

#endif  // #ifndef CLEEF_DESTROY_HEDERINE_SLAYER_ST_ANIM_H__
