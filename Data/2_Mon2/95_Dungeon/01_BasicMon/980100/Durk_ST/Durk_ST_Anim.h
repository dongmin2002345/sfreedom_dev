// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DURK_ST_ANIM_H__
#define DURK_ST_ANIM_H__

namespace Durk_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000071,
        SHOT_02_01              = 1000072,
        SHOT_02_02              = 1000073,
        SHOT_03_01              = 1000074,
        SHOT_03_02              = 1000075,
        SKILL_01_01             = 1000201,
        SKILL_01_02             = 1000202,
        SKILL_02_01             = 1000203,
        SKILL_02_02             = 1000204,
        SKILL_02_03             = 1000205,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef DURK_ST_ANIM_H__
