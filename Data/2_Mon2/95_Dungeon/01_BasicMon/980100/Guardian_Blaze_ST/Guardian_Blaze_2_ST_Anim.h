// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GUARDIAN_BLAZE_2_ST_ANIM_H__
#define GUARDIAN_BLAZE_2_ST_ANIM_H__

namespace Guardian_Blaze_2_ST_Anim
{
    enum
    {
        ATTK_02_02              = 1000206,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef GUARDIAN_BLAZE_2_ST_ANIM_H__
