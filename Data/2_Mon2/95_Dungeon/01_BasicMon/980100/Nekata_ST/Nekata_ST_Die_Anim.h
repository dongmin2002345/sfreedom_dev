// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NEKATA_ST_DIE_ANIM_H__
#define NEKATA_ST_DIE_ANIM_H__

namespace Nekata_ST_Die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef NEKATA_ST_DIE_ANIM_H__
