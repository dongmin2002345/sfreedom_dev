// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HYDRON_FROST_ST_ANIM_H__
#define HYDRON_FROST_ST_ANIM_H__

namespace Hydron_Frost_ST_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_02_SKILL_SHOT   = 1000316,
        BTLIDLE_02_SKILL_SHOT_MIDDLE = 1000318,
        BTLIDLE_02_SKILL_SHOT_START = 1000315,
        BTLIDLE_03_SKILL_SHOT   = 1000321,
        BTLIDLE_03_SKILL_SHOT_MIDDLE = 1000323,
        BTLIDLE_03_SKILL_SHOT_START = 1000320,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FINISH       = 1000389,
        EARTHQUAKE_FIRE         = 1000388,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000314,
        SHOT_RETURN_02          = 1000319,
        SHOT_RETURN_03          = 1000324,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_SHOT_01           = 1000312,
        SKILL_SHOT_02           = 1000317,
        SKILL_SHOT_03           = 1000322,
        STANDUP                 = 1000024,
        STUN_01                 = 1000025,
        WALLK_01                = 1000050
    };
}

#endif  // #ifndef HYDRON_FROST_ST_ANIM_H__
