// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEMON_CREATURE_ST_ANIM_H__
#define DEMON_CREATURE_ST_ANIM_H__

namespace Demon_creature_ST_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000313,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        DASH_AFTER_ATTK_01_02   = 1000417,
        DASH_AFTER_ATTK_BTLIDLE_01 = 1000415,
        DASH_AFTER_ATTK_BTLIDLE_01_START = 1000414,
        DASH_AFTER_ATTK_BTLIDLE_RUSH = 1000416,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_RETURN_01          = 1000314,
        SKILL_SHOT_01           = 1000312,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALLK_01                = 1000050
    };
}

#endif  // #ifndef DEMON_CREATURE_ST_ANIM_H__
