// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TURTLE_SKY_DIE_ANIM_H__
#define TURTLE_SKY_DIE_ANIM_H__

namespace Turtle_Sky_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef TURTLE_SKY_DIE_ANIM_H__
