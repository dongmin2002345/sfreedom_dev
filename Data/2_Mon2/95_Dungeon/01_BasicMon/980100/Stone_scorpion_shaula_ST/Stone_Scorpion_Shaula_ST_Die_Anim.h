// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef STONE_SCORPION_SHAULA_ST_DIE_ANIM_H__
#define STONE_SCORPION_SHAULA_ST_DIE_ANIM_H__

namespace Stone_Scorpion_Shaula_ST_Die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef STONE_SCORPION_SHAULA_ST_DIE_ANIM_H__
