// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEMON_DARKBUST_ST_ANIM_H__
#define DEMON_DARKBUST_ST_ANIM_H__

namespace Demon_Darkbust_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_BLOWUP_DOWN         = 1000044,
        DIE_BLOWUP_LOOP         = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING        = 1000042,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SHOT_02                 = 1000066,
        SKILL_01_01             = 1000201,
        SKILL_01_02             = 1000202,
        SKILL_02_ATTACK         = 1000203,
        SKILL_02_BTLIDLE        = 1000204,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef DEMON_DARKBUST_ST_ANIM_H__
