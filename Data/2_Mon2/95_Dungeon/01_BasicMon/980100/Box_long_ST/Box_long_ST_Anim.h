// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BOX_LONG_ST_ANIM_H__
#define BOX_LONG_ST_ANIM_H__

namespace Box_long_ST_Anim
{
    enum
    {
        ATTK_01_01              = 1000061,
        ATTK_01_02              = 1000062,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000012,
        DMG_02                  = 1000013,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000051,
        SKILL_01_01             = 1000201,
        SKILL_02_01             = 1000202,
        SKILL_02_02             = 1000203,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef BOX_LONG_ST_ANIM_H__
