// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DAIL_SARCOSUCHUS_ST_ANIM_H__
#define DAIL_SARCOSUCHUS_ST_ANIM_H__

namespace Dail_Sarcosuchus_ST_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000071,
        SKILL_01                = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef DAIL_SARCOSUCHUS_ST_ANIM_H__
