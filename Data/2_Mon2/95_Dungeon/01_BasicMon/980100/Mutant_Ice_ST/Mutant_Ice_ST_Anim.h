// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MUTANT_ICE_ST_ANIM_H__
#define MUTANT_ICE_ST_ANIM_H__

namespace Mutant_Ice_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000062,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef MUTANT_ICE_ST_ANIM_H__
