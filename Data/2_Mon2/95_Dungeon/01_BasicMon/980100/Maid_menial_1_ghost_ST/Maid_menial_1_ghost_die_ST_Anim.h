// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAID_MENIAL_1_GHOST_DIE_ST_ANIM_H__
#define MAID_MENIAL_1_GHOST_DIE_ST_ANIM_H__

namespace Maid_menial_1_ghost_die_ST_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef MAID_MENIAL_1_GHOST_DIE_ST_ANIM_H__
