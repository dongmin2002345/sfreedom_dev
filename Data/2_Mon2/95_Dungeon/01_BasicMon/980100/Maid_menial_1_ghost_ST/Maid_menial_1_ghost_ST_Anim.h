// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAID_MENIAL_1_GHOST_ST_ANIM_H__
#define MAID_MENIAL_1_GHOST_ST_ANIM_H__

namespace Maid_menial_1_ghost_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_02_ATTACK         = 1000202,
        SKILL_02_BTLIDLE        = 1000201,
        SKILL_04_ATTACK         = 1000204,
        SKILL_04_BTLIDLE        = 1000203,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef MAID_MENIAL_1_GHOST_ST_ANIM_H__
