// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SWIPER_ANIM_H__
#define SWIPER_ANIM_H__

namespace Swiper_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1001192,
        ENERGY_EXPLOSION_BTLIDLE_START = 1001191,
        ENERGY_EXPLOSION_FIRE   = 1001193,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        LIGHTNING_STROKE_01_FINISH = 1001124,
        LIGHTNING_STROKE_01_FIRE = 1001123,
        LIGHTNING_STROKE_01_IDLE = 1001122,
        LIGHTNING_STROKE_01_MIDDLE = 1001125,
        LIGHTNING_STROKE_01_START = 1001121,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_START = 1001141,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef SWIPER_ANIM_H__
