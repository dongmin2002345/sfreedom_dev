// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SCYTHER_ANIM_H__
#define SCYTHER_ANIM_H__

namespace Scyther_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000402,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000404,
        BTLIDLE_01_SKILL_SHOT_START = 1000401,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000405,
        SKILL_SHOT_01           = 1000403,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_START = 1001141,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000812,
        WHIRLWIND_01_FINISH     = 1000814,
        WHIRLWIND_BTLIDLE_01    = 1000813,
        WHIRLWIND_BTLIDLE_01_START = 1000811
    };
}

#endif  // #ifndef SCYTHER_ANIM_H__
