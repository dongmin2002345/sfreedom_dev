// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SICKLER_ANIM_H__
#define SICKLER_ANIM_H__

namespace Sickler_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000402,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000404,
        BTLIDLE_01_SKILL_SHOT_START = 1000401,
        DASHATTK_BTLIDLE_01     = 1000502,
        DASHATTK_BTLIDLE_01_START = 1000501,
        DASHATTK_FINISH         = 1000504,
        DASHATTK_RUSH           = 1000503,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1001172,
        EARTHQUAKE_BATTLEIDLE_START = 1001171,
        EARTHQUAKE_FIRE         = 1001173,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000405,
        SKILL_SHOT_01           = 1000403,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        TELEPORT_SKILL_01       = 1001143,
        TELEPORT_SKILL_01_BTLIDLE = 1001142,
        TELEPORT_SKILL_01_START = 1001141,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef SICKLER_ANIM_H__
