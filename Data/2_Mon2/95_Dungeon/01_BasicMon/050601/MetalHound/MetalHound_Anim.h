// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METALHOUND_ANIM_H__
#define METALHOUND_ANIM_H__

namespace MetalHound_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_SHOT   = 1000402,
        BTLIDLE_01_SKILL_SHOT_MIDDLE = 1000404,
        BTLIDLE_01_SKILL_SHOT_START = 1000401,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000512,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000511,
        DASH_PIERCING_ATTK_FINISH = 1000514,
        DASH_PIERCING_ATTK_RUSH = 1000513,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        JUMPDOWNCRASH_DOWN      = 1000702,
        JUMPDOWNCRASH_GETUP     = 1000703,
        JUMPDOWNCRASH_UP        = 1000701,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000405,
        SKILL_SHOT_01           = 1000403,
        STANDUP                 = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef METALHOUND_ANIM_H__
