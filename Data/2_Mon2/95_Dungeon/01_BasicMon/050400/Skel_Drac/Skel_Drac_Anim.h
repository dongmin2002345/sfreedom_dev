// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SKEL_DRAC_ANIM_H__
#define SKEL_DRAC_ANIM_H__

namespace Skel_Drac_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_01_START        = 1000201,
        ATTK_01_02              = 1000203,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_UP        = 1000361,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef SKEL_DRAC_ANIM_H__
