// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEVIL_GUARD_ANIM_H__
#define DEVIL_GUARD_ANIM_H__

namespace Devil_Guard_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_02              = 1000203,
        ATTK_01_START           = 1000201,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_02_SHOT         = 1000217,
        BTLIDLE_02_SHOT_START   = 1000216,
        CROSSATTACK_01_FINISH   = 1000398,
        CROSSATTACK_01_FIRE     = 1000397,
        CROSSATTACK_01_IDLE     = 1000396,
        CROSSATTACK_01_START    = 1000395,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_UP        = 1000361,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_02                 = 1000218,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000380,
        WHIRLWIND_01_FINISH     = 1000381,
        WHIRLWIND_BTLIDLE_01    = 1000379,
        WHIRLWIND_BTLIDLE_01_START = 1000378
    };
}

#endif  // #ifndef DEVIL_GUARD_ANIM_H__
