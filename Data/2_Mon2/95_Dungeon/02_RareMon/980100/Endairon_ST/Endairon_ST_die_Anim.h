// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ENDAIRON_ST_DIE_ANIM_H__
#define ENDAIRON_ST_DIE_ANIM_H__

namespace Endairon_ST_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef ENDAIRON_ST_DIE_ANIM_H__
