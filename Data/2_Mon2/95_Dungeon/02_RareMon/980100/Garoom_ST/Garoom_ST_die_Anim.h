// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GAROOM_ST_DIE_ANIM_H__
#define GAROOM_ST_DIE_ANIM_H__

namespace Garoom_ST_die_Anim
{
    enum
    {
        DIE_01                  = 1000027
    };
}

#endif  // #ifndef GAROOM_ST_DIE_ANIM_H__
