// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SKILLMON_NTT_ANIM_H__
#define SKILLMON_NTT_ANIM_H__

namespace SkillMon_Ntt_Anim
{
    enum
    {
        ATTK_01                 = 1000205,
        IDLE_01                 = 1000001,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000204
    };
}

#endif  // #ifndef SKILLMON_NTT_ANIM_H__
