// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BALGON_ST_ANIM_H__
#define BALGON_ST_ANIM_H__

namespace Balgon_ST_Anim
{
    enum
    {
        ATTK_01                 = 1000066,
        ATTK_02                 = 1000067,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000065,
        DIE_01                  = 1000020,
        DIE_02                  = 1000024,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SKILL_01_ATTACK         = 1000207,
        SKILL_01_BTLIDLE        = 1000206,
        SKILL_01_FINISH         = 1000208,
        SKILL_01_START          = 1000205,
        SKILL_02_ATTACK         = 1000210,
        SKILL_02_BTLIDLE        = 1000209,
        SKILL_03_ATTACK         = 1000212,
        SKILL_03_BTLIDLE        = 1000211,
        STANDUP_01              = 1000026,
        STUN                    = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef BALGON_ST_ANIM_H__
