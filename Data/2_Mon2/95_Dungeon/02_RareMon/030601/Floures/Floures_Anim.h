// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FLOURES_ANIM_H__
#define FLOURES_ANIM_H__

namespace Floures_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_FIN_02             = 1000456,
        ATTK_FIN_02_FIN_01      = 1000458,
        ATTK_FIN_02_FIRE        = 1000457,
        ATTK_FIN_02_START       = 1000455,
        BLOW_DOWN               = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT_START   = 1000451,
        BTLIDLE_01_SKILL_SHOT   = 1000452,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        LIGHTNING_STROKE_01_FINISH = 1000436,
        LIGHTNING_STROKE_01_FIRE = 1000435,
        LIGHTNING_STROKE_01_IDLE = 1000434,
        LIGHTNING_STROKE_01_START = 1000433,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_RETURN_01          = 1000454,
        SKILL_SHOT_01           = 1000453,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef FLOURES_ANIM_H__
