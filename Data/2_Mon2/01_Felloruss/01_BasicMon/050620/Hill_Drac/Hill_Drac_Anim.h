// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HILL_DRAC_ANIM_H__
#define HILL_DRAC_ANIM_H__

namespace Hill_Drac_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        RUN_01                  = 1000051,
        SKILL_01_ATTCK          = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_BTLIDLE        = 1000204,
        SKILL_02_FINISH         = 1000205,
        SKILL_02_START          = 1000203,
        SKILL_03_ATTCK          = 1000207,
        SKILL_03_BTLIDLE        = 1000206,
        SKILL_03_FINISH         = 1000208,
        SKILL_04_ATTCK          = 1000211,
        SKILL_04_BTLIDLE        = 1000210,
        SKILL_04_START          = 1000209,
        STANDUP_01              = 1000024
    };
}

#endif  // #ifndef HILL_DRAC_ANIM_H__
