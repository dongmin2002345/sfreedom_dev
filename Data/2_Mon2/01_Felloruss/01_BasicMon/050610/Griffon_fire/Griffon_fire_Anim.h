// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GRIFFON_FIRE_ANIM_H__
#define GRIFFON_FIRE_ANIM_H__

namespace Griffon_fire_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT_START   = 1001151,
        BTLIDLE_01_SKILL_SHOT   = 1001152,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTK           = 1000202,
        SKILL_01_FINISH         = 1000203,
        SKILL_01_START          = 1000201,
        SKILL_ATTK_01           = 1000212,
        SKILL_ATTK_01_START     = 1000210,
        SKILL_ATTK_BTLIDLE_01   = 1000211,
        SKILL_SHOT_01           = 1001153,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef GRIFFON_FIRE_ANIM_H__
