// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GRIFFON_ELECTRON_ANIM_H__
#define GRIFFON_ELECTRON_ANIM_H__

namespace Griffon_electron_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTK           = 1000202,
        SKILL_01_FINISH         = 1000203,
        SKILL_01_START          = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000632,
        SUMMON_THORN_BTLIDLE_START = 1000631,
        SUMMON_THORN_FIRE       = 1000633,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef GRIFFON_ELECTRON_ANIM_H__
