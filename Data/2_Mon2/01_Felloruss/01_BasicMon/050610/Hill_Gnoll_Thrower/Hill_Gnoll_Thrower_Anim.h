// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HILL_GNOLL_THROWER_ANIM_H__
#define HILL_GNOLL_THROWER_ANIM_H__

namespace Hill_Gnoll_Thrower_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BTLIDLE_01_SKILL_SHOT   = 1000311,
        BTLIDLE_01_SKILL_SHOT_START = 1000310,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        SKILL_SHOT_01           = 1000312,
        STAND_UP                = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef HILL_GNOLL_THROWER_ANIM_H__
