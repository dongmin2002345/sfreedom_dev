// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HELL_FIRE_DOC_ANIM_H__
#define HELL_FIRE_DOC_ANIM_H__

namespace Hell_Fire_Doc_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_02              = 1000203,
        ATTK_01_START           = 1000201,
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BITLIDLE                = 1000005,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BREATH_01               = 1000461,
        BREATH_01_FINISH        = 1000462,
        BREATH_02               = 1000463,
        BREATH_BTLIDLE_01       = 1000460,
        BREATH_BTLIDLE_01_START = 1000459,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_02_SHOT         = 1000218,
        BTLIDLE_02_SHOT_START   = 1000217,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE                     = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        ENERGY_EXPLOSION_BTLIDLE = 1000375,
        ENERGY_EXPLOSION_BTLIDLE_START = 1000374,
        ENERGY_EXPLOSION_FIRE   = 1000376,
        FURY                    = 1000900,
        OPENING_01              = 0,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_02                 = 1000219,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef HELL_FIRE_DOC_ANIM_H__
