// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TOMB_YELLOW_ANIM_H__
#define TOMB_YELLOW_ANIM_H__

namespace Tomb_Yellow_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        STANDUP                 = 0,
        STUN                    = 1000025,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef TOMB_YELLOW_ANIM_H__
