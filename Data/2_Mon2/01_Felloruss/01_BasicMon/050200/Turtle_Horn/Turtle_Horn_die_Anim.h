// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TURTLE_HORN_DIE_ANIM_H__
#define TURTLE_HORN_DIE_ANIM_H__

namespace Turtle_Horn_die_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef TURTLE_HORN_DIE_ANIM_H__
