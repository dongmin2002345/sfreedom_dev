// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PRING_LEAF_ANIM_H__
#define PRING_LEAF_ANIM_H__

namespace Pring_Leaf_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000380,
        WHIRLWIND_01_FINISH     = 1000381,
        WHIRLWIND_BTLIDLE_01    = 1000379,
        WHIRLWIND_BTLIDLE_01_START = 1000378
    };
}

#endif  // #ifndef PRING_LEAF_ANIM_H__
