// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FISH_BONE_DIE_ANIM_H__
#define FISH_BONE_DIE_ANIM_H__

namespace Fish_Bone_die_Anim
{
    enum
    {
        DIE_01                  = 1000021
    };
}

#endif  // #ifndef FISH_BONE_DIE_ANIM_H__
