// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef COCKOOTUS_ANIM_H__
#define COCKOOTUS_ANIM_H__

namespace Cockootus_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_FIN_01             = 1001162,
        ATTK_FIN_01_FIN_01      = 1001164,
        ATTK_FIN_01_FIRE        = 1001163,
        ATTK_FIN_01_START       = 1001161,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DASH_PIERCING_ATTK_BTLIDLE_01 = 1000512,
        DASH_PIERCING_ATTK_BTLIDLE_01_START = 1000511,
        DASH_PIERCING_ATTK_FINISH = 1000514,
        DASH_PIERCING_ATTK_RUSH = 1000513,
        DIE_01                  = 1000020,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY                    = 1000900,
        OPENING                 = 1000081,
        RUN_01                  = 1000055,
        SHOT_AROUND_01          = 1001001,
        SHOT_AROUND_01_FIRE     = 1001003,
        SHOT_AROUND_01_START    = 1001002,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        SUMMON_THORN_BTLIDLE    = 1000632,
        SUMMON_THORN_BTLIDLE_START = 1000631,
        SUMMON_THORN_FIRE       = 1000633
    };
}

#endif  // #ifndef COCKOOTUS_ANIM_H__
