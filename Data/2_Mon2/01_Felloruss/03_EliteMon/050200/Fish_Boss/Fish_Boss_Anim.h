// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FISH_BOSS_ANIM_H__
#define FISH_BOSS_ANIM_H__

namespace Fish_Boss_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01_BLENDING         = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY                    = 1000900,
        OPENING                 = 1000082,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WHIRLWIND_01            = 1000384,
        WHIRLWIND_01_FINISH     = 1000385,
        WHIRLWIND_BTLIDLE_01    = 1000383,
        WHIRLWIND_BTLIDLE_01_START = 1000382
    };
}

#endif  // #ifndef FISH_BOSS_ANIM_H__
