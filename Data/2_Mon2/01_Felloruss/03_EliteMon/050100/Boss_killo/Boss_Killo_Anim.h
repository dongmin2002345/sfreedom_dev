// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BOSS_KILLO_ANIM_H__
#define BOSS_KILLO_ANIM_H__

namespace Boss_Killo_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        ATTK_04_01              = 1000211,
        ATTK_04_01_START        = 1000210,
        ATTK_04_02              = 1000212,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY                    = 1000900,
        OPENING                 = 1000082,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SKILL_ATTK_01           = 1000303,
        STANDUP                 = 1000024,
        STUN                    = 1000025
    };
}

#endif  // #ifndef BOSS_KILLO_ANIM_H__
