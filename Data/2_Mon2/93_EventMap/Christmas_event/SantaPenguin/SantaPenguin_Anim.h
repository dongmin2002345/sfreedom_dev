// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SANTAPENGUIN_ANIM_H__
#define SANTAPENGUIN_ANIM_H__

namespace SantaPenguin_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        DASHATTK_BTLIDLE_01     = 1000326,
        DASHATTK_BTLIDLE_01_START = 1000325,
        DASHATTK_FINISH         = 1000328,
        DASHATTK_RUSH           = 1000327,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FIRE         = 1000388,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        MASSIVE_PROJECTILE_BTLIDLE = 1000334,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000333,
        MASSIVE_PROJECTILE_FINISH = 1000336,
        MASSIVE_PROJECTILE_FIRE = 1000335,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_ATTK_01           = 1000303,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef SANTAPENGUIN_ANIM_H__
