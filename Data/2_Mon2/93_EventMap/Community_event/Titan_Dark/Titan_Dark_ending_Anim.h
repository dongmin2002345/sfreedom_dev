// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TITAN_DARK_ENDING_ANIM_H__
#define TITAN_DARK_ENDING_ANIM_H__

namespace Titan_Dark_ending_Anim
{
    enum
    {
        DIE_01                  = 1000020
    };
}

#endif  // #ifndef TITAN_DARK_ENDING_ANIM_H__
