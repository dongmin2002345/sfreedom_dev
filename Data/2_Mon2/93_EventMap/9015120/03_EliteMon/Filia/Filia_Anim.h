// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FILIA_ANIM_H__
#define FILIA_ANIM_H__

namespace Filia_Anim
{
    enum
    {
        ATTK_01_ATTACK          = 1000066,
        ATTK_01_BTLIDLE         = 1000065,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DIE_02                  = 1000024,
        DIE_BLENDING_01         = 1000021,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        FURY_01                 = 1000900,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000082,
        SHOT_02_ATTACK          = 1000076,
        SHOT_02_BTLIDLE         = 1000075,
        SKILL_01_ATTACK         = 1000206,
        SKILL_01_BTLIDLE        = 1000205,
        SKILL_02_ATTACK         = 1000208,
        SKILL_02_BTLIDLE        = 1000207,
        SKILL_03_ATTACK         = 1000210,
        SKILL_03_BTLIDLE        = 1000209,
        SKILL_04_ATTACK         = 1000212,
        SKILL_04_BTLIDLE        = 1000211,
        STANUP_01               = 1000026,
        STUN_01                 = 1000025,
        TALK_01                 = 1000006,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef FILIA_ANIM_H__
