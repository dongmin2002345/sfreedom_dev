// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PUMPKIN_HAZARD_ANIM_H__
#define PUMPKIN_HAZARD_ANIM_H__

namespace Pumpkin_Hazard_Anim
{
    enum
    {
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef PUMPKIN_HAZARD_ANIM_H__
