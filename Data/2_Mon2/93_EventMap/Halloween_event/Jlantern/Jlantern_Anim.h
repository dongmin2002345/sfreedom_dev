// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef JLANTERN_ANIM_H__
#define JLANTERN_ANIM_H__

namespace Jlantern_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_01_START        = 1000201,
        ATTK_01_02              = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000252,
        BTLIDLE_01_SHOT_START   = 1000251,
        BTLIDLE_02_SHOT         = 1000255,
        BTLIDLE_02_SHOT_START   = 1000254,
        DASHATTK_BTLIDLE_01     = 1000412,
        DASHATTK_BTLIDLE_01_START = 1000411,
        DASHATTK_FINISH         = 1000414,
        DASHATTK_RUSH           = 1000413,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        MASSIVE_BTLIDLE         = 1000612,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000611,
        MASSIVE_PROJECTILE_FIRE = 1000613,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000253,
        SHOT_02                 = 1000256,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000803,
        WHIRLWIND_01_FINISH     = 1000804,
        WHIRLWIND_BTLIDLE_01    = 1000802,
        WHIRLWIND_BTLIDLE_01_START = 1000801
    };
}

#endif  // #ifndef JLANTERN_ANIM_H__
