// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef JLANTERN_ENDING_01_ANIM_H__
#define JLANTERN_ENDING_01_ANIM_H__

namespace Jlantern_ending_01_Anim
{
    enum
    {
        ENDING_01               = 1000021
    };
}

#endif  // #ifndef JLANTERN_ENDING_01_ANIM_H__
