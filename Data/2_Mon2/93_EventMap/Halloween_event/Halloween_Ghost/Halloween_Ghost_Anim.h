// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HALLOWEEN_GHOST_ANIM_H__
#define HALLOWEEN_GHOST_ANIM_H__

namespace Halloween_Ghost_Anim
{
    enum
    {
        ATTK_01                 = 1000203,
        ATTK_01_BTLDILE         = 1000202,
        ATTK_01_START           = 1000201,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef HALLOWEEN_GHOST_ANIM_H__
