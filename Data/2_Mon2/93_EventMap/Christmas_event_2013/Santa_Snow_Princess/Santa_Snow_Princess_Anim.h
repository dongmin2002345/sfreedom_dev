// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SANTA_SNOW_PRINCESS_ANIM_H__
#define SANTA_SNOW_PRINCESS_ANIM_H__

namespace Santa_Snow_Princess_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000214,
        BTLIDLE_01_SHOT_START   = 1000213,
        BTLIDLE_01_SKILL_ATTK   = 1000302,
        BTLIDLE_01_SKILL_ATTK_START = 1000301,
        BTLIDLE_02_SKILL_ATTK   = 1000305,
        BTLIDLE_02_SKILL_ATTK_START = 1000304,
        BTLIDLE_03_SKILL_ATTK   = 1000308,
        BTLIDLE_03_SKILL_ATTK_START = 1000307,
        DIE_01                  = 1000020,
        DIE_BLOWUP_DOWN_01      = 1000044,
        DIE_BLOWUP_LOOP_01      = 1000043,
        DIE_DMG_01              = 1000041,
        DIE_DMG_BLENDING_01     = 1000042,
        DIE_IDLE_01             = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        JUMPDOWNCRASH_DOWN      = 1000362,
        JUMPDOWNCRASH_GETUP     = 1000363,
        JUMPDOWNCRASH_UP        = 1000361,
        MASSIVE_PROJECTILE_BTLIDLE = 1000334,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000333,
        MASSIVE_PROJECTILE_FIRE = 1000335,
        OPENING_01              = 1000081,
        OPENING_01              = 1000082,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000215,
        SHOT_AROUND_01          = 1000420,
        SHOT_AROUND_01_FIRE     = 1000419,
        SHOT_AROUND_01_START    = 1000418,
        SKILL_ATTK_01           = 1000303,
        SKILL_ATTK_02           = 1000306,
        SKILL_ATTK_03           = 1000309,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef SANTA_SNOW_PRINCESS_ANIM_H__
