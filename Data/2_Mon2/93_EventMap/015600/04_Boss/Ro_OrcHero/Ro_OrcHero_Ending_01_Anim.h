// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RO_ORCHERO_ENDING_01_ANIM_H__
#define RO_ORCHERO_ENDING_01_ANIM_H__

namespace Ro_OrcHero_Ending_01_Anim
{
    enum
    {
        ENDING                  = 1000021
    };
}

#endif  // #ifndef RO_ORCHERO_ENDING_01_ANIM_H__
