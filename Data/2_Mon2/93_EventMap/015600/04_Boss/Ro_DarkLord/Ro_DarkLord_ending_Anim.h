// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RO_DARKLORD_ENDING_ANIM_H__
#define RO_DARKLORD_ENDING_ANIM_H__

namespace Ro_DarkLord_ending_Anim
{
    enum
    {
        ENDING_01               = 1000021
    };
}

#endif  // #ifndef RO_DARKLORD_ENDING_ANIM_H__
