// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WATERMELON_ANIM_H__
#define WATERMELON_ANIM_H__

namespace Watermelon_Anim
{
    enum
    {
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef WATERMELON_ANIM_H__
