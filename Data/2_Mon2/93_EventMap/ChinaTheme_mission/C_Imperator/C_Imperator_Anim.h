// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef C_IMPERATOR_ANIM_H__
#define C_IMPERATOR_ANIM_H__

namespace C_Imperator_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000205,
        BTLIDLE_01_SHOT_START   = 1000204,
        DASHATTK_BTLIDLE_01     = 1000502,
        DASHATTK_BTLIDLE_01_START = 1000501,
        DASHATTK_FINISH         = 1000504,
        DASHATTK_RUSH           = 1000503,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        DMG_03                  = 1000013,
        DMG_BLENDING_01         = 1000015,
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081,
        SHOT_01                 = 1000206,
        SKILL_01_ATTK           = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000055
    };
}

#endif  // #ifndef C_IMPERATOR_ANIM_H__
