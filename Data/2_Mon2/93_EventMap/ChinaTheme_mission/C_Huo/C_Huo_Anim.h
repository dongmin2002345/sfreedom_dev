// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef C_HUO_ANIM_H__
#define C_HUO_ANIM_H__

namespace C_Huo_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        BTLIDLE_01              = 1000005,
        BTLIDLE_03_SHOT         = 1000251,
        BTLIDLE_03_SHOT_START   = 1000250,
        DIE_01                  = 1000020,
        MASSIVE_PROJECTILE_BTLIDLE = 1000338,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000337,
        MASSIVE_PROJECTILE_FINISH = 1000340,
        MASSIVE_PROJECTILE_FIRE = 1000339,
        SHOT_03                 = 1000252,
        SKILL_01_ATTCK          = 1000202,
        SKILL_01_BTLIDLE        = 1000201,
        SKILL_02_ATTCK          = 1000204,
        SKILL_02_BTLIDLE        = 1000203,
        SKILL_03_ATTCK          = 1000206,
        SKILL_03_BTLIDLE        = 1000205
    };
}

#endif  // #ifndef C_HUO_ANIM_H__
