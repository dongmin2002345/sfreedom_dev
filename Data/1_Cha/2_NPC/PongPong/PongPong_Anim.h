// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PONGPONG_ANIM_H__
#define PONGPONG_ANIM_H__

namespace PongPong_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        IDLE_04                 = 1000004,
        IDLE_TALK               = 1000005,
        OPENGIN_01              = 1000081,
        OPENGIN_02              = 1000082,
        RUN_01                  = 1000020,
        TALK_01                 = 1000006
    };
}

#endif  // #ifndef PONGPONG_ANIM_H__
