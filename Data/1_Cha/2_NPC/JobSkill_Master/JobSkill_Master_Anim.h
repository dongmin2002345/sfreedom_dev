// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef JOBSKILL_MASTER_ANIM_H__
#define JOBSKILL_MASTER_ANIM_H__

namespace JobSkill_Master_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef JOBSKILL_MASTER_ANIM_H__
