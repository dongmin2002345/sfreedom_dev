// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BORIS_ANIM_H__
#define BORIS_ANIM_H__

namespace Boris_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        TALK_01                 = 1000005,
        TALK_02                 = 1000006
    };
}

#endif  // #ifndef BORIS_ANIM_H__
