// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FISHERMAN_01_ANIM_H__
#define FISHERMAN_01_ANIM_H__

namespace Fisherman_01_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef FISHERMAN_01_ANIM_H__
