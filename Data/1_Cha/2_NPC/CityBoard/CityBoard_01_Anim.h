// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CITYBOARD_01_ANIM_H__
#define CITYBOARD_01_ANIM_H__

namespace CityBoard_01_Anim
{
    enum
    {
        IDLE_01                 = 0
    };
}

#endif  // #ifndef CITYBOARD_01_ANIM_H__
