// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MYHOME_ARBEIT_BOARD_ANIM_H__
#define MYHOME_ARBEIT_BOARD_ANIM_H__

namespace MYhome_Arbeit_Board_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef MYHOME_ARBEIT_BOARD_ANIM_H__
