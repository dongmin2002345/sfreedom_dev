// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PALMIR_ANIM_H__
#define PALMIR_ANIM_H__

namespace Palmir_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        IDLE_04                 = 1000004,
        SPELL_01                = 1000201,
        TALK                    = 1000005
    };
}

#endif  // #ifndef PALMIR_ANIM_H__
