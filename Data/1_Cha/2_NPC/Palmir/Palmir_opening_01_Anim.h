// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PALMIR_OPENING_01_ANIM_H__
#define PALMIR_OPENING_01_ANIM_H__

namespace Palmir_opening_01_Anim
{
    enum
    {
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef PALMIR_OPENING_01_ANIM_H__
