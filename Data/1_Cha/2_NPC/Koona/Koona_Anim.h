// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KOONA_ANIM_H__
#define KOONA_ANIM_H__

namespace Koona_Anim
{
    enum
    {
        IDLE_01                 = 0,
        IDLE_02                 = 1000002
    };
}

#endif  // #ifndef KOONA_ANIM_H__
