// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SHANA_ANIM_H__
#define SHANA_ANIM_H__

namespace SHANA_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef SHANA_ANIM_H__
