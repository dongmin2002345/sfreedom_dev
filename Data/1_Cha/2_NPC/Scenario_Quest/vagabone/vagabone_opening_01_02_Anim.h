// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAGABONE_OPENING_01_02_ANIM_H__
#define VAGABONE_OPENING_01_02_ANIM_H__

namespace vagabone_opening_01_02_Anim
{
    enum
    {
        OPENING01_02            = 1000001
    };
}

#endif  // #ifndef VAGABONE_OPENING_01_02_ANIM_H__
