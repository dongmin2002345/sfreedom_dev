// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PALIS_02_ANIM_H__
#define PALIS_02_ANIM_H__

namespace palis_02_Anim
{
    enum
    {
        ATTCK_01                = 1000061,
        CLOTH_01                = 1000100,
        ENDING_02               = 1000020,
        IDLE                    = 1000001,
        IDLE_02                 = 1000002,
        OPENING                 = 1000081,
        OPENING_IDLE            = 1000004,
        TALK_02                 = 1000005,
        TALK_03                 = 1000006
    };
}

#endif  // #ifndef PALIS_02_ANIM_H__
