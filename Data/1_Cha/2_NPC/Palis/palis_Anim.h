// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PALIS_ANIM_H__
#define PALIS_ANIM_H__

namespace palis_Anim
{
    enum
    {
        ENDING                  = 1000003,
        IDLE                    = 1000000,
        OPENING                 = 1000001,
        OPENING_IDLE            = 1000004,
        TALK                    = 1000002
    };
}

#endif  // #ifndef PALIS_ANIM_H__
