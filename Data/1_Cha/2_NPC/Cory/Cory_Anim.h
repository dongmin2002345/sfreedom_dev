// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CORY_ANIM_H__
#define CORY_ANIM_H__

namespace Cory_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef CORY_ANIM_H__
