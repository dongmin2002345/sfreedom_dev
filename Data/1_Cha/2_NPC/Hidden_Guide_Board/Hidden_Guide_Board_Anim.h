// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HIDDEN_GUIDE_BOARD_ANIM_H__
#define HIDDEN_GUIDE_BOARD_ANIM_H__

namespace Hidden_Guide_Board_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef HIDDEN_GUIDE_BOARD_ANIM_H__
