// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FAIRY_OF_TRUTH_ANIM_H__
#define FAIRY_OF_TRUTH_ANIM_H__

namespace Fairy_of_Truth_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef FAIRY_OF_TRUTH_ANIM_H__
