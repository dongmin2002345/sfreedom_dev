// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GWALTER_ANIM_H__
#define GWALTER_ANIM_H__

namespace Gwalter_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        IDLE_04                 = 1000004,
        IDLE_05                 = 1000005
    };
}

#endif  // #ifndef GWALTER_ANIM_H__
