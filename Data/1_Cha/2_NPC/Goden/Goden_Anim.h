// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GODEN_ANIM_H__
#define GODEN_ANIM_H__

namespace Goden_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000002,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef GODEN_ANIM_H__
