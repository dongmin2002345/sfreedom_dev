// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WALTRE_SOLDIERS_02_ANIM_H__
#define WALTRE_SOLDIERS_02_ANIM_H__

namespace waltre_Soldiers_02_Anim
{
    enum
    {
        IDLE_02                 = 1000000
    };
}

#endif  // #ifndef WALTRE_SOLDIERS_02_ANIM_H__
