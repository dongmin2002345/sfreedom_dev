// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MARV_ANIM_H__
#define MARV_ANIM_H__

namespace Marv_Anim
{
    enum
    {
        IDLE_01_01              = 1000000,
        IDLE_02_01              = 1000001
    };
}

#endif  // #ifndef MARV_ANIM_H__
