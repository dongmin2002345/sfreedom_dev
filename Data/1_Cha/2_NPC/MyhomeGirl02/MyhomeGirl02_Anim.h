// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MYHOMEGIRL02_ANIM_H__
#define MYHOMEGIRL02_ANIM_H__

namespace MyhomeGirl02_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef MYHOMEGIRL02_ANIM_H__
