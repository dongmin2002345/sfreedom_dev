// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PRIMO_FISHING_ANIM_H__
#define PRIMO_FISHING_ANIM_H__

namespace Primo_fishing_Anim
{
    enum
    {
        FISHING_01              = 1000001
    };
}

#endif  // #ifndef PRIMO_FISHING_ANIM_H__
