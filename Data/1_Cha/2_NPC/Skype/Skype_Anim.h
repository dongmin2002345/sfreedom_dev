// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SKYPE_ANIM_H__
#define SKYPE_ANIM_H__

namespace Skype_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        SPELL_01                = 1000201,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef SKYPE_ANIM_H__
