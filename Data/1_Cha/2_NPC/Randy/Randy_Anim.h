// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RANDY_ANIM_H__
#define RANDY_ANIM_H__

namespace Randy_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef RANDY_ANIM_H__
