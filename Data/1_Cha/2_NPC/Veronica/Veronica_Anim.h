// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VERONICA_ANIM_H__
#define VERONICA_ANIM_H__

namespace Veronica_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        TALK_01                 = 1000005,
        TALK_02                 = 1000006
    };
}

#endif  // #ifndef VERONICA_ANIM_H__
