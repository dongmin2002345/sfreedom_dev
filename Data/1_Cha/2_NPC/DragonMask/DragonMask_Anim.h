// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRAGONMASK_ANIM_H__
#define DRAGONMASK_ANIM_H__

namespace DragonMask_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        LEVEL_01                = 1000021,
        LEVEL_02                = 1000022,
        LEVEL_03                = 1000023
    };
}

#endif  // #ifndef DRAGONMASK_ANIM_H__
