// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CENTAURUS_OPENING_01_ANIM_H__
#define CENTAURUS_OPENING_01_ANIM_H__

namespace Centaurus_opening_01_Anim
{
    enum
    {
        ACTION_01               = 1000001,
        ACTION_02               = 1000002,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef CENTAURUS_OPENING_01_ANIM_H__
