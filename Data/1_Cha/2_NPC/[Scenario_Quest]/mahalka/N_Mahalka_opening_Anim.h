// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_MAHALKA_OPENING_ANIM_H__
#define N_MAHALKA_OPENING_ANIM_H__

namespace N_Mahalka_opening_Anim
{
    enum
    {
        ACTION                  = 1000001,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef N_MAHALKA_OPENING_ANIM_H__
