// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PORKY_ANIM_H__
#define PORKY_ANIM_H__

namespace Porky_Anim
{
    enum
    {
        ACTION_01               = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef PORKY_ANIM_H__
