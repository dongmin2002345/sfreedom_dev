// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MIMIR_KING_ENDING_01_ANIM_H__
#define MIMIR_KING_ENDING_01_ANIM_H__

namespace Mimir_king_ending_01_Anim
{
    enum
    {
        ACTION_01               = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef MIMIR_KING_ENDING_01_ANIM_H__
