// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TUTORIAL_02_ANIM_H__
#define TUTORIAL_02_ANIM_H__

namespace tutorial_02_Anim
{
    enum
    {
        IDLE                    = 1000001,
        PLAY01                  = 1000002
    };
}

#endif  // #ifndef TUTORIAL_02_ANIM_H__
