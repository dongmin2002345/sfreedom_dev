// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ALVIDA_OPENING_01_ANIM_H__
#define ALVIDA_OPENING_01_ANIM_H__

namespace Alvida_opening_01_Anim
{
    enum
    {
        ACTION                  = 1000001,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef ALVIDA_OPENING_01_ANIM_H__
