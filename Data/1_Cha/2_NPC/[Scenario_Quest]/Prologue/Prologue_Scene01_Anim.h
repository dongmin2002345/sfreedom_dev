// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PROLOGUE_SCENE01_1_ANIM_H__
#define PROLOGUE_SCENE01_1_ANIM_H__

namespace Prologue_Scene01_1_Anim
{
    enum
    {
        SEQUENCE_01             = 1000001,
        SEQUENCE_02             = 1000002,
        SEQUENCE_03             = 1000003
    };
}

#endif  // #ifndef PROLOGUE_SCENE01_1_ANIM_H__
