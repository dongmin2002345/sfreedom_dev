// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PROLOGUE_SCENE02_ANIM_H__
#define PROLOGUE_SCENE02_ANIM_H__

namespace Prologue_Scene02_Anim
{
    enum
    {
        SEQUENCE_01             = 1000001
    };
}

#endif  // #ifndef PROLOGUE_SCENE02_ANIM_H__
