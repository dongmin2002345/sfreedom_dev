// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef B_ARAM_MUTISHA_OPENING_CAMERA_ANIM_H__
#define B_ARAM_MUTISHA_OPENING_CAMERA_ANIM_H__

namespace B_aram_Mutisha_opening_camera_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef B_ARAM_MUTISHA_OPENING_CAMERA_ANIM_H__
