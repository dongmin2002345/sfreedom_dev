// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAGABONE_ENDING_03_ANIM_H__
#define VAGABONE_ENDING_03_ANIM_H__

namespace vagabone_ending_03_Anim
{
    enum
    {
        ACTION_01               = 1000001,
        ACTION_02               = 1000002,
        ACTION_03               = 1000003,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef VAGABONE_ENDING_03_ANIM_H__
