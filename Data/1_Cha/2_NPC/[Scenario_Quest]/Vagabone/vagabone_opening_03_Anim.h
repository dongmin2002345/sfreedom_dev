// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAGABONE_OPENING_03_ANIM_H__
#define VAGABONE_OPENING_03_ANIM_H__

namespace vagabone_opening_03_Anim
{
    enum
    {
        ACTION                  = 1000001,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef VAGABONE_OPENING_03_ANIM_H__
