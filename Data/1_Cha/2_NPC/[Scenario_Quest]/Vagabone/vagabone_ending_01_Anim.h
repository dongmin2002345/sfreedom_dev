// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAGABONE_ENDING_01_ANIM_H__
#define VAGABONE_ENDING_01_ANIM_H__

namespace vagabone_ending_01_Anim
{
    enum
    {
        ACTION                  = 1000001,
        IDLE                    = 1000000
    };
}

#endif  // #ifndef VAGABONE_ENDING_01_ANIM_H__
