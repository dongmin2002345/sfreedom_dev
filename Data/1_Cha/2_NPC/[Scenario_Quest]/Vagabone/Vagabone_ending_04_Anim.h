// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAGABONE_ENDING_04_ANIM_H__
#define VAGABONE_ENDING_04_ANIM_H__

namespace Vagabone_ending_04_Anim
{
    enum
    {
        ACTION                  = 1000001
    };
}

#endif  // #ifndef VAGABONE_ENDING_04_ANIM_H__
