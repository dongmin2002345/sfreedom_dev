// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GINA_ANIM_H__
#define GINA_ANIM_H__

namespace Gina_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef GINA_ANIM_H__
