// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SHADY_ANIM_H__
#define SHADY_ANIM_H__

namespace Shady_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003
    };
}

#endif  // #ifndef SHADY_ANIM_H__
