// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GOOD_CENTAURUS_02_ANIM_H__
#define GOOD_CENTAURUS_02_ANIM_H__

namespace Good_Centaurus_02_Anim
{
    enum
    {
        BTLIDLE_01              = 1000060,
        DIE_01                  = 1000020,
        IDLE_01                 = 1000001,
        SCEN_SHOT_01            = 1000061
    };
}

#endif  // #ifndef GOOD_CENTAURUS_02_ANIM_H__
