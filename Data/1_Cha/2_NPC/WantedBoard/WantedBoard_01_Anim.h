// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WANTEDBOARD_01_ANIM_H__
#define WANTEDBOARD_01_ANIM_H__

namespace WantedBoard_01_Anim
{
    enum
    {
        IDLE_01                 = 0
    };
}

#endif  // #ifndef WANTEDBOARD_01_ANIM_H__
