// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WOOKIBOKI_ANIM_H__
#define WOOKIBOKI_ANIM_H__

namespace Wookiboki_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef WOOKIBOKI_ANIM_H__
