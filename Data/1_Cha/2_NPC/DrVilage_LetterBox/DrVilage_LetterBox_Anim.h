// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRVILAGE_LETTERBOX_ANIM_H__
#define DRVILAGE_LETTERBOX_ANIM_H__

namespace DrVilage_LetterBox_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef DRVILAGE_LETTERBOX_ANIM_H__
