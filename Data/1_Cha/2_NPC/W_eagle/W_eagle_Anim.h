// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef W_EAGLE_ANIM_H__
#define W_EAGLE_ANIM_H__

namespace W_eagle_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef W_EAGLE_ANIM_H__
