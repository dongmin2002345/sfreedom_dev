// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef NIKOLA_CAT_ANIM_H__
#define NIKOLA_CAT_ANIM_H__

namespace Nikola_Cat_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002
    };
}

#endif  // #ifndef NIKOLA_CAT_ANIM_H__
