// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GRAVE_STONE_ANIM_H__
#define GRAVE_STONE_ANIM_H__

namespace Grave_stone_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef GRAVE_STONE_ANIM_H__
