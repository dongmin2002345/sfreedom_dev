// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef 1ST_BIRTHDAY_CAKE_ANIM_H__
#define 1ST_BIRTHDAY_CAKE_ANIM_H__

namespace 1st_birthday_cake_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef 1ST_BIRTHDAY_CAKE_ANIM_H__
