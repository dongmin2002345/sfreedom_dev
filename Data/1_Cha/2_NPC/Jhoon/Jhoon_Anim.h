// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef JHOON_ANIM_H__
#define JHOON_ANIM_H__

namespace Jhoon_Anim
{
    enum
    {
        IDLE_01                 = 10000000,
        TALK_01                 = 10000005
    };
}

#endif  // #ifndef JHOON_ANIM_H__
