// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GASHA_MACHINE_01_ANIM_H__
#define GASHA_MACHINE_01_ANIM_H__

namespace Gasha_Machine_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef GASHA_MACHINE_01_ANIM_H__
