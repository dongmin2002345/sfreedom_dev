// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WALTRE_02_ANIM_H__
#define WALTRE_02_ANIM_H__

namespace waltre_02_Anim
{
    enum
    {
        BTLIDLE_01              = 1000000,
        BTLIDLE_02              = 1000001,
        BTLTALK_01              = 1000005,
        WALK_01                 = 1000010
    };
}

#endif  // #ifndef WALTRE_02_ANIM_H__
