// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TU_WALTER_ANIM_H__
#define TU_WALTER_ANIM_H__

namespace tu_walter_Anim
{
    enum
    {
        BTLIDLE                 = 1000003,
        IDLE_01                 = 1000001,
        TALK                    = 1000005,
        WALK                    = 1000055
    };
}

#endif  // #ifndef TU_WALTER_ANIM_H__
