// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CINTAMANI_WIND_ANIM_H__
#define CINTAMANI_WIND_ANIM_H__

namespace Cintamani_Wind_Anim
{
    enum
    {
        CLEAN_01                = 1000003,
        IDLE_01                 = 1000001,
        PURIFY_01               = 1000002
    };
}

#endif  // #ifndef CINTAMANI_WIND_ANIM_H__
