// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUMDUNAMIC_ROLLING_ANIM_H__
#define SUMDUNAMIC_ROLLING_ANIM_H__

namespace SumDunamic_Rolling_Anim
{
    enum
    {
        ATTACK_01               = 1000206,
        ATTACK_01_START         = 1000204,
        ATTACK_02_01            = 1000208,
        ATTACK_02_02            = 1000209,
        ATTACK_02_START         = 1000207,
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RE_GAIN_01              = 1000110,
        RUN_01                  = 1000055,
        SKILL_ATTACK_01         = 1000301,
        SKILL_ATTACK_02         = 1000325,
        SKILL_ATTACK_02_M       = 1000324,
        SKILL_ATTACK_02_START   = 1000323,
        SKILL_ATTACK_03_01      = 1000380,
        SKILL_ATTACK_03_02      = 1000381,
        SKILL_ATTACK_03_START   = 1000378,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef SUMDUNAMIC_ROLLING_ANIM_H__
