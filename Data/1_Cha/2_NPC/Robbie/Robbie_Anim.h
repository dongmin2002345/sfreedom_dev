// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ROBBIE_ANIM_H__
#define ROBBIE_ANIM_H__

namespace Robbie_Anim
{
    enum
    {
        DANCE_01                = 1000002,
        DANCE_02                = 1000003,
        IDLE_01                 = 1000001,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef ROBBIE_ANIM_H__
