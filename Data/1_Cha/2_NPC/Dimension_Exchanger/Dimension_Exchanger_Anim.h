// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DIMENSION_EXCHANGER_ANIM_H__
#define DIMENSION_EXCHANGER_ANIM_H__

namespace Dimension_Exchanger_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef DIMENSION_EXCHANGER_ANIM_H__
