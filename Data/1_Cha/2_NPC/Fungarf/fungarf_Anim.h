// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FUNGARF_ANIM_H__
#define FUNGARF_ANIM_H__

namespace fungarf_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        TALK_01                 = 1000005,
        TALK_02                 = 1000006,
        WALK_01                 = 1000010
    };
}

#endif  // #ifndef FUNGARF_ANIM_H__
