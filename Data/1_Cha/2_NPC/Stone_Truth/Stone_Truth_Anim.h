// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef STONE_TRUTH_ANIM_H__
#define STONE_TRUTH_ANIM_H__

namespace Stone_Truth_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef STONE_TRUTH_ANIM_H__
