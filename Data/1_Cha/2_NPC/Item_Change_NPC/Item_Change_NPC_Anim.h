// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ITEM_CHANGE_NPC_ANIM_H__
#define ITEM_CHANGE_NPC_ANIM_H__

namespace Item_Change_NPC_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        TALK_01                 = 1000005
    };
}

#endif  // #ifndef ITEM_CHANGE_NPC_ANIM_H__
