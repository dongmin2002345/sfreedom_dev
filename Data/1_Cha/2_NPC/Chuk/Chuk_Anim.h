// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHUK_ANIM_H__
#define CHUK_ANIM_H__

namespace Chuk_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001
    };
}

#endif  // #ifndef CHUK_ANIM_H__
