// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef 00_BASE_ANIM_H__
#define 00_BASE_ANIM_H__

namespace 00_Base_Anim
{
    enum
    {
        BOXING_BTLIDLE_01       = 512005,
        BOXING_FU_SKILL_MRG_FINISH_IMPECT_01 = 512514,
        BOXING_FU_SKILL_MRG_FINISH_MOVE_01 = 512513,
        BOXING_FU_SKILL_MRG_FUSION_BOOST_01_ATTACK = 512517,
        BOXING_FU_SKILL_MRG_FUSION_BOOST_01_START = 512512,
        BOXING_FU_SKILL_MRG_GHOST_FIGHTER_01_ATTACK = 512511,
        BOXING_FU_SKILL_MRG_GHOST_FIGHTER_01_START = 512522,
        BOXING_FU_SKILL_TWN_ABSOULTE_PROTECT_01 = 512414,
        BOXING_FU_SKILL_TWN_FIGHTING_SPIRIT_01_ATTACK = 512418,
        BOXING_FU_SKILL_TWN_FIGHTING_SPIRIT_01_ATTACK_L = 512419,
        BOXING_FU_SKILL_TWN_FIGHTING_SPIRIT_01_BTIDLE = 512417,
        BOXING_FU_SKILL_TWN_FIGHTING_SPIRIT_01_FINISH = 512420,
        BOXING_FU_SKILL_TWN_FIGHTING_SPIRIT_01_START = 512416,
        BOXING_FU_SKILL_TWN_FIST_STORM_01_ATTACK = 512421,
        BOXING_FU_SKILL_TWN_FIST_STORM_01_START = 512423,
        BOXING_FU_SKILL_TWN_IMPECT_BLOW_01 = 512415,
        BOXING_IDLE_01          = 512200,
        BOXING_IDLE_02          = 512201,
        BOXING_IDLE_03          = 512202,
        BOXING_MC_ATTK_01       = 512061,
        BOXING_MC_ATTK_02       = 512062,
        BOXING_MC_ATTK_04       = 512064,
        BOXING_MC_ATTK_CHARGE_01_ATTACK = 512072,
        BOXING_MC_ATTK_CHARGE_01_BTLIDLE = 512071,
        BOXING_MC_ATTK_CHARGE_01_START = 512070,
        BOXING_MC_ATTK_CHARGE_02 = 512073,
        BOXING_MC_ATTK_CHARGE_03 = 512074,
        BOXING_MC_ATTK_CHARGE_04 = 512075,
        BOXING_MC_ATTK_DOWN_01  = 512080,
        BOXING_MC_ATTK_DOWN_02  = 512081,
        BOXING_MC_ATTK_DOWN_03  = 512082,
        BOXING_MC_DASH_ATTK_01  = 512045,
        BOXING_MC_JUMP_ATTK_01  = 512035,
        BOXING_MC_JUMP_ATTK_02  = 512036,
        BOXING_MC_JUMP_ATTK_03  = 512037,
        BOXING_MC_SKILL_DFT_MAKE_TWO_01 = 512300,
        BOXING_MC_SKILL_DFT_POWER_FIST_01 = 512305,
        BOXING_MC_SKILL_DFT_REPID_STENSE_01_ATTACK = 512312,
        BOXING_MC_SKILL_DFT_REPID_STENSE_01_START = 512304,
        BOXING_MC_SKILL_DFT_SCREW_DOWN_01_ATTACK = 512310,
        BOXING_MC_SKILL_DFT_SCREW_DOWN_01_FINISH = 512311,
        BOXING_MC_SKILL_DFT_SCREW_DOWN_01_START = 512303,
        BOXING_MC_SKILL_DFT_TORNADO_SPIN_01 = 512301,
        BOXING_MC_SKILL_DFT_WEAVING_01 = 512302,
        BOXING_MC_SKILL_DRF_CROSS_FIST_CHARGE = 512615,
        BOXING_MC_SKILL_DRF_CROSS_FIST_DASH = 512616,
        BOXING_MC_SKILL_DRF_CROSS_FIST_FINISH = 512618,
        BOXING_MC_SKILL_DRF_CROSS_FIST_HIT = 512617,
        BOXING_MC_SKILL_DRF_CROSS_FIST_START = 512614,
        BOXING_MC_SKILL_DRF_RAPID_FIST_01_ATTACK = 512603,
        BOXING_MC_SKILL_DRF_RAPID_FIST_01_BTIDLE = 512604,
        BOXING_MC_SKILL_DRF_RAPID_FIST_01_FINISH = 512605,
        BOXING_MC_SKILL_DRF_RAPID_FIST_01_START = 512602,
        BOXING_MC_SKILL_DRF_TRAININGTIME_01_FIRE = 512601,
        BOXING_MC_SKILL_DRF_TRAININGTIME_01_START = 512600,
        BOXING_MC_SKILL_DRF_TWIN_ATTACK_01_FIRE = 512610,
        BOXING_MC_SKILL_DRF_TWIN_ATTACK_01_START = 512609,
        BOXING_MC_SKILL_MRG_BUNKER_BUSTER_01 = 512507,
        BOXING_MC_SKILL_MRG_BUNKER_BUSTER_01_FINISH = 512515,
        BOXING_MC_SKILL_MRG_COMBAT_REVIVE_01_ATTACK = 512521,
        BOXING_MC_SKILL_MRG_COMBAT_REVIVE_01_START = 512520,
        BOXING_MC_SKILL_MRG_COMBAT_REVIVE_02 = 1250520,
        BOXING_MC_SKILL_MRG_LIGHTING_MOVE_01_ATTACK = 512516,
        BOXING_MC_SKILL_MRG_LIGHTING_MOVE_01_START = 512510,
        BOXING_MC_SKILL_MRG_LION_SHOUT_01 = 512508,
        BOXING_MC_SKILL_MRG_TORNADO_UPER_01 = 512509,
        BOXING_MC_SKILL_TWN_DEADLY_SPOT_01_ATTACK = 512422,
        BOXING_MC_SKILL_TWN_DEADLY_SPOT_01_START = 512413,
        BOXING_MC_SKILL_TWN_DRAGON_UPER_01 = 512412,
        BOXING_MC_SKILL_TWN_FUSION_01 = 512409,
        BOXING_MC_SKILL_TWN_HYPER_NEEKICK_01 = 512411,
        BOXING_MC_SKILL_TWN_UNLIMIT_SPINKICK_01 = 512410,
        BOXING_SC_READY_TO_SKILL_01 = 512006,
        BOXING_SC_SET_POSITION_01 = 512007,
        BOXING_SC_SKILL_DFT_DRAGON_KICK_01 = 512307,
        BOXING_SC_SKILL_DFT_RIGHTING_KICK_01 = 512309,
        BOXING_SC_SKILL_DFT_ROKET_BLOW_01 = 512308,
        BOXING_SC_SKILL_DFT_SPIN_KICK_01 = 512306,
        BOXING_SC_SKILL_MRG_ROLLING_GRAPPLE_01 = 512502,
        BOXING_SC_SKILL_MRG_SLIDING_KICK_01 = 512503,
        BOXING_SC_SKILL_MRG_SMASH_KICK_01_ATTACK = 512518,
        BOXING_SC_SKILL_MRG_SMASH_KICK_01_FINISH = 512519,
        BOXING_SC_SKILL_MRG_SMASH_KICK_01_START = 512504,
        BOXING_SC_SKILL_MRG_SPIN_BOOM_01 = 512500,
        BOXING_SC_SKILL_MRG_SPIRIT_SPEAR_01 = 512501,
        BOXING_SC_SKILL_TWN_M_RIGHTING_KICK_01_ATTACK_L = 512406,
        BOXING_SC_SKILL_TWN_M_RIGHTING_KICK_01_FINISH = 512407,
        BOXING_SC_SKILL_TWN_M_RIGHTING_KICK_01_START = 512405,
        BOXING_SC_SKILL_TWN_RISING_DROPKICK_01 = 512408,
        BOXING_SC_SKILL_TWN_SPIN_BLADE_01_ATTACK_L = 512402,
        BOXING_SC_SKILL_TWN_SPIN_BLADE_01_FINISH = 512403,
        BOXING_SC_SKILL_TWN_SPIN_BLADE_01_START = 512401,
        BOXING_SC_SKILL_TWN_STAMPER_01 = 512400,
        BOXING_SC_SKILL_TWN_SWISH_KICK_01 = 512404,
        COM_BLOWUP_DOWN_01      = 1000023,
        COM_BLOWUP_LOOP_01      = 1000022,
        COM_BOOSTER_DOWN        = 1000108,
        COM_BOOSTER_UP          = 1000107,
        COM_BREAKFALL_01        = 1000015,
        COM_BREAKFALL_02        = 1000016,
        COM_BREAKFALL_03        = 1000017,
        COM_BREAKFALL_04        = 1000018,
        COM_BTLIDLE_01          = 511005,
        COM_CARRY_DASH_01       = 1160040,
        COM_CARRY_IDLE_01       = 1160000,
        COM_CARRY_JUMP_01       = 1160030,
        COM_CARRY_JUMP_02       = 1160032,
        COM_CARRY_JUMP_03       = 1160033,
        COM_CARRY_JUMP_IDLE_01  = 1160031,
        COM_CARRY_RUN_01        = 1160055,
        COM_CARRY_SPEEDFAD_01   = 1160045,
        COM_CLASSPROMOTION_01   = 1000005,
        COM_DASH_01             = 1250040,
        COM_DIE_01              = 1250220,
        COM_DIE_IDLE_01         = 1250221,
        COM_DMG_01              = 1000011,
        COM_DMG_02              = 1000012,
        COM_DMG_KNOCKDOWN_01    = 1000026,
        COM_DMG_KNOCKDOWN_IDLE_01 = 1000027,
        COM_DMG_KNOCKDOWN_UP_01 = 1000028,
        COM_IDLE_01             = 1251200,
        COM_IDLE_02             = 1251201,
        COM_IDLE_03             = 1251202,
        COM_INTRO_ACTION_BXG    = 512001,
        COM_INTRO_ACTION_HAND   = 511001,
        COM_INTRO_CANCLE_01     = 1300014,
        COM_INTRO_CANCLE_02     = 1300024,
        COM_INTRO_CANCLE_03     = 1300034,
        COM_INTRO_CANCLE_04     = 1300044,
        COM_INTRO_CANCLE_05     = 1300054,
        COM_INTRO_CANCLE_06     = 1300064,
        COM_INTRO_CANCLE_07     = 1300074,
        COM_INTRO_CANCLE_08     = 1300084,
        COM_INTRO_CANCLE_BXG    = 512003,
        COM_INTRO_CANCLE_HAND   = 511003,
        COM_INTRO_IDLE_01_01    = 1300011,
        COM_INTRO_IDLE_01_02    = 1300012,
        COM_INTRO_IDLE_02_01    = 1300021,
        COM_INTRO_IDLE_02_02    = 1300022,
        COM_INTRO_IDLE_03_01    = 1300031,
        COM_INTRO_IDLE_03_02    = 1300032,
        COM_INTRO_IDLE_04_01    = 1300041,
        COM_INTRO_IDLE_04_02    = 1300042,
        COM_INTRO_IDLE_05_01    = 1300051,
        COM_INTRO_IDLE_05_02    = 1300052,
        COM_INTRO_IDLE_06_01    = 1300061,
        COM_INTRO_IDLE_06_02    = 1300062,
        COM_INTRO_IDLE_07_01    = 1300071,
        COM_INTRO_IDLE_07_02    = 1300072,
        COM_INTRO_IDLE_08_01    = 1300081,
        COM_INTRO_IDLE_08_02    = 1300082,
        COM_INTRO_IDLE_BXG      = 512002,
        COM_INTRO_IDLE_HAND     = 511002,
        COM_INTRO_SELECT_01     = 1300013,
        COM_INTRO_SELECT_02     = 1300023,
        COM_INTRO_SELECT_03     = 1300033,
        COM_INTRO_SELECT_04     = 1300043,
        COM_INTRO_SELECT_05     = 1300053,
        COM_INTRO_SELECT_06     = 1300063,
        COM_INTRO_SELECT_07     = 1300073,
        COM_INTRO_SELECT_08     = 1300083,
        COM_JOB_FISHING_FISHING_01 = 1110411,
        COM_JOB_FISHING_FISHING_02 = 1110412,
        COM_JOB_FISHING_FISHING_03 = 1110413,
        COM_JOB_FISHING_NET_01  = 1110421,
        COM_JOB_FISHING_NET_02  = 1110422,
        COM_JOB_FISHING_NET_03  = 1110423,
        COM_JOB_GATHER_HOE_01   = 1110311,
        COM_JOB_GATHER_HOE_02   = 1110312,
        COM_JOB_GATHER_HOE_03   = 1110313,
        COM_JOB_GATHER_WEEDER_01 = 1110321,
        COM_JOB_GATHER_WEEDER_02 = 1110322,
        COM_JOB_GATHER_WEEDER_03 = 1110323,
        COM_JOB_LUMBER_AXE_01   = 1110111,
        COM_JOB_LUMBER_AXE_02   = 1110112,
        COM_JOB_LUMBER_AXE_03   = 1110113,
        COM_JOB_LUMBER_CHAINSAW_01 = 1110121,
        COM_JOB_LUMBER_CHAINSAW_02 = 1110122,
        COM_JOB_LUMBER_CHAINSAW_03 = 1110123,
        COM_JOB_MINING_DRILL_01 = 1110221,
        COM_JOB_MINING_DRILL_02 = 1110222,
        COM_JOB_MINING_DRILL_03 = 1110223,
        COM_JOB_MINING_PICK_01  = 1110211,
        COM_JOB_MINING_PICK_02  = 1110212,
        COM_JOB_MINING_PICK_03  = 1110213,
        COM_JOB_SUCCESS         = 1110001,
        COM_JUMP_01             = 1250030,
        COM_JUMP_02             = 1250031,
        COM_JUMP_03             = 1250033,
        COM_JUMP_DASH_01        = 1250043,
        COM_JUMP_IDLE_01        = 1250032,
        COM_LADDER_DOWN_01      = 1000103,
        COM_LADDER_IDLE_01      = 1000102,
        COM_LADDER_UP_01        = 1000101,
        COM_LIE_DOWN_01         = 1000030,
        COM_LIE_IDLE_01         = 1000031,
        COM_LIE_UP_01           = 1000032,
        COM_LV_UP_01            = 1000001,
        COM_PORTAL_IN_01        = 1000105,
        COM_PORTAL_OUT_01       = 1000106,
        COM_REST_01             = 1100011,
        COM_REST_13_01          = 1100131,
        COM_REST_13_02          = 1100132,
        COM_REST_14_01          = 1100141,
        COM_REST_14_02          = 1100142,
        COM_REST_15_01          = 1100151,
        COM_REST_15_02          = 1100152,
        COM_RESURRECTION_01     = 1250229,
        COM_RIDE_01             = 1000100,
        COM_RUN_01              = 1250255,
        COM_SIT_DOWN_01         = 1002001,
        COM_SIT_IDLE_01         = 1002002,
        COM_SIT_UP_01           = 1002003,
        COM_SKILL_AWAKE_CHARGE_01_01 = 1146001,
        COM_SKILL_AWAKE_CHARGE_01_02 = 1146002,
        COM_SKILL_DARK_WHEELWIND_01 = 1130001,
        COM_SKILL_DOMINATION_TRANS_01_SPELL = 1140005,
        COM_SKILL_DOMINATION_TRANS_01_START = 1140004,
        COM_SKILL_SHOT_01       = 1140003,
        COM_SKILL_SPELL_01      = 1140002,
        COM_SKILL_STEPTHEBIT_01 = 1140001,
        COM_STANDUP_01          = 1000024,
        COM_STUN_01             = 1000025,
        COM_WALK_01             = 1250250,
        COM_WORK_COOK_FAIL      = 1150103,
        COM_WORK_COOK_START     = 1150100,
        COM_WORK_COOK_SUCCESS   = 1150102,
        COM_WORK_COOK_WORK      = 1150101,
        COM_WORK_SPELL_01_FAIL  = 1150303,
        COM_WORK_SPELL_01_START = 1150300,
        COM_WORK_SPELL_01_SUCCESS = 1150302,
        COM_WORK_SPELL_01_WORK  = 1150301,
        COM_WORK_WORKMANSHIP_FAIL = 1150203,
        COM_WORK_WORKMANSHIP_START = 1150200,
        COM_WORK_WORKMANSHIP_SUCCESS = 1150202,
        COM_WORK_WORKMANSHIP_WORK = 1150201,
        EMOTION_COUPLE_BALLOON_01 = 2200201,
        EMOTION_COUPLE_CARRY_01_01 = 2200202,
        EMOTION_COUPLE_CARRY_01_02 = 2200203,
        EMOTION_COUPLE_CARRY_01_03 = 2200204,
        EMOTION_COUPLE_DANCE_01 = 2200205,
        EMOTION_COUPLE_DANCE_02 = 2200206,
        EMOTION_COUPLE_HOLDHANDS_01_01 = 2200207,
        EMOTION_COUPLE_HOLDHANDS_01_02 = 2200208,
        EMOTION_COUPLE_HOLDHANDS_01_03 = 2200209,
        EMOTION_COUPLE_KISS_01  = 2200210,
        EMOTION_COUPLE_WEDDING_01 = 2200211,
        EMOTION_SINGLE_BEG_01   = 2100001,
        EMOTION_SINGLE_BEG_02   = 2100002,
        EMOTION_SINGLE_BEG_03   = 2100003,
        EMOTION_SINGLE_BOW_01   = 2100207,
        EMOTION_SINGLE_CRY_01   = 2100208,
        EMOTION_SINGLE_DANCE_01 = 2100209,
        EMOTION_SINGLE_DANCE_02 = 2100210,
        EMOTION_SINGLE_DANCE_03 = 2100039,
        EMOTION_SINGLE_FLOP_01  = 2100011,
        EMOTION_SINGLE_FLOP_02  = 2100012,
        EMOTION_SINGLE_FLOP_03  = 2100013,
        EMOTION_SINGLE_HANDCLAP_01 = 2100214,
        EMOTION_SINGLE_HI_01    = 2100016,
        EMOTION_SINGLE_HONOR_01 = 2100017,
        EMOTION_SINGLE_HONOR_02 = 2100018,
        EMOTION_SINGLE_HONOR_03 = 2100019,
        EMOTION_SINGLE_LAUGH_01 = 2100220,
        EMOTION_SINGLE_LOVE_01  = 2100221,
        EMOTION_SINGLE_OTL_01   = 2100022,
        EMOTION_SINGLE_OTL_02   = 2100023,
        EMOTION_SINGLE_OTL_03   = 2100024,
        EMOTION_SINGLE_PRIDE_01 = 2100025,
        EMOTION_SINGLE_PROVOC_01 = 2100026,
        EMOTION_SINGLE_PROVOC_02 = 2100027,
        EMOTION_SINGLE_PROVOC_03 = 2100028,
        EMOTION_SINGLE_ROWDY_01 = 2100029,
        EMOTION_SINGLE_ROWDY_02 = 2100030,
        EMOTION_SINGLE_ROWDY_03 = 2100031,
        EMOTION_SINGLE_RUSH_01  = 2100032,
        EMOTION_SINGLE_SLEEP_01 = 2100037,
        EMOTION_SINGLE_SORRY_01 = 2100033,
        EMOTION_SINGLE_SURPRISE_01 = 2100034,
        EMOTION_SINGLE_VICTORY_01 = 2100035,
        EMOTION_SINGLE_VOTE_01  = 2100036,
        PG_COMBO_A_2_ATTACK     = 9512125,
        PG_COMBO_A_2_FINISH     = 9512129,
        PG_COMBO_A_3_ATTACK     = 9512132,
        PG_COMBO_A_3_ATTACK_F   = 9512133,
        PG_COMBO_A_3_FINISH     = 9512136,
        PG_COMBO_A_3_FINISH_F   = 9512137,
        PG_COMBO_A_4_ATTACK     = 9512145,
        PG_COMBO_A_4_FINISH     = 9512149,
        PG_COMBO_A_5_ATTACK     = 9512152,
        PG_COMBO_A_5_ATTACK_F   = 9512153,
        PG_COMBO_A_5_FINISH     = 9512156,
        PG_COMBO_A_5_FINISH_F   = 9512157,
        PG_COMBO_A_ATTACK       = 9512115,
        PG_COMBO_A_FINISH       = 9512119,
        PG_COMBO_B_3_ATTACK     = 9512235,
        PG_COMBO_B_3_FINISH     = 9512239,
        PG_COMBO_B_4_ATTACK     = 9512245,
        PG_COMBO_B_4_FINISH     = 9512249,
        PG_COMBO_B_5_ATTACK     = 9512252,
        PG_COMBO_B_5_ATTACK_F   = 9512253,
        PG_COMBO_B_5_FINISH     = 9512256,
        PG_COMBO_B_5_FINISH_F   = 9512257,
        PG_COMBO_B_ATTACK       = 9512215,
        PG_COMBO_B_FINISH       = 9512219,
        PG_COMBO_C_4_ATTACK     = 9512345,
        PG_COMBO_C_4_FINISH     = 9512349,
        PG_COMBO_C_5_ATTACK     = 9512352,
        PG_COMBO_C_5_ATTCK_F    = 9512353,
        PG_COMBO_C_5_FINISH     = 9512356,
        PG_COMBO_C_5_FINISH_F   = 9512357,
        PG_COMBO_C_ATTACK       = 9512312,
        PG_COMBO_C_ATTACK_F     = 9512313,
        PG_COMBO_C_FINISH       = 9512316,
        PG_COMBO_C_FINISH_F     = 9512317,
        PG_COMBO_D_5_ATTACK     = 9512455,
        PG_COMBO_D_5_ATTACK_F   = 9512453,
        PG_COMBO_D_5_FINISH     = 9512459,
        PG_COMBO_D_5_FINISH_F   = 9512457,
        PG_COMBO_D_ATTACK       = 9512415,
        PG_COMBO_D_ATTACK_F     = 9512413,
        PG_COMBO_D_FINISH       = 9512419,
        PG_COMBO_D_FINISH_F     = 9512417,
        PG_COMBO_E_ATTACK       = 9512512,
        PG_COMBO_E_ATTACK_F     = 9512513,
        PG_COMBO_E_FINISH       = 9512516,
        PG_COMBO_E_FINISH_F     = 9512517
    };
}

#endif  // #ifndef 00_BASE_ANIM_H__
