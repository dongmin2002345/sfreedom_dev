// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef 08_WEAPON_L_ANIM_H__
#define 08_WEAPON_L_ANIM_H__

namespace 08_Weapon_l_Anim
{
    enum
    {
        CL_COMBO_A_2_ATTK       = 9032125,
        CL_COMBO_A_2_FINISH     = 9032129,
        CL_COMBO_A_3_ATTK       = 9032135,
        CL_COMBO_A_3_FINISH     = 9032139,
        CL_COMBO_A_4_ATTK       = 9032145,
        CL_COMBO_A_4_FINISH     = 9032149,
        CL_COMBO_A_5_6_ATTK     = 9032655,
        CL_COMBO_A_5_6_FINISH   = 9032659,
        CL_COMBO_A_5_ATTK       = 9032155,
        CL_COMBO_A_5_FINISH     = 9032159,
        CL_COMBO_A_6_ATTK       = 9032165,
        CL_COMBO_A_6_FINISH     = 9032169,
        CL_COMBO_A_7_ATTK       = 9032175,
        CL_COMBO_A_7_FINISH     = 9032179,
        CL_COMBO_A_ATTK         = 9032115,
        CL_COMBO_A_FINISH       = 9032119,
        CL_COMBO_B_3_ATTK       = 9032235,
        CL_COMBO_B_3_FINISH     = 9032239,
        CL_COMBO_B_4_ATTK       = 9032245,
        CL_COMBO_B_4_FINISH     = 9032249,
        CL_COMBO_B_5_ATTK       = 9032255,
        CL_COMBO_B_5_FINISH     = 9032259,
        CL_COMBO_B_6_ATTK       = 9032265,
        CL_COMBO_B_6_FINISH     = 9032269,
        CL_COMBO_B_7_ATTK       = 9032275,
        CL_COMBO_B_7_FINISH     = 9032279,
        CL_COMBO_B_ATTK         = 9032215,
        CL_COMBO_B_FINISH       = 9032219,
        CL_COMBO_C_4_ATTK       = 9032345,
        CL_COMBO_C_4_FINISH     = 9032349,
        CL_COMBO_C_5_ATTK       = 9032355,
        CL_COMBO_C_5_FINISH     = 9032359,
        CL_COMBO_C_6_ATTK       = 9032365,
        CL_COMBO_C_6_FINISH     = 9032369,
        CL_COMBO_C_ATTK         = 9032315,
        CL_COMBO_C_FINISH       = 9032319,
        CL_COMBO_D_5_ATTK       = 9032455,
        CL_COMBO_D_5_FINISH     = 9032459,
        CL_COMBO_D_6_ATTK       = 9032465,
        CL_COMBO_D_6_FINISH     = 9032469,
        CL_COMBO_D_ATTK         = 9032415,
        CL_COMBO_D_FINISH       = 9032419,
        CL_COMBO_E_ATTK         = 9032515,
        CL_COMBO_E_FINISH       = 9032519,
        CLAW_ATTK_01            = 32021,
        CLAW_ATTK_02            = 32022,
        CLAW_ATTK_03            = 32023,
        CLAW_ATTK_BACK_01       = 32044,
        CLAW_ATTK_CHARGE_01_01  = 32031,
        CLAW_ATTK_CHARGE_01_02  = 32032,
        CLAW_ATTK_CHARGE_01_03  = 32033,
        CLAW_ATTK_DOWN_01       = 32034,
        CLAW_ATTK_DOWN_02       = 32060,
        CLAW_ATTK_DOWN_03       = 32061,
        CLAW_ATTK_DOWN_FLY_01   = 32062,
        CLAW_BTLIDLE_01         = 32011,
        CLAW_DASH_ATTK_01       = 32037,
        CLAW_DMG_01             = 32051,
        CLAW_DMG_02             = 32052,
        CLAW_DMG_03             = 32053,
        CLAW_IDLE_WEAPON_01     = 32004,
        CLAW_IDLE_WEAPON_02     = 32005,
        CLAW_IDLE_WEAPON_03     = 32006,
        CLAW_JUMP_ATTK_01       = 32038,
        CLAW_JUMP_DOWN_ATTK_01  = 32039,
        CLAW_JUMP_DOWN_ATTK_02  = 32040,
        CLAW_RUN_WEAPON_01      = 32206,
        CLAW_SKILL_CLOWFISHING_01_01 = 1000160,
        CLAW_SKILL_CLOWFISHING_01_02 = 1000161,
        CLAW_SKILL_CLOWFISHING_01_03 = 1000162,
        CLAW_SKILL_CLUBMANIA_01 = 1000163,
        CLAW_SKILL_CLUBMANIA_02 = 1000169,
        CLAW_SKILL_HEADSPIN_01  = 32101,
        CLAW_SKILL_HEADSPIN_01_FINISH = 32102,
        CLAW_SKILL_HELLSHOUTING_01 = 1000164,
        CLAW_SKILL_MARIONETTE_01 = 1000165,
        CLAW_SKILL_MARIONETTE_02 = 1000166,
        CLAW_SKILL_MARIONETTE_03 = 1000167,
        CLAW_SKILL_MARIONETTE_04 = 1000168,
        CLAW_SKILL_RAID_01      = 1000147,
        CLAW_SKILL_ROCKET_PUNCH_01 = 1000146,
        CLAW_WALK_WEAPON_01     = 32203,
        COM_BREAKFALL_01        = 1000256,
        COM_DASH_01             = 1000211,
        COM_DMG_KNOCKDOWN_01    = 1000054,
        COM_DMG_KNOCKDOWN_IDLE_01 = 1000055,
        COM_DMG_KNOCKDOWN_UP_01 = 1000056,
        COM_IDLE_01             = 1000000,
        COM_IDLE_02             = 1000001,
        COM_IDLE_03             = 1000002,
        COM_INTRO_ACTION_CLW    = 32001,
        COM_INTRO_ACTION_KAT    = 33001,
        COM_INTRO_CANCLE_CLW    = 32003,
        COM_INTRO_CANCLE_KAT    = 33003,
        COM_INTRO_IDLE_CLW      = 32002,
        COM_INTRO_IDLE_KAT      = 33002,
        COM_JUMP_01             = 1000251,
        COM_JUMP_02             = 1000252,
        COM_JUMP_03             = 1000254,
        COM_JUMP_DASH_01        = 1000255,
        COM_JUMP_IDLE_01        = 1000253,
        COM_LIE_DOWN_01         = 1000310,
        COM_LIE_IDLE_01         = 1000311,
        COM_LIE_UP_01           = 1000312,
        COM_LV_UP_01            = 1000321,
        COM_PORTAL_IN_01        = 1100201,
        COM_PORTAL_OUT_01       = 1100202,
        COM_RUN_01              = 1000204,
        COM_SIT_DOWN_01         = 1000301,
        COM_SIT_IDLE_01         = 1000302,
        COM_SIT_UP_01           = 1000303,
        COM_SKILL_AWAKE_CHARGE_01_01 = 1030501,
        COM_SKILL_AWAKE_CHARGE_01_02 = 1030502,
        COM_SKILL_BUFF_01       = 1000112,
        COM_SKILL_BUFF_02_01    = 1000220,
        COM_SKILL_BUFF_02_02    = 1000221,
        COM_SKILL_CHAINCRASH_01 = 1000123,
        COM_SKILL_DARK_WHEELWIND_01 = 1000231,
        COM_SKILL_DEBUFF_01     = 1000113,
        COM_SKILL_DEBUFF_02_01  = 1000222,
        COM_SKILL_DEBUFF_02_02  = 1000223,
        COM_SKILL_DOUBLECRASH_01 = 1000122,
        COM_SKILL_FIRE_01       = 1000127,
        COM_SKILL_GROUND_01     = 1000128,
        COM_SKILL_HAPHAZARD_01  = 1000125,
        COM_SKILL_SHADOWLEAP_01_01 = 1000129,
        COM_SKILL_SHADOWLEAP_01_02 = 1000130,
        COM_SKILL_SHOUT_01      = 1000131,
        COM_SKILL_SONICBOOM_01  = 1000121,
        COM_SKILL_STEPTHEBIT_01 = 1000232,
        COM_SKILL_STRIPWEAPON_01 = 1000120,
        COM_SKILL_VENOM_01      = 1000114,
        COM_SKILL_WINDSURIKEN_01 = 1000132,
        COM_WALK_01             = 1000201,
        CU_COMBO_A_2_ATTK       = 9033125,
        CU_COMBO_A_2_FINISH     = 9033129,
        CU_COMBO_A_3_ATTK       = 9033135,
        CU_COMBO_A_3_FINISH     = 9033139,
        CU_COMBO_A_4_ATTK       = 9033145,
        CU_COMBO_A_4_FINISH     = 9033149,
        CU_COMBO_A_5_6_ATTK     = 9033655,
        CU_COMBO_A_5_6_FINISH   = 9033659,
        CU_COMBO_A_5_ATTK       = 9033155,
        CU_COMBO_A_5_FINISH     = 9033159,
        CU_COMBO_A_6_ATTK       = 9033165,
        CU_COMBO_A_6_FINISH     = 9033169,
        CU_COMBO_A_7_ATTK       = 9033175,
        CU_COMBO_A_7_FINISH     = 9033179,
        CU_COMBO_A_ATTK         = 9033115,
        CU_COMBO_A_FINISH       = 9033119,
        CU_COMBO_B_3_ATTK       = 9033235,
        CU_COMBO_B_3_FINISH     = 9033239,
        CU_COMBO_B_4_ATTK       = 9033245,
        CU_COMBO_B_4_FINISH     = 9033249,
        CU_COMBO_B_5_ATTK       = 9033255,
        CU_COMBO_B_5_FINISH     = 9033259,
        CU_COMBO_B_6_ATTK       = 9033265,
        CU_COMBO_B_6_FINISH     = 9033269,
        CU_COMBO_B_7_ATTK       = 9033275,
        CU_COMBO_B_7_FINISH     = 9033279,
        CU_COMBO_B_ATTK         = 9033215,
        CU_COMBO_B_FINISH       = 9033219,
        CU_COMBO_C_4_ATTK       = 9033345,
        CU_COMBO_C_4_FINISH     = 9033349,
        CU_COMBO_C_5_ATTK       = 9033355,
        CU_COMBO_C_5_FINISH     = 9033359,
        CU_COMBO_C_6_ATTK       = 9033365,
        CU_COMBO_C_6_FINISH     = 9033369,
        CU_COMBO_C_ATTK         = 9033315,
        CU_COMBO_C_FINISH       = 9033319,
        CU_COMBO_D_5_ATTK       = 9033455,
        CU_COMBO_D_5_FINISH     = 9033459,
        CU_COMBO_D_6_ATTK       = 9033465,
        CU_COMBO_D_6_FINISH     = 9033469,
        CU_COMBO_D_ATTK         = 9033415,
        CU_COMBO_D_FINISH       = 9033419,
        CU_COMBO_E_ATTK         = 9033515,
        CU_COMBO_E_FINISH       = 1,
        HAND_ATTK_01            = 31021,
        HAND_ATTK_02            = 31022,
        HAND_ATTK_03            = 31023,
        HAND_BTLIDLE_01         = 31011,
        HAND_DMG_01             = 31051,
        HAND_DMG_02             = 31052,
        HAND_DMG_03             = 31053,
        KATTAR_ATTK_01          = 33021,
        KATTAR_ATTK_02          = 33022,
        KATTAR_ATTK_03          = 33023,
        KATTAR_ATTK_CHARGE_01_01 = 33031,
        KATTAR_ATTK_CHARGE_01_02 = 33032,
        KATTAR_ATTK_CHARGE_01_03 = 33033,
        KATTAR_ATTK_DOWN_01     = 33034,
        KATTAR_ATTK_DOWN_02     = 33060,
        KATTAR_ATTK_DOWN_03     = 33061,
        KATTAR_ATTK_DOWN_FLY_01 = 33062,
        KATTAR_ATTK_FLY_01      = 33035,
        KATTAR_ATTK_PUSH_01     = 33036,
        KATTAR_BTLIDLE_01       = 33011,
        KATTAR_DASH_ATTK_01     = 33037,
        KATTAR_DMG_01           = 33051,
        KATTAR_DMG_02           = 33052,
        KATTAR_DMG_03           = 33053,
        KATTAR_IDLE_WEAPON_01   = 33004,
        KATTAR_IDLE_WEAPON_02   = 33005,
        KATTAR_IDLE_WEAPON_03   = 33006,
        KATTAR_JUMP_ATTK_01     = 33038,
        KATTAR_JUMP_DOWN_ATTK_01 = 33039,
        KATTAR_JUMP_DOWN_ATTK_02 = 33040,
        KATTAR_RUN_WEAPON_01    = 33206,
        KATTAR_SKILL_CROSSBLADE_01 = 1000159,
        KATTAR_SKILL_PHANTOMWARRIOR_01 = 1000152,
        KATTAR_SKILL_PROTECTEDGE_01_01 = 1000157,
        KATTAR_SKILL_PROTECTEDGE_01_02 = 1000158,
        KATTAR_WALK_WEAPON_01   = 33203
    };
}

#endif  // #ifndef 08_WEAPON_L_ANIM_H__
