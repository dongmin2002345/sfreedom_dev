// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRAGON_LORD_ANIM_H__
#define DRAGON_LORD_ANIM_H__

namespace Dragon_Lord_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        SELECT_01               = 1000002,
        SELECT_02               = 1000003,
        SELECT_BACK_01          = 1000006,
        SELECT_BACK_02          = 1000007,
        SELECT_IDLE_01          = 1000004,
        SELECT_IDLE_02          = 1000005
    };
}

#endif  // #ifndef DRAGON_LORD_ANIM_H__
