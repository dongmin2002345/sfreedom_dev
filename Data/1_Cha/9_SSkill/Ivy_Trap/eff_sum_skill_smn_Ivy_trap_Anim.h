// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EFF_SUM_SKILL_SMN_IVY_TRAP_ANIM_H__
#define EFF_SUM_SKILL_SMN_IVY_TRAP_ANIM_H__

namespace eff_sum_skill_smn_Ivy_trap_Anim
{
    enum
    {
        LOOP                    = 1000000
    };
}

#endif  // #ifndef EFF_SUM_SKILL_SMN_IVY_TRAP_ANIM_H__
