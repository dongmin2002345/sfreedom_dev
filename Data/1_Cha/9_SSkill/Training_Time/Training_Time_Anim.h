// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef TRAINING_TIME_ANIM_H__
#define TRAINING_TIME_ANIM_H__

namespace Training_Time_Anim
{
    enum
    {
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef TRAINING_TIME_ANIM_H__
