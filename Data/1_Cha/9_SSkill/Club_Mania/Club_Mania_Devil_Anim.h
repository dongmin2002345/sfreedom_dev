// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CLUB_MANIA_DEVIL_ANIM_H__
#define CLUB_MANIA_DEVIL_ANIM_H__

namespace Club_Mania_Devil_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef CLUB_MANIA_DEVIL_ANIM_H__
