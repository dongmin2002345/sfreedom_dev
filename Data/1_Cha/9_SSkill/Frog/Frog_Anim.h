// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FROG_ANIM_H__
#define FROG_ANIM_H__

namespace Frog_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        RUN_01                  = 1000204
    };
}

#endif  // #ifndef FROG_ANIM_H__
