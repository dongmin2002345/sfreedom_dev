// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef N_CHOCO_ANIM_H__
#define N_CHOCO_ANIM_H__

namespace N_Choco_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        JUMP_01                 = 1000071,
        JUMP_02                 = 1000072,
        JUMP_03                 = 1000074,
        JUMP_IDLE               = 1000073,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef N_CHOCO_ANIM_H__
