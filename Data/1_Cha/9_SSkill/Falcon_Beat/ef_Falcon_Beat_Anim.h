// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_FALCON_BEAT_ANIM_H__
#define EF_FALCON_BEAT_ANIM_H__

namespace ef_Falcon_Beat_Anim
{
    enum
    {
        ATTK_01                 = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef EF_FALCON_BEAT_ANIM_H__
