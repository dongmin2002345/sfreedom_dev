// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MULTI_STRIPE_ANIM_H__
#define MULTI_STRIPE_ANIM_H__

namespace Multi_Stripe_Anim
{
    enum
    {
        SKILL_01                = 1000000,
        SKILL_02                = 1000001
    };
}

#endif  // #ifndef MULTI_STRIPE_ANIM_H__
