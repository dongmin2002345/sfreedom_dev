// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef LOVEMODE_ANIM_H__
#define LOVEMODE_ANIM_H__

namespace LoveMode_Anim
{
    enum
    {
        DASH_01                 = 1160040,
        IDLE_01                 = 1160000,
        JUMP_01                 = 1160030,
        JUMP_02                 = 1160032,
        JUMP_03                 = 1160033,
        JUMP_IDLE               = 1160031,
        RUN_01                  = 1160055,
        SPEEDFAD_01             = 1160045
    };
}

#endif  // #ifndef LOVEMODE_ANIM_H__
