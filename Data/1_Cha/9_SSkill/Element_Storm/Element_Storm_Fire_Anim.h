// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELEMENT_STORM_FIRE_ANIM_H__
#define ELEMENT_STORM_FIRE_ANIM_H__

namespace Element_Storm_Fire_Anim
{
    enum
    {
        FIRE_SKILL_01_ATTACK    = 1000001,
        FIRE_SKILL_01_START     = 1000000
    };
}

#endif  // #ifndef ELEMENT_STORM_FIRE_ANIM_H__
