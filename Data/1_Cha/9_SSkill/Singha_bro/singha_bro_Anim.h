// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SINGHA_BRO_ANIM_H__
#define SINGHA_BRO_ANIM_H__

namespace singha_bro_Anim
{
    enum
    {
        SKILL_02                = 1000003,
        SKILL_03                = 1000004,
        SKILL_04                = 1000005,
        SKILL_BROTHER_IS_RETURN_01_01 = 1000000,
        SKILL_BROTHER_IS_RETURN_01_02 = 1000001,
        SKILL_BROTHER_IS_RETURN_01_03 = 1000002
    };
}

#endif  // #ifndef SINGHA_BRO_ANIM_H__
