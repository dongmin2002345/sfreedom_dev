// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SELF_BOMBER_ANIM_H__
#define SELF_BOMBER_ANIM_H__

namespace Self_Bomber_Anim
{
    enum
    {
        SKILL_01_01             = 1000000,
        SKILL_01_02             = 1000001
    };
}

#endif  // #ifndef SELF_BOMBER_ANIM_H__
