// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_MANABURN_ANIM_H__
#define EF_MANABURN_ANIM_H__

namespace ef_Manaburn_Anim
{
    enum
    {
        BOOM_01                 = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef EF_MANABURN_ANIM_H__
