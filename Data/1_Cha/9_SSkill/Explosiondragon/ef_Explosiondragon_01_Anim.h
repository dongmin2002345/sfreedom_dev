// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_EXPLOSIONDRAGON_01_ANIM_H__
#define EF_EXPLOSIONDRAGON_01_ANIM_H__

namespace ef_Explosiondragon_01_Anim
{
    enum
    {
        BOOM_01_01              = 1000000,
        BOOM_01_02              = 1000001,
        BOOM_01_03              = 1000002
    };
}

#endif  // #ifndef EF_EXPLOSIONDRAGON_01_ANIM_H__
