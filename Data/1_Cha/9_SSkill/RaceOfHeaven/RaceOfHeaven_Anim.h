// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RACEOFHEAVEN_ANIM_H__
#define RACEOFHEAVEN_ANIM_H__

namespace RaceOfHeaven_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef RACEOFHEAVEN_ANIM_H__
