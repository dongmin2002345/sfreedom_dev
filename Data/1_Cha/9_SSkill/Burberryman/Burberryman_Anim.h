// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BURBERRYMAN_ANIM_H__
#define BURBERRYMAN_ANIM_H__

namespace Burberryman_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef BURBERRYMAN_ANIM_H__
