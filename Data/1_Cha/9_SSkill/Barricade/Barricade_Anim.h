// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BARRICADE_ANIM_H__
#define BARRICADE_ANIM_H__

namespace Barricade_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef BARRICADE_ANIM_H__
