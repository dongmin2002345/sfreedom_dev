// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHAOS_BOMBER_ANIM_H__
#define CHAOS_BOMBER_ANIM_H__

namespace Chaos_Bomber_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        IDLE_01                 = 1000000,
        RUN_01                  = 1000055,
        SKILL_01                = 1000201
    };
}

#endif  // #ifndef CHAOS_BOMBER_ANIM_H__
