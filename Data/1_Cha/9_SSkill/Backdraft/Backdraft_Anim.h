// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BACKDRAFT_ANIM_H__
#define BACKDRAFT_ANIM_H__

namespace Backdraft_Anim
{
    enum
    {
        ATTK_01                 = 1000060,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef BACKDRAFT_ANIM_H__
