// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef OCTOPUS_INK_ANIM_H__
#define OCTOPUS_INK_ANIM_H__

namespace Octopus_Ink_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef OCTOPUS_INK_ANIM_H__
