// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VALCAN_300_ANIM_H__
#define VALCAN_300_ANIM_H__

namespace Valcan_300_Anim
{
    enum
    {
        ATTCK_01                = 1000061,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef VALCAN_300_ANIM_H__
