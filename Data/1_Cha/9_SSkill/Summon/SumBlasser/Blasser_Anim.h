// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BLASSER_ANIM_H__
#define BLASSER_ANIM_H__

namespace Blasser_Anim
{
    enum
    {
        BLOWUP_DOWN_01          = 1000023,
        BLOWUP_LOOP_01          = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RE_GAIN_01              = 1000110,
        RUN_01                  = 1000055,
        SKILL_01                = 1000201,
        SKILL_02                = 1000202,
        SKILL_03                = 1000203,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef BLASSER_ANIM_H__
