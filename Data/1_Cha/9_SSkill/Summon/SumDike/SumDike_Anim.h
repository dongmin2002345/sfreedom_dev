// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUMDIKE_ANIM_H__
#define SUMDIKE_ANIM_H__

namespace SumDike_Anim
{
    enum
    {
        ATTK_01                 = 1000062,
        ATTK_01_START           = 1000061,
        ATTK_02                 = 1000064,
        ATTK_02_START           = 1000063,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DASH                    = 1000202,
        DASH_BTLIDLE            = 1000204,
        DASH_FINISH             = 1000203,
        DASH_START              = 1000201,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RE_GAIN_01              = 1000110,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef SUMDIKE_ANIM_H__
