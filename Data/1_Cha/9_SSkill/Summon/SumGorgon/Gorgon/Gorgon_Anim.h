// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GORGON_ANIM_H__
#define GORGON_ANIM_H__

namespace Gorgon_Anim
{
    enum
    {
        ATTK_01                 = 1000063,
        ATTK_01_BLTIDLE         = 1000062,
        ATTK_01_START           = 1000061,
        ATTK_02                 = 1000066,
        ATTK_02_BTLIDLE         = 1000065,
        ATTK_02-START           = 1000064,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RE_GAIN_01              = 1000110,
        ROCK_PUNCH_01           = 1000203,
        ROCK_PUNCH_01_BTLIDLE   = 1000202,
        ROCK_PUNCH_01_BTLIDLE_START = 1000201,
        ROLLING_ATTACK_01       = 1000206,
        ROLLING_ATTACK_01_BTLIDLE_START = 1000204,
        ROLLING_ATTACK_01_DASH  = 1000205,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef GORGON_ANIM_H__
