// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUMKALKY_ANIM_H__
#define SUMKALKY_ANIM_H__

namespace SumKalky_Anim
{
    enum
    {
        ATTACK_01               = 1000062,
        ATTACK_01_START         = 1000061,
        ATTACK_BTLIDLE_01       = 1000060,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTIDLE_01               = 1000005,
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        OPENING_01              = 1000081,
        RE_GAIN_01              = 1000110,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_ATTACK_BTLIDLE_01 = 1000200,
        SKILL_01_ATTACK_RUSH    = 1000202,
        SKILL_01_ATTACK_START   = 1000201,
        SKILL_02_ATTACK         = 1000328,
        SKILL_02_ATTACK_BTLIDLE_01 = 1000326,
        SKILL_02_START          = 1000325,
        SKILL_03_ATTACK         = 1000380,
        SKILL_03_ATTACK_BTLIDLE_01 = 1000379,
        SKILL_03_START          = 1000378,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef SUMKALKY_ANIM_H__
