// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUMCOMBODEFENDER_ANIM_H__
#define SUMCOMBODEFENDER_ANIM_H__

namespace SumComboDefender_Anim
{
    enum
    {
        ATTK_01_01              = 1000202,
        ATTK_01_01_START        = 1000201,
        ATTK_01_02              = 1000203,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        OPENING_01              = 1000080,
        RUN_01                  = 1000055,
        STANDUP_01              = 1000024,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef SUMCOMBODEFENDER_ANIM_H__
