// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUMDRAGOON_ANIM_H__
#define SUMDRAGOON_ANIM_H__

namespace SumDragoon_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DASHATTK_01             = 1000202,
        DASHATTK_01_BTLIDLE     = 1000204,
        DASHATTK_01_FINISH      = 1000203,
        DASHATTK_01_START       = 1000201,
        EARTHQUAKE_BATTLEIDLE   = 1000387,
        EARTHQUAKE_BATTLEIDLE_START = 1000386,
        EARTHQUAKE_FINISH       = 1000389,
        EARTHQUAKE_FIRE         = 1000388,
        LIDLE_01                = 1000001,
        LIDLE_02                = 1000002,
        OPENING                 = 1000081,
        RE_GAIN_01              = 1000110,
        RUN_01                  = 1000055,
        SKILL_SHOT_01           = 1000310,
        SKILL_SHOT_01_FINISH    = 1000313,
        SKILL_SHOT_01_START     = 1000312,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef SUMDRAGOON_ANIM_H__
