// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METEOR_STRIKE_ANIM_H__
#define METEOR_STRIKE_ANIM_H__

namespace Meteor_strike_Anim
{
    enum
    {
        IDLE_01                 = 2,
        OPENNING_01             = 0,
        OUT_01                  = 1,
        SHOT_01                 = 3
    };
}

#endif  // #ifndef METEOR_STRIKE_ANIM_H__
