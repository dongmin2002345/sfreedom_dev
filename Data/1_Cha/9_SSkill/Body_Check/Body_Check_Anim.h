// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BODY_CHECK_ANIM_H__
#define BODY_CHECK_ANIM_H__

namespace Body_Check_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        FINISH_01               = 1000025,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef BODY_CHECK_ANIM_H__
