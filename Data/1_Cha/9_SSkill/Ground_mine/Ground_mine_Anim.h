// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GROUND_MINE_ANIM_H__
#define GROUND_MINE_ANIM_H__

namespace Ground_mine_Anim
{
    enum
    {
        BOOM_01                 = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef GROUND_MINE_ANIM_H__
