// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SIEGE_CANNON_ANIM_H__
#define SIEGE_CANNON_ANIM_H__

namespace Siege_Cannon_Anim
{
    enum
    {
        ATTK_01                 = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef SIEGE_CANNON_ANIM_H__
