// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHAIN_SHIELD_ANIM_H__
#define CHAIN_SHIELD_ANIM_H__

namespace Chain_Shield_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef CHAIN_SHIELD_ANIM_H__
