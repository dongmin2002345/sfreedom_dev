// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SNOWBLUES_ANIM_H__
#define SNOWBLUES_ANIM_H__

namespace SnowBlues_Anim
{
    enum
    {
        SNOW_BLUES_01_01        = 1000061,
        SNOW_BLUES_01_02        = 1000062,
        SNOW_BLUES_01_03        = 1000063
    };
}

#endif  // #ifndef SNOWBLUES_ANIM_H__
