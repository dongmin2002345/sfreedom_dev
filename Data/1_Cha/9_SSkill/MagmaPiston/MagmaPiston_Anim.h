// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAGMAPISTON_ANIM_H__
#define MAGMAPISTON_ANIM_H__

namespace MagmaPiston_Anim
{
    enum
    {
        SKILL_01_ATTACK         = 1000001,
        SKILL_01_FINISH         = 1000002,
        SKILL_01_START          = 1000000
    };
}

#endif  // #ifndef MAGMAPISTON_ANIM_H__
