// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef METAMORPHOSIS_ANIM_H__
#define METAMORPHOSIS_ANIM_H__

namespace Metamorphosis_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        ATTK_03                 = 1000063,
        ATTK_DOWN_01            = 1000065,
        ATTK_DOWN_02            = 1000066,
        ATTK_DOWN_03            = 1000067,
        ATTK_DOWN_FLY_01        = 1000068,
        BLOWUP_DOWN_01          = 1000042,
        BLOWUP_LOOP_01          = 1000041,
        BREAKFALL_01            = 1000043,
        BREAKFALL_02            = 1000044,
        BREAKFALL_03            = 1000051,
        BREAKFALL_04            = 1000052,
        BTLIDLE_01              = 1000005,
        DASH_01                 = 1000210,
        DASH_ATTK_01            = 1000211,
        DIE_01                  = 1000021,
        DIE_IDLE_01             = 1000022,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        JUMP_01                 = 1000101,
        JUMP_02                 = 1000102,
        JUMP_03                 = 1000104,
        JUMP_ATTK_01            = 1000106,
        JUMP_ATTK_02            = 1000107,
        JUMP_ATTK_03            = 1000108,
        JUMP_ATTK_04            = 1000109,
        JUMP_DASH_01            = 1000112,
        JUMP_DOWN_ATTK_01       = 1000110,
        JUMP_DOWN_ATTK_02       = 1000111,
        JUMP_IDLE_01            = 1000103,
        KNOCKDOWN_01            = 1000045,
        KNOCKDOWN_IDLE_01       = 1000046,
        KNOCKDOWN_UP_01         = 1000047,
        LADDER_DOWN_01          = 1000303,
        LADDER_IDLE_01          = 1000302,
        LADDER_UP_01            = 1000301,
        RUN_01                  = 1000055,
        SKILL_FALLINGLEAF_01    = 1000201,
        SKILL_KILLINGMIST_01    = 1000202,
        SKILL_LIGHTNINGSLASH_01 = 1000203,
        SKILL_LIGHTNINGSLASH_02 = 1000208,
        SKILL_LIGHTNINGSLASH_03 = 1000209,
        SKILL_MOONSLASH_01_01   = 1000204,
        SKILL_MOONSLASH_01_02   = 1000205,
        SKILL_SAKURASPRINKLE_01 = 1000206,
        SKILL_SKYPRICE_01       = 1000207,
        SPELL_01                = 1000200,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef METAMORPHOSIS_ANIM_H__
