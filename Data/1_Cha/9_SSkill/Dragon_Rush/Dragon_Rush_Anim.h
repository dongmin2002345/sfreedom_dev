// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRAGON_RUSH_ANIM_H__
#define DRAGON_RUSH_ANIM_H__

namespace Dragon_Rush_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef DRAGON_RUSH_ANIM_H__
