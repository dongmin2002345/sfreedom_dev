// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ENERGYBALL_ANIM_H__
#define ENERGYBALL_ANIM_H__

namespace EnergyBall_Anim
{
    enum
    {
        SHOT                    = 1000000
    };
}

#endif  // #ifndef ENERGYBALL_ANIM_H__
