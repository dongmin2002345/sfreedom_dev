// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHANGE_BBOY_F_ANIM_H__
#define CHANGE_BBOY_F_ANIM_H__

namespace Change_bboy_F_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        ATTK_02                 = 1000062,
        ATTK_03                 = 1000063,
        ATTK_04                 = 1000064,
        ATTK_05                 = 1000065,
        ATTK_DOWN_01            = 1000066,
        ATTK_DOWN_02            = 1000067,
        ATTK_DOWN_FLY_01        = 1000068,
        BLOWUP_DOWN_01          = 1000042,
        BLOWUP_LOOP_01          = 1000041,
        BREAKFALL_01            = 1000043,
        BTLIDLE_01              = 1000005,
        DASH_01                 = 1000210,
        DASH_ATTK_01            = 1000211,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000000,
        JUMP_01                 = 1000101,
        JUMP_02                 = 1000102,
        JUMP_03                 = 1000104,
        JUMP_ATTK_01_01         = 1000106,
        JUMP_ATTK_01_02         = 1000107,
        JUMP_DASH_01            = 1000112,
        JUMP_DOWN_ATTK_01_01    = 1000110,
        JUMP_DOWN_ATTK_01_02    = 1000111,
        JUMP_IDLE_01            = 1000103,
        KNOCKDOWN_01            = 1000045,
        KNOCKDOWN_IDLE_01       = 1000046,
        KNOCKDOWN_UP_01         = 1000047,
        LADDER_DOWN_01          = 1000303,
        LADDER_IDLE_01          = 1000302,
        LADDER_UP_01            = 1000301,
        RUN_01                  = 1000055,
        SKILL_NINETEENNINE_01_01 = 1000201,
        SKILL_NINETEENNINE_01_02 = 1000202,
        SKILL_NINETEENNINE_01_03 = 1000203,
        SKILL_SEVEN_01          = 1000204,
        SKILL_WINDMILL_01_01    = 1000205,
        SKILL_WINDMILL_01_02    = 1000206,
        SKILL_WINDMILL_01_03    = 1000207,
        SPELL_01                = 1000200,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef CHANGE_BBOY_F_ANIM_H__
