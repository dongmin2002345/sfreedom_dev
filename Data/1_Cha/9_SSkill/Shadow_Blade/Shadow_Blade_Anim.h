// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SHADOW_BLADE_ANIM_H__
#define SHADOW_BLADE_ANIM_H__

namespace Shadow_Blade_Anim
{
    enum
    {
        SKILL_SHADOWBLADE_01_01 = 1000000,
        SKILL_SHADOWBLADE_01_02 = 1000001
    };
}

#endif  // #ifndef SHADOW_BLADE_ANIM_H__
