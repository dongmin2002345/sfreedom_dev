// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GROUND_ZERO_ANIM_H__
#define GROUND_ZERO_ANIM_H__

namespace Ground_Zero_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef GROUND_ZERO_ANIM_H__
