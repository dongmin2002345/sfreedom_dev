// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ROLLINGLOG_ANIM_H__
#define ROLLINGLOG_ANIM_H__

namespace RollingLog_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef ROLLINGLOG_ANIM_H__
