// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAGIC_STONE_ANIM_H__
#define MAGIC_STONE_ANIM_H__

namespace Magic_Stone_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        IDLE_01                 = 1000001,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef MAGIC_STONE_ANIM_H__
