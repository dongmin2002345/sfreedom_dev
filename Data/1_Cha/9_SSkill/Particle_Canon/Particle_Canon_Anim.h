// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PARTICLE_CANON_ANIM_H__
#define PARTICLE_CANON_ANIM_H__

namespace Particle_Canon_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef PARTICLE_CANON_ANIM_H__
