// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BONEDRAGON_SKILL_ANIM_H__
#define BONEDRAGON_SKILL_ANIM_H__

namespace BoneDragon_skill_Anim
{
    enum
    {
        ATTK_01                 = 1000000
    };
}

#endif  // #ifndef BONEDRAGON_SKILL_ANIM_H__
