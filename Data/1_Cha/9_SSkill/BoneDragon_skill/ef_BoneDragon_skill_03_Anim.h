// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_BONEDRAGON_SKILL_03_ANIM_H__
#define EF_BONEDRAGON_SKILL_03_ANIM_H__

namespace ef_BoneDragon_skill_03_Anim
{
    enum
    {
        IDLE_0001               = 1000000
    };
}

#endif  // #ifndef EF_BONEDRAGON_SKILL_03_ANIM_H__
