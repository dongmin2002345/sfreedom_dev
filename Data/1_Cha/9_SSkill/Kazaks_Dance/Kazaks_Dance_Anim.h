// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef KAZAKS_DANCE_ANIM_H__
#define KAZAKS_DANCE_ANIM_H__

namespace Kazaks_Dance_Anim
{
    enum
    {
        DANCE_01                = 1000001,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef KAZAKS_DANCE_ANIM_H__
