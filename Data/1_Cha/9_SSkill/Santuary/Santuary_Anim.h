// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SANTUARY_ANIM_H__
#define SANTUARY_ANIM_H__

namespace Santuary_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        OPENING_01              = 1000000
    };
}

#endif  // #ifndef SANTUARY_ANIM_H__
