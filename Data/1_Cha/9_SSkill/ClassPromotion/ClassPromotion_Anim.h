// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CLASSPROMOTION_ANIM_H__
#define CLASSPROMOTION_ANIM_H__

namespace ClassPromotion_Anim
{
    enum
    {
        DRAGON                  = 1000001,
        HUMAN                   = 1000002
    };
}

#endif  // #ifndef CLASSPROMOTION_ANIM_H__
