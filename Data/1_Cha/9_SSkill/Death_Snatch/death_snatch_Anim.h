// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEATH_SNATCH_ANIM_H__
#define DEATH_SNATCH_ANIM_H__

namespace death_snatch_Anim
{
    enum
    {
        SKILL_01                = 1000000,
        SKILL_02                = 1000001,
        SKILL_03                = 1000002
    };
}

#endif  // #ifndef DEATH_SNATCH_ANIM_H__
