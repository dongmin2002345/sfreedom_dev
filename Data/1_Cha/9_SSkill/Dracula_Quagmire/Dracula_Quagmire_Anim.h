// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRACULA_QUAGMIRE_ANIM_H__
#define DRACULA_QUAGMIRE_ANIM_H__

namespace Dracula_Quagmire_Anim
{
    enum
    {
        ATTK_01                 = 1000000
    };
}

#endif  // #ifndef DRACULA_QUAGMIRE_ANIM_H__
