// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_GRANADE_WP_02_ANIM_H__
#define EF_GRANADE_WP_02_ANIM_H__

namespace ef_Granade_WP_02_Anim
{
    enum
    {
        ATTK_01                 = 1000060
    };
}

#endif  // #ifndef EF_GRANADE_WP_02_ANIM_H__
