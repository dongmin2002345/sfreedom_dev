// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FIREBALL_DRAGON_ANIM_H__
#define FIREBALL_DRAGON_ANIM_H__

namespace FireBall_Dragon_Anim
{
    enum
    {
        FIRE_BALL               = 1000000
    };
}

#endif  // #ifndef FIREBALL_DRAGON_ANIM_H__
