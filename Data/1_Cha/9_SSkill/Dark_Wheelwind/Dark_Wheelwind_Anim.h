// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DARK_WHEELWIND_ANIM_H__
#define DARK_WHEELWIND_ANIM_H__

namespace Dark_Wheelwind_Anim
{
    enum
    {
        SKILL_01                = 1000000
    };
}

#endif  // #ifndef DARK_WHEELWIND_ANIM_H__
