// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_SIEGEDRAGON_BIG2_SKILL_ANIM_H__
#define EF_SIEGEDRAGON_BIG2_SKILL_ANIM_H__

namespace ef_Siegedragon_Big2_skill_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef EF_SIEGEDRAGON_BIG2_SKILL_ANIM_H__
