// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EARTHQUAKE_GUARDZONE_ANIM_H__
#define EARTHQUAKE_GUARDZONE_ANIM_H__

namespace Earthquake_guardzone_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef EARTHQUAKE_GUARDZONE_ANIM_H__
