// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef REST13_ANIM_H__
#define REST13_ANIM_H__

namespace Rest13_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001
    };
}

#endif  // #ifndef REST13_ANIM_H__
