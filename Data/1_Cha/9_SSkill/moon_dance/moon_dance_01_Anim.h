// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MOON_DANCE_01_ANIM_H__
#define MOON_DANCE_01_ANIM_H__

namespace moon_dance_01_Anim
{
    enum
    {
        SKILL_MOON_DANCE_01     = 1000000
    };
}

#endif  // #ifndef MOON_DANCE_01_ANIM_H__
