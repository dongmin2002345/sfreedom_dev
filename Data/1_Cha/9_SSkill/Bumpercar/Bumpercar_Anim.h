// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BUMPERCAR_ANIM_H__
#define BUMPERCAR_ANIM_H__

namespace Bumpercar_Anim
{
    enum
    {
        ANI_01                  = 0
    };
}

#endif  // #ifndef BUMPERCAR_ANIM_H__
