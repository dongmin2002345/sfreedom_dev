// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEATH_ANIM_H__
#define DEATH_ANIM_H__

namespace Death_Anim
{
    enum
    {
        ATTK_01                 = 1000061
    };
}

#endif  // #ifndef DEATH_ANIM_H__
