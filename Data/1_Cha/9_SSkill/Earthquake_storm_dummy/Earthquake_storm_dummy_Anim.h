// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EARTHQUAKE_STORM_DUMMY_ANIM_H__
#define EARTHQUAKE_STORM_DUMMY_ANIM_H__

namespace Earthquake_storm_dummy_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef EARTHQUAKE_STORM_DUMMY_ANIM_H__
