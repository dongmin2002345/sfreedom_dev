// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P08_C01_ANIM_H__
#define P08_C01_ANIM_H__

namespace P08_C01_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef P08_C01_ANIM_H__
