// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_S201_ANIM_H__
#define R_S201_ANIM_H__

namespace R_S201_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000205,
        BTLIDLE_01_SHOT_START   = 1000204,
        IDLE_01                 = 1000001,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RIDING_SHOT_01          = 1000201,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000206,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef R_S201_ANIM_H__
