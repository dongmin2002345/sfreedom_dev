// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef S_PERSEUS_ANIM_H__
#define S_PERSEUS_ANIM_H__

namespace S_Perseus_Anim
{
    enum
    {
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        COMBO_01_FINISH         = 1002006,
        COMBO_ATTK_01           = 1002003,
        COMBO_BTLIDLE_01_START  = 1002001,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        MASSIVE_PROJECTILE_BTLIDLE_START = 1000256,
        MASSIVE_PROJECTILE_FINISH = 1000258,
        MASSIVE_PROJECTILE_FIRE = 1000259,
        RUN_01                  = 1000055,
        STANDUP                 = 1000024,
        STUN                    = 1000025,
        WALLK_01                = 1000050
    };
}

#endif  // #ifndef S_PERSEUS_ANIM_H__
