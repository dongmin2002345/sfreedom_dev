// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_HUMMING_BIRD_ANIM_H__
#define P_HUMMING_BIRD_ANIM_H__

namespace P_Humming_Bird_Anim
{
    enum
    {
        BTIDLE_01               = 1000005,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_BTIDLE         = 1000202,
        SKILL_01_START          = 1000201,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTIDLE         = 1000205,
        SKILL_02_START          = 1000204,
        SKILL_03_ATTACK         = 1000209,
        SKILL_03_BTIDLE         = 1000208,
        SKILL_03_START          = 1000207,
        WALK_01                 = 1000050,
        WARP_01                 = 1000057
    };
}

#endif  // #ifndef P_HUMMING_BIRD_ANIM_H__
