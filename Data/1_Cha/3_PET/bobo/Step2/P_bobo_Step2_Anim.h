// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_BOBO_STEP2_ANIM_H__
#define P_BOBO_STEP2_ANIM_H__

namespace P_bobo_Step2_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        JUMP_01                 = 1000101,
        JUMP_02                 = 1000102,
        JUMP_03                 = 1000104,
        JUMP_IDLE_01            = 1000103,
        RUN_01                  = 1000055,
        WALK_01                 = 1000050,
        WARP_01                 = 1000105
    };
}

#endif  // #ifndef P_BOBO_STEP2_ANIM_H__
