// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P06_C01_ANIM_H__
#define P06_C01_ANIM_H__

namespace P06_C01_Anim
{
    enum
    {
        CHAR_INTRO_ACTION_01    = 1300014,
        CHAR_INTRO_ACTION_02    = 1300015,
        CHAR_INTRO_CANCLE_01    = 1300016,
        CHAR_INTRO_IDLE_01_01   = 1300011,
        CHAR_INTRO_IDLE_01_02   = 1300012,
        CHAR_INTRO_SELECT_01    = 1300013,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef P06_C01_ANIM_H__
