// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_PEGASUS_ENDING_ANIM_H__
#define R_PEGASUS_ENDING_ANIM_H__

namespace R_Pegasus_ending_Anim
{
    enum
    {
        SCENE_01                = 1000001
    };
}

#endif  // #ifndef R_PEGASUS_ENDING_ANIM_H__
