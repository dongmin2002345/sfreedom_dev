// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HERCULES_ANIM_H__
#define HERCULES_ANIM_H__

namespace Hercules_Anim
{
    enum
    {
        BLOWUP_DOWN             = 1000023,
        BLOWUP_LOOP             = 1000022,
        COMBO_01_FINISH         = 1002006,
        COMBO_ATTK_01           = 1002003,
        COMBO_BTLIDLE_01        = 1002002,
        COMBO_BTLIDLE_01_START  = 1002001,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        EARTHQUAKE_BATTLEIDLE   = 1001172,
        EARTHQUAKE_BATTLEIDLE_START = 1001171,
        EARTHQUAKE_FIRE         = 1001173,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        RUN_01                  = 1000055,
        STANDUP                 = 1000024,
        STUN_01                 = 1000025,
        WALK_01                 = 1000050,
        WHIRLWIND_01            = 1000803,
        WHIRLWIND_01_FINISH     = 1000804,
        WHIRLWIND_BTLIDLE_01    = 1000802,
        WHIRLWIND_BTLIDLE_01_START = 1000801
    };
}

#endif  // #ifndef HERCULES_ANIM_H__
