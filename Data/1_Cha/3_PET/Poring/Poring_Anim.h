// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PORING_ANIM_H__
#define PORING_ANIM_H__

namespace Poring_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        ATTK_03_01              = 1000208,
        ATTK_03_01_START        = 1000207,
        ATTK_03_02              = 1000209,
        BTLIDLE_01              = 1000005,
        DASH_01                 = 1000221,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        RUN_01                  = 1000055,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef PORING_ANIM_H__
