// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUNFLOWER02_ANIM_H__
#define SUNFLOWER02_ANIM_H__

namespace Sunflower02_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef SUNFLOWER02_ANIM_H__
