// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef S_FLIX_ANIM_H__
#define S_FLIX_ANIM_H__

namespace S_Flix_Anim
{
    enum
    {
        ATTK_02_01              = 1000205,
        ATTK_02_01_START        = 1000204,
        ATTK_02_02              = 1000206,
        BTIDLE_01               = 1000005,
        DIE_01                  = 1000020,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        RUN_01                  = 1000055,
        STUN_01                 = 1000025
    };
}

#endif  // #ifndef S_FLIX_ANIM_H__
