// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SUNFLOWER03_ANIM_H__
#define SUNFLOWER03_ANIM_H__

namespace Sunflower03_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        SKILL_01_FINISH         = 1000203,
        SKILL_01_START          = 1000201,
        SKILL_01_START_BLTIDLE  = 1000202
    };
}

#endif  // #ifndef SUNFLOWER03_ANIM_H__
