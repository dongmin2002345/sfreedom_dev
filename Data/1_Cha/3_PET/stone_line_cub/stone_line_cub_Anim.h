// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef STONE_LINE_CUB_ANIM_H__
#define STONE_LINE_CUB_ANIM_H__

namespace stone_line_cub_Anim
{
    enum
    {
        BREATH_01               = 1000347,
        BREATH_01_FINISH        = 1000348,
        BREATH_BTLIDLE_01       = 1000346,
        BREATH_BTLIDLE_01_START = 1000345,
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        DMG_02                  = 1000012,
        FURY_01                 = 1000044,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        STUN                    = 1000025,
        WALK_01                 = 1000050,
        WARP_01                 = 1000057
    };
}

#endif  // #ifndef STONE_LINE_CUB_ANIM_H__
