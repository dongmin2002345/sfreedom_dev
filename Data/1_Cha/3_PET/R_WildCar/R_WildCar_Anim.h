// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_WILDCAR_ANIM_H__
#define R_WILDCAR_ANIM_H__

namespace R_WildCar_Anim
{
    enum
    {
        ATTK_02_02              = 1000206,
        BTLIDLE_01              = 1000005,
        IDLE_01                 = 1000001,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000307,
        SHOT_01_START           = 1000306,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef R_WILDCAR_ANIM_H__
