// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P04_C01_ANIM_H__
#define P04_C01_ANIM_H__

namespace P04_C01_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef P04_C01_ANIM_H__
