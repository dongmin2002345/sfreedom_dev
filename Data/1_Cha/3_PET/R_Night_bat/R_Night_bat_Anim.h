// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_NIGHT_BAT_ANIM_H__
#define R_NIGHT_BAT_ANIM_H__

namespace R_Night_bat_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        BTLIDLE_01_SHOT         = 1000251,
        BTLIDLE_01_SHOT_START   = 1000250,
        BUFF_01                 = 1000231,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000252,
        WALK_01                 = 1000050,
        WARP_01                 = 1000057
    };
}

#endif  // #ifndef R_NIGHT_BAT_ANIM_H__
