// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_POOYAN_STEP2_ANIM_H__
#define P_POOYAN_STEP2_ANIM_H__

namespace P_Pooyan_step2_Anim
{
    enum
    {
        IDLE_01                 = 0,
        IDLE_02                 = 1,
        RUN_01                  = 2,
        WALK_01                 = 3
    };
}

#endif  // #ifndef P_POOYAN_STEP2_ANIM_H__
