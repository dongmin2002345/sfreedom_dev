// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_POOYAN_STEP1_ANIM_H__
#define P_POOYAN_STEP1_ANIM_H__

namespace P_Pooyan_step1_Anim
{
    enum
    {
        IDLE_01                 = 0
    };
}

#endif  // #ifndef P_POOYAN_STEP1_ANIM_H__
