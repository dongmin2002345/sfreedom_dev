// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_DRAGONWING_STAFF_ANIM_H__
#define R_DRAGONWING_STAFF_ANIM_H__

namespace R_DragonWing_Staff_Anim
{
    enum
    {
        DASH_01                 = 1250040,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01                = 1000231,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef R_DRAGONWING_STAFF_ANIM_H__
