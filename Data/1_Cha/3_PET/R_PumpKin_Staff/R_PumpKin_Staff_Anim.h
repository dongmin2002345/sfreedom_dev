// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_PUMPKIN_STAFF_ANIM_H__
#define R_PUMPKIN_STAFF_ANIM_H__

namespace R_PumpKin_Staff_Anim
{
    enum
    {
        BTLIDLE_01              = 1000002,
        DASH_01                 = 1250040,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000003,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01                = 1000231,
        WALK_01                 = 1000050
    };
}

#endif  // #ifndef R_PUMPKIN_STAFF_ANIM_H__
