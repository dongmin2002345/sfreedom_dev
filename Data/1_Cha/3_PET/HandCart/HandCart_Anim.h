// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HANDCART_ANIM_H__
#define HANDCART_ANIM_H__

namespace HandCart_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef HANDCART_ANIM_H__
