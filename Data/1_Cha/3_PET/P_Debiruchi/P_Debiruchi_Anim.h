// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_DEBIRUCHI_ANIM_H__
#define P_DEBIRUCHI_ANIM_H__

namespace P_Debiruchi_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000252,
        SKILL_01_BTIDLE         = 1000251,
        SKILL_01_START          = 1000250,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTIDLE         = 1000205,
        SKILL_02_START          = 1000204
    };
}

#endif  // #ifndef P_DEBIRUCHI_ANIM_H__
