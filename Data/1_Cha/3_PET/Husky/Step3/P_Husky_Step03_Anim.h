// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_HUSKY_STEP03_ANIM_H__
#define P_HUSKY_STEP03_ANIM_H__

namespace P_Husky_Step03_Anim
{
    enum
    {
        BTLIDLE_01              = 1000011,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        IDLE_03                 = 1000003,
        IDLE_04                 = 1000004,
        IDLE_05                 = 1000005,
        IDLE_DOWN_01            = 1000009,
        IDLE_UP_01              = 1000010,
        RUN_01                  = 1000051
    };
}

#endif  // #ifndef P_HUSKY_STEP03_ANIM_H__
