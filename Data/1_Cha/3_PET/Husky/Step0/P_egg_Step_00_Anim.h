// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_EGG_STEP_00_ANIM_H__
#define P_EGG_STEP_00_ANIM_H__

namespace P_egg_Step_00_Anim
{
    enum
    {
        IDLE_01                 = 90001,
        IDLE_02                 = 90002,
        IDLE_03                 = 90003
    };
}

#endif  // #ifndef P_EGG_STEP_00_ANIM_H__
