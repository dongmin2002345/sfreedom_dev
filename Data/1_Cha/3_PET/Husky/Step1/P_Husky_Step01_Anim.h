// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_HUSKY_STEP01_ANIM_H__
#define P_HUSKY_STEP01_ANIM_H__

namespace P_Husky_Step01_Anim
{
    enum
    {
        IDLE_01                 = 90001
    };
}

#endif  // #ifndef P_HUSKY_STEP01_ANIM_H__
