// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_GRIFFON_STEP3_ANIM_H__
#define P_GRIFFON_STEP3_ANIM_H__

namespace P_Griffon_Step3_Anim
{
    enum
    {
        ATTK_01                 = 1000061,
        FLY_01                  = 1000051,
        FLYIDLE_01              = 1000005,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        SKILL_01                = 1000201
    };
}

#endif  // #ifndef P_GRIFFON_STEP3_ANIM_H__
