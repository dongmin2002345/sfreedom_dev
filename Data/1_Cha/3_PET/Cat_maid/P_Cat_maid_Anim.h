// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_CAT_MAID_ANIM_H__
#define P_CAT_MAID_ANIM_H__

namespace P_Cat_maid_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        RUN_01                  = 1000051
    };
}

#endif  // #ifndef P_CAT_MAID_ANIM_H__
