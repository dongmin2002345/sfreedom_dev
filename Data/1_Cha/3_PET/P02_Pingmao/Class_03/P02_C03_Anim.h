// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P02_C03_ANIM_H__
#define P02_C03_ANIM_H__

namespace P02_C03_Anim
{
    enum
    {
        ANNOY_01                = 1000093,
        ATTK_01                 = 1000201,
        BUFF_01                 = 1000231,
        CHARGE_01               = 1000241,
        DASH_01                 = 1000221,
        DIE_01                  = 1000021,
        EXERCISE_01             = 1000072,
        HUNGRY_01               = 1000091,
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        IDLE_04                 = 1000003,
        IDLE_05                 = 1000004,
        IDLE_06                 = 1000005,
        MEAL_01                 = 1000071,
        PLAY_01                 = 1000073,
        RUN_01                  = 1000055,
        SHOT_01                 = 1000211,
        SLEEP_01                = 1000092,
        WALK_01                 = 1000051,
        WARP_01                 = 1000105,
        WHEELWIND_01            = 1000251
    };
}

#endif  // #ifndef P02_C03_ANIM_H__
