// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef P_SOI_ANIM_H__
#define P_SOI_ANIM_H__

namespace P_Soi_Anim
{
    enum
    {
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_BTLIDLE        = 1000251,
        SKILL_01_FINISH         = 1000252,
        SKILL_01_START          = 1000250,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_FINISH         = 1000206,
        SKILL_02_START          = 1000204
    };
}

#endif  // #ifndef P_SOI_ANIM_H__
