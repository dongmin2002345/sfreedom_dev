// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PARIS_PET_ANIM_H__
#define PARIS_PET_ANIM_H__

namespace Paris_pet_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        WALK_01                 = 1000051
    };
}

#endif  // #ifndef PARIS_PET_ANIM_H__
