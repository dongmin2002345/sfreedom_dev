// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ARAM_PET_ANIM_H__
#define ARAM_PET_ANIM_H__

namespace Aram_pet_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001,
        IDLE_03                 = 1000002,
        RUN_01                  = 1000055
    };
}

#endif  // #ifndef ARAM_PET_ANIM_H__
