// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef R_BLUE_LIZARD_ANIM_H__
#define R_BLUE_LIZARD_ANIM_H__

namespace R_Blue_Lizard_Anim
{
    enum
    {
        BTLIDLE_01              = 1000005,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001,
        IDLE_02                 = 1000002,
        JUMP_01                 = 1000030,
        JUMP_02                 = 1000031,
        JUMP_03                 = 1000033,
        JUMP_IDLE_01            = 1000032,
        JUMP_IDLE_02            = 1000035,
        OPENING_01              = 1000081,
        RUN_01                  = 1000055,
        SKILL_01_ATTACK         = 1000203,
        SKILL_01_ATTACK_01      = 1000213,
        SKILL_01_BTLIDLE        = 1000202,
        SKILL_01_BTLIDLE_02     = 1000212,
        SKILL_01_START          = 1000201,
        SKILL_01_START_02       = 1000211,
        SKILL_02_ATTACK         = 1000206,
        SKILL_02_BTLIDLE        = 1000205,
        SKILL_02_START          = 1000204,
        WALK_01                 = 1000050,
        WARP_01                 = 1000057
    };
}

#endif  // #ifndef R_BLUE_LIZARD_ANIM_H__
