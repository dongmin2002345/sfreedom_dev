// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef X_FAIRY1001_03_ANIM_H__
#define X_FAIRY1001_03_ANIM_H__

namespace x_Fairy1001_03_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001
    };
}

#endif  // #ifndef X_FAIRY1001_03_ANIM_H__
