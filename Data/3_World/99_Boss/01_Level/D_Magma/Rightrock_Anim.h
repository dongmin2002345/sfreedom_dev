// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RIGHTROCK_ANIM_H__
#define RIGHTROCK_ANIM_H__

namespace Rightrock_Anim
{
    enum
    {
        DMG_01                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef RIGHTROCK_ANIM_H__
