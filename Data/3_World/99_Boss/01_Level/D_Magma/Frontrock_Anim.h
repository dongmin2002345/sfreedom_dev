// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef FRONTROCK_ANIM_H__
#define FRONTROCK_ANIM_H__

namespace Frontrock_Anim
{
    enum
    {
        DMG_01                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef FRONTROCK_ANIM_H__
