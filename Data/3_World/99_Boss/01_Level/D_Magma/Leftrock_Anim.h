// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef LEFTROCK_ANIM_H__
#define LEFTROCK_ANIM_H__

namespace Leftrock_Anim
{
    enum
    {
        DMG_01                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef LEFTROCK_ANIM_H__
