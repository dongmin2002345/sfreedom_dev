// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CAMERA_ANIM_H__
#define CAMERA_ANIM_H__

namespace camera_Anim
{
    enum
    {
        ENDING                  = 1,
        OPENING                 = 0
    };
}

#endif  // #ifndef CAMERA_ANIM_H__
