// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_BOSSROOM1001_01_ANIM_H__
#define ELGA_BOSSROOM1001_01_ANIM_H__

namespace Elga_Bossroom1001_01_Anim
{
    enum
    {
        ELGA_01                 = 1000011,
        ELGA_01_TRANS           = 1000020,
        ELGA_02                 = 1000021,
        ELGA_02_TRANS           = 1000030,
        ELGA_03                 = 1000031,
        ELGA_03_TRANS           = 1000032,
        ELGA_04                 = 1000033
    };
}

#endif  // #ifndef ELGA_BOSSROOM1001_01_ANIM_H__
