// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ELGA_BOSSROOM_HERO_ANIM_H__
#define ELGA_BOSSROOM_HERO_ANIM_H__

namespace Elga_Bossroom_Hero_Anim
{
    enum
    {
        ELGA_03                 = 1000000
    };
}

#endif  // #ifndef ELGA_BOSSROOM_HERO_ANIM_H__
