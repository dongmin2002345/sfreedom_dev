// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CORE_BLUE_ANIM_H__
#define CORE_BLUE_ANIM_H__

namespace Core_Blue_Anim
{
    enum
    {
        IDLE_01                 = 0
    };
}

#endif  // #ifndef CORE_BLUE_ANIM_H__
