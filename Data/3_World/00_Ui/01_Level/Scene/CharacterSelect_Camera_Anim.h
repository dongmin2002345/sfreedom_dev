// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CHARACTERSELECT_CAMERA_ANIM_H__
#define CHARACTERSELECT_CAMERA_ANIM_H__

namespace CharacterSelect_Camera_Anim
{
    enum
    {
        CREATEDRAGON            = 1000202,
        CREATEDRAGON_IN         = 1000201,
        CREATEDRAGON_OUT        = 1000203,
        CREATEHUMAN             = 1000102,
        CREATEHUMAN_IN          = 1000101,
        CREATEHUMAN_OUT         = 1000103,
        SELECT_CHARACTER        = 1000000
    };
}

#endif  // #ifndef CHARACTERSELECT_CAMERA_ANIM_H__
