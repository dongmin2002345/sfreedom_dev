// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_AXTRAP02_ANIM_H__
#define VAN_AXTRAP02_ANIM_H__

namespace Van_AxTrap02_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef VAN_AXTRAP02_ANIM_H__
