// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_GREENPLANT1005_01_ANIM_H__
#define VAN_GREENPLANT1005_01_ANIM_H__

namespace Van_GreenPlant1005_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef VAN_GREENPLANT1005_01_ANIM_H__
