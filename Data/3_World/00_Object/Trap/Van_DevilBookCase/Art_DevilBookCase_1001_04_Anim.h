// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ART_DEVILBOOKCASE_1001_04_ANIM_H__
#define ART_DEVILBOOKCASE_1001_04_ANIM_H__

namespace Art_DevilBookCase_1001_04_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef ART_DEVILBOOKCASE_1001_04_ANIM_H__
