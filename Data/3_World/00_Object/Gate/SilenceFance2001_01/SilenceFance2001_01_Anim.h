// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SILENCEFANCE2001_01_ANIM_H__
#define SILENCEFANCE2001_01_ANIM_H__

namespace SilenceFance2001_01_Anim
{
    enum
    {
        CLOSE_01                = 1000011,
        CLOSE_IDLE_01           = 1000000,
        OPEN_01                 = 1000030,
        OPEN_IDLE_01            = 1000020
    };
}

#endif  // #ifndef SILENCEFANCE2001_01_ANIM_H__
