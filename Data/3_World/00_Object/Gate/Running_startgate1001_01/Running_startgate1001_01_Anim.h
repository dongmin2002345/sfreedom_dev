// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef RUNNING_STARTGATE1001_01_ANIM_H__
#define RUNNING_STARTGATE1001_01_ANIM_H__

namespace Running_startgate1001_01_Anim
{
    enum
    {
        CLOSE_01                = 1000011,
        CLOSE_IDLE_01           = 1000000,
        OPEN_01                 = 1000030,
        OPEN_IDLE               = 1000020
    };
}

#endif  // #ifndef RUNNING_STARTGATE1001_01_ANIM_H__
