// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ART_ROKET_1001_01_ANIM_H__
#define ART_ROKET_1001_01_ANIM_H__

namespace Art_Roket_1001_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        START_01                = 1000001
    };
}

#endif  // #ifndef ART_ROKET_1001_01_ANIM_H__
