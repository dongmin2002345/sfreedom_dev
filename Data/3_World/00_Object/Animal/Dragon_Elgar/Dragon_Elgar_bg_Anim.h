// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DRAGON_ELGAR_BG_ANIM_H__
#define DRAGON_ELGAR_BG_ANIM_H__

namespace Dragon_Elgar_bg_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef DRAGON_ELGAR_BG_ANIM_H__
