// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PIG1001_01_ANIM_H__
#define PIG1001_01_ANIM_H__

namespace Pig1001_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        IDLE_02                 = 1000001
    };
}

#endif  // #ifndef PIG1001_01_ANIM_H__
