// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_GUARD_STONE_B_01_ANIM_H__
#define MISSION_GUARD_STONE_B_01_ANIM_H__

namespace Mission_Guard_Stone_B_01_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef MISSION_GUARD_STONE_B_01_ANIM_H__
