// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_PUMPKIN_CANDY_LEVEL02_ANIM_H__
#define MISSION_PUMPKIN_CANDY_LEVEL02_ANIM_H__

namespace Mission_Pumpkin_candy_Level02_Anim
{
    enum
    {
        LEVEL02_DIE_01          = 1000021,
        LEVEL02_DMG_01          = 1000011,
        LEVEL02_IDLE_01         = 1000001
    };
}

#endif  // #ifndef MISSION_PUMPKIN_CANDY_LEVEL02_ANIM_H__
