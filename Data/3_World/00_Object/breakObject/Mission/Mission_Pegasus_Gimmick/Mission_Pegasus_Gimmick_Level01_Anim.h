// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_PEGASUS_GIMMICK_LEVEL01_ANIM_H__
#define MISSION_PEGASUS_GIMMICK_LEVEL01_ANIM_H__

namespace Mission_Pegasus_Gimmick_Level01_Anim
{
    enum
    {
        DIE_01                  = 1000020,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef MISSION_PEGASUS_GIMMICK_LEVEL01_ANIM_H__
