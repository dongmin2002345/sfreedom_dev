// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_SANTA_LEVEL03_ANIM_H__
#define MISSION_SANTA_LEVEL03_ANIM_H__

namespace Mission_Santa_Level03_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef MISSION_SANTA_LEVEL03_ANIM_H__
