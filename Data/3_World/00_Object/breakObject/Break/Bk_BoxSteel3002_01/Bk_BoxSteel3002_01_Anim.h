// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BK_BOXSTEEL3002_01_ANIM_H__
#define BK_BOXSTEEL3002_01_ANIM_H__

namespace Bk_BoxSteel3002_01_Anim
{
    enum
    {
        CLOSEIDLE_01            = 1000000,
        DMG_01                  = 1000011,
        OPEN_01                 = 1000030,
        OPENIDLE_01             = 1000020
    };
}

#endif  // #ifndef BK_BOXSTEEL3002_01_ANIM_H__
