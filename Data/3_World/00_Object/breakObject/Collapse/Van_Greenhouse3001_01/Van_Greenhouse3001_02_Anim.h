// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_GREENHOUSE3001_02_ANIM_H__
#define VAN_GREENHOUSE3001_02_ANIM_H__

namespace Van_Greenhouse3001_02_Anim
{
    enum
    {
        DIE_02                  = 1000020,
        DMG_02                  = 1000011,
        IDLE_02                 = 1000000
    };
}

#endif  // #ifndef VAN_GREENHOUSE3001_02_ANIM_H__
