// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ART_MANASTONE1003_01_ANIM_H__
#define ART_MANASTONE1003_01_ANIM_H__

namespace Art_ManaStone1003_01_Anim
{
    enum
    {
        DLE_01                  = 1000020,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000,
        UP_01                   = 1000030,
        UP_02                   = 1000031
    };
}

#endif  // #ifndef ART_MANASTONE1003_01_ANIM_H__
