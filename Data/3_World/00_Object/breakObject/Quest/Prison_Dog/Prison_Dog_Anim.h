// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PRISON_DOG_ANIM_H__
#define PRISON_DOG_ANIM_H__

namespace Prison_Dog_Anim
{
    enum
    {
        DIE_01                  = 1000021,
        DMG_01                  = 1000011,
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef PRISON_DOG_ANIM_H__
