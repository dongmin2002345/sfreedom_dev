// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSION_PORTAL_1001_02_ANIM_H__
#define MISSION_PORTAL_1001_02_ANIM_H__

namespace Mission_Portal_1001_02_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        OPENING_01              = 1000081
    };
}

#endif  // #ifndef MISSION_PORTAL_1001_02_ANIM_H__
