// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VETA_TRAP1005_01_ANIM_H__
#define VETA_TRAP1005_01_ANIM_H__

namespace Veta_Trap1005_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef VETA_TRAP1005_01_ANIM_H__
