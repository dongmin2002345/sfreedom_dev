// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef STARSTORY_METEORITETRAP1001_01_ANIM_H__
#define STARSTORY_METEORITETRAP1001_01_ANIM_H__

namespace Starstory_MeteoriteTrap1001_01_Anim
{
    enum
    {
        IDLE_01                 = 1000000
    };
}

#endif  // #ifndef STARSTORY_METEORITETRAP1001_01_ANIM_H__
