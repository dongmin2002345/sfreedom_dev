// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef BTTRAP2001_01_ANIM_H__
#define BTTRAP2001_01_ANIM_H__

namespace BTtrap2001_01_Anim
{
    enum
    {
        IDLE_01                 = 1000001
    };
}

#endif  // #ifndef BTTRAP2001_01_ANIM_H__
