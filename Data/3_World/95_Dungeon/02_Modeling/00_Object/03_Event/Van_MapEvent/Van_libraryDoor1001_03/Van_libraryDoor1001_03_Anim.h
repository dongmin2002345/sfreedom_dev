// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_LIBRARYDOOR1001_03_ANIM_H__
#define VAN_LIBRARYDOOR1001_03_ANIM_H__

namespace Van_libraryDoor1001_03_Anim
{
    enum
    {
        CLOSE_01                = 1,
        CLOSEIDLE_01            = 2,
        OPEN_01                 = 3,
        OPENIDLE_01             = 0
    };
}

#endif  // #ifndef VAN_LIBRARYDOOR1001_03_ANIM_H__
