// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VAN_DEVILDOOR_1003_01_ANIM_H__
#define VAN_DEVILDOOR_1003_01_ANIM_H__

namespace Van_DevilDoor_1003_01_Anim
{
    enum
    {
        CLOSE_01                = 1,
        CLOSE_IDLE_01           = 2,
        OPEN_01                 = 3,
        OPEN_IDLE_01            = 0
    };
}

#endif  // #ifndef VAN_DEVILDOOR_1003_01_ANIM_H__
