// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MAZEDOOR1001_07_ANIM_H__
#define MAZEDOOR1001_07_ANIM_H__

namespace MazeDoor1001_07_Anim
{
    enum
    {
        CLOSE_01                = 1000011,
        CLOSEIDLE_01            = 1000000,
        OPEN_01                 = 1000030,
        OPENIDLE_01             = 1000020
    };
}

#endif  // #ifndef MAZEDOOR1001_07_ANIM_H__
