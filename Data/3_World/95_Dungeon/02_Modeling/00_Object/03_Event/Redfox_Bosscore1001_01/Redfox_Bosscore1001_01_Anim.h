// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef REDFOX_BOSSCORE1001_01_ANIM_H__
#define REDFOX_BOSSCORE1001_01_ANIM_H__

namespace Redfox_Bosscore1001_01_Anim
{
    enum
    {
        01_OPENING_01           = 1000001,
        02_ACTION_01            = 1000002,
        03_ENDING_01            = 1000003,
        04_FINISH_01            = 1000004
    };
}

#endif  // #ifndef REDFOX_BOSSCORE1001_01_ANIM_H__
