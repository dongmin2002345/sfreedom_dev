// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef VETA_DARKGATE1001_01_ANIM_H__
#define VETA_DARKGATE1001_01_ANIM_H__

namespace Veta_DarkGate1001_01_Anim
{
    enum
    {
        CLOSE_01                = 1000011,
        CLOSE_IDLE_01           = 1000000,
        OPEN_01                 = 1000030,
        OPEN_02                 = 1000031,
        OPEN_03                 = 1000032,
        OPEN_04                 = 1000033,
        OPEN_05                 = 1000034,
        OPEN_06                 = 1000035,
        OPEN_07                 = 1000036,
        OPEN_08                 = 1000037,
        OPEN_09                 = 1000038,
        OPEN_IDLE_01            = 1000020,
        OPEN_IDLE_02            = 1000021,
        OPEN_IDLE_03            = 1000022,
        OPEN_IDLE_04            = 1000023,
        OPEN_IDLE_05            = 1000024,
        OPEN_IDLE_06            = 1000025,
        OPEN_IDLE_07            = 1000026,
        OPEN_IDLE_08            = 1000027,
        OPEN_IDLE_09            = 1000028
    };
}

#endif  // #ifndef VETA_DARKGATE1001_01_ANIM_H__
