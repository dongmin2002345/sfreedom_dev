// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_SKILL_FIRE_BALL_CHAR_ROOT_ANIM_H__
#define EF_SKILL_FIRE_BALL_CHAR_ROOT_ANIM_H__

namespace ef_skill_Fire_Ball_char_root_Anim
{
    enum
    {
        01                      = 1000000
    };
}

#endif  // #ifndef EF_SKILL_FIRE_BALL_CHAR_ROOT_ANIM_H__
