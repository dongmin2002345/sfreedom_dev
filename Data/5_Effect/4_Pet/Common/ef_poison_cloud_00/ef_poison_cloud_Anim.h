// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef EF_POISON_CLOUD_ANIM_H__
#define EF_POISON_CLOUD_ANIM_H__

namespace ef_poison_cloud_Anim
{
    enum
    {
        SKILL_00                = 1000000
    };
}

#endif  // #ifndef EF_POISON_CLOUD_ANIM_H__
