// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPBOX_ANIM_H__
#define DROPBOX_ANIM_H__

namespace dropbox_Anim
{
    enum
    {
        APPEAR                  = 0,
        DISAPPEAR               = 1,
        OPEN                    = 2
    };
}

#endif  // #ifndef DROPBOX_ANIM_H__
