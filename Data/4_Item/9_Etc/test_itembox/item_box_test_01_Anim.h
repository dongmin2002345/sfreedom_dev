// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ITEM_BOX_TEST_01_ANIM_H__
#define ITEM_BOX_TEST_01_ANIM_H__

namespace item_box_test_01_Anim
{
    enum
    {
        IDLE_OPEN_01            = 1000002,
        IDLE_OPEN_STOP_01       = 1000003,
        IDLE_ROTATION_01        = 1000000,
        IDLE_ROTATION_STOP_01   = 1000001
    };
}

#endif  // #ifndef ITEM_BOX_TEST_01_ANIM_H__
