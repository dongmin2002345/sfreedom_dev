// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SADONICS_BROKEN_02_ANIM_H__
#define SADONICS_BROKEN_02_ANIM_H__

namespace sadonics_broken_02_Anim
{
    enum
    {
        DIE_02                  = 1000020,
        DMG_02                  = 1000010,
        IDLE_02                 = 1000000
    };
}

#endif  // #ifndef SADONICS_BROKEN_02_ANIM_H__
