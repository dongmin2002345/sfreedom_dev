// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef SADONICS_BROKEN_03_ANIM_H__
#define SADONICS_BROKEN_03_ANIM_H__

namespace sadonics_broken_03_Anim
{
    enum
    {
        DIE_03                  = 1000020,
        DMG_03                  = 1000010,
        IDLE_03                 = 1000000
    };
}

#endif  // #ifndef SADONICS_BROKEN_03_ANIM_H__
