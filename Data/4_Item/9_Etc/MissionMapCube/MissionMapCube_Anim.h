// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef MISSIONMAPCUBE_ANIM_H__
#define MISSIONMAPCUBE_ANIM_H__

namespace MissionMapCube_Anim
{
    enum
    {
        IDLE_01                 = 1000000,
        OPEN_01                 = 1000001
    };
}

#endif  // #ifndef MISSIONMAPCUBE_ANIM_H__
