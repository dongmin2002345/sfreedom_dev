// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPSCR_ANIM_H__
#define DROPSCR_ANIM_H__

namespace DropScr_Anim
{
    enum
    {
        APPEAR                  = 0,
        DISAPPEAR               = 1,
        OPEN                    = 2
    };
}

#endif  // #ifndef DROPSCR_ANIM_H__
