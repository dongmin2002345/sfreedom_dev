// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPSCROLL_GREY_ANIM_H__
#define DROPSCROLL_GREY_ANIM_H__

namespace dropScroll_grey_Anim
{
    enum
    {
        APPEAR                  = 0,
        IDLE_01                 = 1
    };
}

#endif  // #ifndef DROPSCROLL_GREY_ANIM_H__
