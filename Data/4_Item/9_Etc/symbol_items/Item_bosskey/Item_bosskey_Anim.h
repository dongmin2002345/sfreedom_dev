// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ITEM_BOSSKEY_ANIM_H__
#define ITEM_BOSSKEY_ANIM_H__

namespace Item_bosskey_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef ITEM_BOSSKEY_ANIM_H__
