// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPETC_GREY_ANIM_H__
#define DROPETC_GREY_ANIM_H__

namespace DropEtc_grey_Anim
{
    enum
    {
        APPEAR                  = 0,
        IDLE_01                 = 1
    };
}

#endif  // #ifndef DROPETC_GREY_ANIM_H__
