// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef HERO_CERTIFICATE_ANIM_H__
#define HERO_CERTIFICATE_ANIM_H__

namespace hero_certificate_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef HERO_CERTIFICATE_ANIM_H__
