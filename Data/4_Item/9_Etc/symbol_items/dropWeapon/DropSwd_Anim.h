// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPSWD_ANIM_H__
#define DROPSWD_ANIM_H__

namespace DropSwd_Anim
{
    enum
    {
        APPEAR                  = 0,
        DISAPPEAR               = 1,
        OPEN                    = 2
    };
}

#endif  // #ifndef DROPSWD_ANIM_H__
