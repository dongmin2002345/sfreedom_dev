// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPPOTION_ANIM_H__
#define DROPPOTION_ANIM_H__

namespace DropPotion_Anim
{
    enum
    {
        APPEAR                  = 0,
        IDLE_01                 = 1
    };
}

#endif  // #ifndef DROPPOTION_ANIM_H__
