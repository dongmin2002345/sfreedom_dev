// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ITEM_HERO_ANIM_H__
#define ITEM_HERO_ANIM_H__

namespace Item_Hero_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef ITEM_HERO_ANIM_H__
