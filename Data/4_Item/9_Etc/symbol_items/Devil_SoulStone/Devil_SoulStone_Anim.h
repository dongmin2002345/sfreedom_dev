// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DEVIL_SOULSTONE_ANIM_H__
#define DEVIL_SOULSTONE_ANIM_H__

namespace Devil_SoulStone_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef DEVIL_SOULSTONE_ANIM_H__
