// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WALL_SCROLL_BALLOON_ANIM_H__
#define WALL_SCROLL_BALLOON_ANIM_H__

namespace Wall_Scroll_Balloon_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef WALL_SCROLL_BALLOON_ANIM_H__
