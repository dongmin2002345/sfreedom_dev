// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef T_SHIRT_BALLOON_GREY_ANIM_H__
#define T_SHIRT_BALLOON_GREY_ANIM_H__

namespace T_Shirt_Balloon_grey_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef T_SHIRT_BALLOON_GREY_ANIM_H__
