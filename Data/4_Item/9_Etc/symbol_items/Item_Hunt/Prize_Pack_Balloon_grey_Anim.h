// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef PRIZE_PACK_BALLOON_GREY_ANIM_H__
#define PRIZE_PACK_BALLOON_GREY_ANIM_H__

namespace Prize_Pack_Balloon_grey_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef PRIZE_PACK_BALLOON_GREY_ANIM_H__
