// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef ICE_CASH_BALLOON_GREY_ANIM_H__
#define ICE_CASH_BALLOON_GREY_ANIM_H__

namespace Ice_Cash_Balloon_grey_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef ICE_CASH_BALLOON_GREY_ANIM_H__
