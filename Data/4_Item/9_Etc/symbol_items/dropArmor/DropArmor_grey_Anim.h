// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPARMOR_GREY_ANIM_H__
#define DROPARMOR_GREY_ANIM_H__

namespace DropArmor_grey_Anim
{
    enum
    {
        APPEAR                  = 0,
        IDLE_01                 = 1
    };
}

#endif  // #ifndef DROPARMOR_GREY_ANIM_H__
