// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef DROPDEF_ANIM_H__
#define DROPDEF_ANIM_H__

namespace DropDef_Anim
{
    enum
    {
        APPEAR                  = 0,
        DISAPPEAR               = 1,
        OPEN                    = 2
    };
}

#endif  // #ifndef DROPDEF_ANIM_H__
