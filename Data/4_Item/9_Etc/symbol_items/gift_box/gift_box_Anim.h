// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef GIFT_BOX_ANIM_H__
#define GIFT_BOX_ANIM_H__

namespace gift_box_Anim
{
    enum
    {
        APPEAR                  = 0,
        ICLE_01                 = 1
    };
}

#endif  // #ifndef GIFT_BOX_ANIM_H__
