// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef WEAPON_POWDER_ANIM_H__
#define WEAPON_POWDER_ANIM_H__

namespace weapon_powder_Anim
{
    enum
    {
        APPEAR_01               = 0,
        IDLE_01                 = 1
    };
}

#endif  // #ifndef WEAPON_POWDER_ANIM_H__
