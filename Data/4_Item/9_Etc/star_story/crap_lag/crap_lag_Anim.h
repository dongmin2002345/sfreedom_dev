// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef CRAP_LAG_ANIM_H__
#define CRAP_LAG_ANIM_H__

namespace crap_lag_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef CRAP_LAG_ANIM_H__
