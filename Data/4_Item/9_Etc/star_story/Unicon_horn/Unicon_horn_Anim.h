// This file was automatically generated. It contains definitions for all the
// animations stored in the associated KFM file. Include this file in your
// final application to easily refer to animation sequences.

#ifndef UNICON_HORN_ANIM_H__
#define UNICON_HORN_ANIM_H__

namespace Unicon_horn_Anim
{
    enum
    {
        APPEAR                  = 1,
        IDLE_01                 = 0
    };
}

#endif  // #ifndef UNICON_HORN_ANIM_H__
