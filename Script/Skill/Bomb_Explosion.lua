-- Melee
function Skill_Bomb_Explosion_OnCastingCompleted(actor, action)
end

function Skill_Bomb_Explosion_OnEnter(actor, action)
	return true
end

function Skill_Bomb_Explosion_OnUpdate(actor, accumTime, frameTime)
	return false
end

function Skill_Bomb_Explosion_OnCleanUp(actor)
	return true;
end

function Skill_Bomb_Explosion_OnLeave(actor, action)
	return true
end

